DROP TABLE IF EXISTS "chart_acl";
DROP TABLE IF EXISTS "dataset_acl";
DROP TABLE IF EXISTS "dashboard_acl";
DROP TABLE IF EXISTS "project_acl";

DROP TABLE IF EXISTS "chart_resources";

DROP TABLE IF EXISTS "dashboards_charts";
DROP TABLE IF EXISTS "dashboards";

DROP TABLE IF EXISTS "charts_datasets";

DROP TABLE IF EXISTS "charts";
DROP TABLE IF EXISTS "datasets";
DROP TABLE IF EXISTS "projects";
DROP TABLE IF EXISTS "local_credentials";
DROP TABLE IF EXISTS "users";


--
-- Authentication
--

CREATE TABLE "users" (
	"id"				serial NOT NULL,
	"username"			text NOT NULL,
	"credential_source"	text NOT NULL,
	"name"				text NOT NULL,
	"active"			boolean NOT NULL DEFAULT true,
	"modified"			timestamp with time zone NOT NULL,
	"roles"				text ARRAY NOT NULL,
	
	PRIMARY KEY ("id"),
	UNIQUE ("username", "credential_source")
);

CREATE TABLE "local_credentials" (
	"id__users"			int NOT NULL REFERENCES "users" ("id") ON DELETE CASCADE,
	"password_hash"		text NOT NULL,
	"modified"			timestamp with time zone NOT NULL,
	
	PRIMARY KEY ("id__users")
);


--
-- Projects
--

CREATE TABLE "projects"(
	"id"			serial NOT NULL,
	"name"			text NOT NULL,
	"description"	text,
	
	
	PRIMARY KEY ("id"),
	UNIQUE ("name")
);
CREATE INDEX IF NOT EXISTS projects_name_idx ON "projects" ("name");

--
-- Charts, datasets, etc.
--


CREATE TABLE "charts" (
	"id"					serial NOT NULL,
	"creator"				int REFERENCES "users" ("id") ON DELETE SET NULL,
	"id__projects"			int NOT NULL REFERENCES "projects" ("id") ON DELETE CASCADE,
	"name"					text NOT NULL,
	"description"			text,
	"created"				timestamp with time zone NOT NULL,
	"modified"				timestamp with time zone NOT NULL,
	"main_resource_path"	text NOT NULL,
	
	PRIMARY KEY ("id"),
	UNIQUE ("name", "id__projects")
);
CREATE INDEX IF NOT EXISTS charts_id_projects_idx ON "charts" ("id__projects");
CREATE INDEX IF NOT EXISTS charts_creator_idx ON "charts" ("creator");
CREATE INDEX IF NOT EXISTS charts_name_idx ON "charts" ("name");


CREATE TABLE "chart_resources"(
	"id"		serial NOT NULL,
	"chart"		int NOT NULL REFERENCES "charts" ("id") ON DELETE CASCADE,
	"path"		text NOT NULL,
	"data"		bytea NOT NULL,
	
	PRIMARY KEY ("id"),
	UNIQUE ("path", "chart")
);

CREATE INDEX IF NOT EXISTS chart_resources_chart_idx ON "chart_resources" ("chart");


CREATE TABLE "datasets"(
	"id"				serial NOT NULL,
	"id__projects"		int NOT NULL REFERENCES "projects" ("id") ON DELETE CASCADE,
	"identifier"		text NOT NULL,
	"content_type"		text NOT NULL,
	"description" 		text,
	"data"				bytea NOT NULL,
	
	PRIMARY KEY ("id"),
	UNIQUE ("identifier", "id__projects")
);
CREATE INDEX IF NOT EXISTS datasets_id_projects_idx ON "datasets" ("id__projects");
CREATE INDEX IF NOT EXISTS datasets_identifier_idx ON "datasets" ("identifier");


CREATE TABLE "charts_datasets"(
	"id__charts"	int NOT NULL REFERENCES "charts" ("id") ON DELETE CASCADE,
	"id__datasets"	int NOT NULL REFERENCES "datasets" ("id") ON DELETE RESTRICT,
	
	PRIMARY KEY ("id__charts", "id__datasets")
);




--
-- Dashboards
--
CREATE TABLE "dashboards"(
	"id"			serial NOT NULL,
	"id__projects"	int NOT NULL REFERENCES "projects" ("id") ON DELETE CASCADE,
	"name"			text NOT NULL,
	"description"	text,
	
	PRIMARY KEY ("id"),
	UNIQUE ("id__projects", "name")
);

CREATE INDEX IF NOT EXISTS dashboards_id__projects_idx ON "dashboards" ("id__projects");
CREATE INDEX IF NOT EXISTS dashboards_name_idx ON "dashboards" ("name");

CREATE TABLE "dashboards_charts" (
	"id__dashboards"	int NOT NULL REFERENCES "dashboards" ("id") ON DELETE CASCADE,
	"id__charts"		int NOT NULL REFERENCES "charts" ("id") ON DELETE CASCADE,
	"position"			int NOT NULL,
	
	PRIMARY KEY ("id__dashboards", "id__charts", "position"),
	UNIQUE ("id__dashboards", "position")
);
CREATE INDEX IF NOT EXISTS dashboards_charts_id__dashboards_idx ON "dashboards_charts" ("id__dashboards");
CREATE INDEX IF NOT EXISTS dashboards_charts_id__charts_idx ON "dashboards_charts" ("id__charts");

--
-- SECURITY
--

-- an ACL consists of all rows in the appropriate ACL table for a given 
-- chart/dataset

CREATE TABLE "chart_acl"(
	"chart"			int NOT NULL REFERENCES "charts" ("id") ON DELETE CASCADE,
	"id__users"		int REFERENCES "users" ("id") ON DELETE CASCADE,
	"role"			text,
	"permission"	text NOT NULL,
		
	CHECK (("role" IS NOT NULL) != ("id__users" IS NOT NULL)),
	
	UNIQUE ("chart", "id__users", "role")
);
CREATE INDEX IF NOT EXISTS chart_acl_chart_idx ON "chart_acl" ("chart");
CREATE INDEX IF NOT EXISTS chart_acl_id__users ON "chart_acl" ("id__users");

CREATE TABLE "dashboard_acl"(
	"dashboard"		int NOT NULL REFERENCES "dashboards" ("id") ON DELETE CASCADE,
	"id__users"		int REFERENCES "users" ("id") ON DELETE CASCADE,
	"role"			text,
	"permission"	text NOT NULL,
		
	CHECK (("role" IS NOT NULL) != ("id__users" IS NOT NULL)),
	
	UNIQUE ("dashboard", "id__users", "role")
);
CREATE INDEX IF NOT EXISTS dashboard_acl_dashboard_acl_idx ON "dashboard_acl" ("dashboard");
CREATE INDEX IF NOT EXISTS dashboard_acl_id__users_idx ON "dashboard_acl" ("id__users");


CREATE TABLE "dataset_acl"(
	"dataset"		int NOT NULL REFERENCES "datasets" ("id") ON DELETE CASCADE,
	"id__users"		int REFERENCES "users" ("id") ON DELETE CASCADE,
	"role"			text,
	"permission"	text NOT NULL,
		
	CHECK (("role" IS NOT NULL) != ("id__users" IS NOT NULL)),
	
	UNIQUE ("dataset", "id__users", "role")
);
CREATE INDEX IF NOT EXISTS dataset_acl_dataset_idx ON "dataset_acl" ("dataset");
CREATE INDEX IF NOT EXISTS dataset_acl_id__users_idx ON "dataset_acl" ("id__users");

CREATE TABLE "project_acl"(
	"project"		int NOT NULL REFERENCES "projects" ("id") ON DELETE CASCADE,
	"id__users"		int REFERENCES "users" ("id") ON DELETE CASCADE,
	"role"			text,
	"permission"	text NOT NULL,
		
	CHECK (("role" IS NOT NULL) != ("id__users" IS NOT NULL)),
	
	UNIQUE ("project", "id__users", "role")
);
CREATE INDEX IF NOT EXISTS project_acl_project_idx ON "project_acl" ("project");
CREATE INDEX IF NOT EXISTS project_acl_id__users_idx ON "project_acl" ("id__users");
