package edu.unc.cscc.views;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;

public class Util {


	    public static void main(String args[]) {
	        String upperCaseLetters = RandomStringUtils.random(2, 65, 90, true, true);
	        String lowerCaseLetters = RandomStringUtils.random(3, 97, 122, true, true);
	        String numbers = RandomStringUtils.randomNumeric(3);
	        String specialChar = RandomStringUtils.random(2, 33, 47, false, false);
	        String totalChars = RandomStringUtils.randomAlphanumeric(8);
	        String combinedChars = upperCaseLetters.concat(lowerCaseLetters)
	          .concat(numbers)
	          .concat(specialChar)
	          .concat(totalChars);
	        List<Character> pwdChars = combinedChars.chars()
	          .mapToObj(c -> (char) c)
	          .collect(Collectors.toList());
	        Collections.shuffle(pwdChars);
	        String password = pwdChars.stream()
	          .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
	          .toString();
	        
	        System.out.println(password);
//	    	System.out.println(new Auth0Service().generateRandomPassword());
	    }
}
