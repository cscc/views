package edu.unc.cscc.views.exception;

public class AuthStateException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AuthStateException(String message) {
        super(message);
    }
    
}
