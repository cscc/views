package edu.unc.cscc.views.exception;

import com.auth0.exception.APIException;

public class UserExistsException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserExistsException(String message){
        super(message);
    }

    public UserExistsException(String message, APIException ex) {
        super(message, ex);
    }
    
}
