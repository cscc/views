package edu.unc.cscc.views;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class ViewsApplication 
{

	public static void 
	main(String[] args) 
	{
		SpringApplicationBuilder builder = new SpringApplicationBuilder();
		
		builder.sources(ViewsApplication.class)
				.run(args);
	}
}
