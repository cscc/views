package edu.unc.cscc.views.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

import edu.unc.cscc.views.model.Chart;
import edu.unc.cscc.views.model.ChartResource;
import edu.unc.cscc.views.service.VSec;
import edu.unc.cscc.views.service.storage.ChartStorageService;
import edu.unc.cscc.views.service.storage.ResourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = {"Chart Resources"})
@RestController
@Validated
public class ResourceController 
{
	
	@Autowired
	private ResourceService			resourceService;
	
	@Autowired
	private ChartStorageService		chartService;
	
	@Autowired
	private VSec					sec;

	@RequestMapping(path = "/charts/{chartID}/resources/{path:.+}", 
					method = RequestMethod.GET)
	@ApiOperation(value = "get a chart resource, identified by path",
					notes = "Get the resource located at a specified path "
							+ "for a specified chart.  In order for this "
							+ "operation to succeed, the requesting user "
							+ "must be able to read the specified chart.")
	@ApiResponses({
		@ApiResponse(code = 200, 
					message = "resource retrieved successfully"),
		@ApiResponse(code = 404, message = "no such resource found for chart")
	})
	public void
	getResource(@PathVariable("chartID")
				@Min(0)
				@ApiParam(value = "ID of the chart with which the requested "
									+ "resource is associated") 
					final int chartID, 
				@PathVariable("path")
				@Size(min = 1)
				@ApiParam("path of the requested resource")
					final String path, 
				HttpServletResponse response)
	throws IOException
	{
		if (! this.resourceService.exists(path, chartID))
		{
			response.setStatus(HttpStatus.NOT_FOUND.value());
			return;
		}
		
		final Chart chart = this.chartService.load(chartID);
		
		if (! this.sec.canReadChart(chart))
		{
			throw new AccessDeniedException(
					"Access to chart ID " + chartID + " denied based on DS/project");
		}
		
		final ChartResource resource = 
				this.resourceService.loadShallow(path, chartID);
		
		response.setContentType(resource.contentType());
		
		this.resourceService.loadData(resource.id(), response.getOutputStream());
		
	}
	
	@RequestMapping(path = "/charts/{chartID}/resources/{path:.+}", 
					method = RequestMethod.PUT)
	@PreAuthorize("@vsec.chartAllowed(#chartID, @vsec.PERM_WRITE)")
	@ApiOperation(value = "update a resource associated with a chart",
					notes = "Updates a resource associated with the "
							+ "specified chart.  The resource will be "
							+ "replaced in its entirity with the payload of "
							+ "the request.  In order for this operation to "
							+ "succeed, the requesting user must hold the "
							+ "'write' permission for the chart with which the "
							+ "resource is associated.",
					code = 204)
	@ApiResponses({
		@ApiResponse(code = 204, message = "resource successfully updated"),
		@ApiResponse(code = 404, message = "no such resource/chart found")
	})
	public ResponseEntity<?>
	updateResource(@PathVariable("chartID")
					@Min(0)
					@ApiParam("ID of the chart for which the resource should be updated")
						final int chartID, 
					@PathVariable("path")
					@Size(min = 1)
					@ApiParam("path of the resource to update")
						final String path, 
					@RequestPart(name = "data", required = true)
					@ApiParam(value = "content with which to replace the "
										+ "resource; will be used as-is",
								collectionFormat = "single",
								
								/* yep, this is a lie.  But as far as I can tell,
								 * Swagger doesn't support multi-part uploads
								 * with a part that can be any arbitrary data.
								 */
								type = "any",
								/* apparently specifying type makes it ignore 
								 * @RequestPart#name().  Yay.
								 */
								name = "data") 
					byte[] content)
	{
		if (! this.resourceService.exists(path, chartID))
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		ChartResource res = this.resourceService.loadShallow(path, chartID);
		
		res = this.resourceService.save(res.data(content));
		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
}
