package edu.unc.cscc.views.controller;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonGetter;

import edu.unc.cscc.views.model.security.VACLDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Model for search results wrapper used to support client-side paging.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 * @param <T> result type
 */
@ApiModel("search-results")
public class SearchResults<T>
{
	private final List<T>					results;
	private final int						totalCount;
	private final Map<Integer, VACLDTO>		acls;
	
	public SearchResults(List<T> results, int totalCount)
	{
		this(results, totalCount, null);
	}
	
	public SearchResults(List<T> results, int totalCount, 
							Map<Integer, VACLDTO> acls)
	{
		this.results = results;
		this.totalCount = totalCount;
		this.acls = acls;
	}
	
	/**
	 * Get the ACLs for the contained results (if requested.)
	 * 
	 * @return ACLs, or <code>null</code> if no ACLs were requested/required
	 */
	@JsonGetter("acls")
	@ApiModelProperty(value = "ACLs for the results, indexed by ID of result entity")
	public Map<Integer, VACLDTO> 
	acls()
	{
		return this.acls != null
				? Collections.unmodifiableMap(this.acls)
				: null;
	}
	
	/**
	 * Get the loaded segment of results.
	 * 
	 * @return results
	 */
	@JsonGetter("results")
	@ApiModelProperty(value = "requested portion of search results")
	public List<T>
	results()
	{
		return Collections.unmodifiableList(results);
	}
	
	
	/**
	 * Alias for {@link #count()}.
	 * 
	 * @return total result count
	 */
	@JsonGetter("totalCount")
	@ApiModelProperty(value = "count of all possible results", hidden = true)
	public int
	totalCount()
	{
		return this.count();
	}
	
	/**
	 * Get the total result count, i.e. the count of all results known to exist
	 * by the server regardless of whether or not they are present in the 
	 * given results.
	 * 
	 * @return total result count
	 */
	@JsonGetter("total-count")
	@ApiModelProperty(value = "count of all possible results")
	public int
	count()
	{
		return this.totalCount;
	}
}
