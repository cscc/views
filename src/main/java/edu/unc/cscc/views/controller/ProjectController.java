package edu.unc.cscc.views.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import edu.unc.cscc.views.config.PreloadedUserConfig;
import edu.unc.cscc.views.model.Project;
import edu.unc.cscc.views.model.security.ProjectOID;
import edu.unc.cscc.views.model.security.User;
import edu.unc.cscc.views.model.security.VACL;
import edu.unc.cscc.views.model.security.VACLDTO;
import edu.unc.cscc.views.model.security.VACLEntry;
import edu.unc.cscc.views.model.security.VPermission;
import edu.unc.cscc.views.model.security.VSid;
import edu.unc.cscc.views.service.storage.ProjectStorageService;
import edu.unc.cscc.views.service.storage.UserStorage;
import edu.unc.cscc.views.service.storage.VACLService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Projects")
@RequestMapping("/projects")
@RestController
@Validated
public class ProjectController
{
	
	@Autowired
	private ProjectStorageService		service;
	
	@Autowired
	private VACLService					aclService;
	
	@Autowired
	private UserStorage					userService;
	@Autowired
	PreloadedUserConfig preloadedUserConfig;
	
	@RequestMapping(value = "/{id}",
					method = RequestMethod.GET,
					produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "retrieve a project by ID",
					notes = "Retrieves the project with the given ID. In order"
							+ "for this operation to succeed, the requesting "
							+ "user must have the 'read' permission for the "
							+ "requested project.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "project retrieved successfully",
						response = Project.class),
		@ApiResponse(code = 404, message = "no such project found")
	})
	public ResponseEntity<?>
	get(@PathVariable("id")
		@Min(0)
		@ApiParam("ID of the project to retrieve")
		final int id)
	{
		final Project p = this.service.load(id);
		
		if (p == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(p, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/search",
					method = RequestMethod.GET,
					produces = "application/json")
	@ApiOperation(value = "search for a project by name, description",
					notes = "Search for any projects with names or descriptions "
							+ "containing a given string.  If no search term is "
							+ "provided, list all projects visible to the requesting "
							+ "user.")
	public SearchResults<Project>
	search(@RequestParam(value = "term", required = false)
			@ApiParam("search term; omitted to list all projects")
				final String term, 
			@RequestParam(value = "limit", defaultValue = "100", required = false)
			@Min(1) @Max(1000)
			@ApiParam(value = "maximum number of results to return, starting "
						+ "from offset",
						defaultValue = "100")
				int limit, 
			@RequestParam(value = "offset", required = false, defaultValue = "0")
			@Min(0)
			@ApiParam(value = "offset of results; zero-indexed",
						required = false, 
						defaultValue = "0")	
				int offset,
			@RequestParam(value = "with-user-acl", required = false, defaultValue = "true")
			@ApiParam(value = "whether to include ACL entries reflecting the "
								+ "requesting user's permissions for the results",
						required = false, 
						defaultValue = "true")
			boolean withUserACL,
			@AuthenticationPrincipal User user)
	{
		List<Integer> ids = this.service.search(term, user, limit, offset);
		
		if (ids.isEmpty())
		{
			return new SearchResults<>(Collections.emptyList(), 0);
		}
		
		final Map<Integer, Project> projects = this.service.load(ids);
		
		final List<Project> p = 
				ids.stream().map(projects :: get).collect(Collectors.toList());
		
		final int totalCount = this.service.count(term, user);		
		
		if (withUserACL)
		{
			Map<Integer, VACLDTO> acls = new HashMap<>();
			/* TODO - batch load! */
			p.stream()
				.forEach(project -> 
					acls.put(project.id(), 
							VACLDTO.create(this.aclService.load(new ProjectOID(project.id()), user), f -> user)));
			
			return new SearchResults<>(p, totalCount, acls);
		}
		
		return new SearchResults<>(p, totalCount);
	}
	
	@RequestMapping(value = "/{id}",
					method = RequestMethod.DELETE)
	@ApiOperation(value = "delete a project",
					notes = "Delete the project with the given ID.  In order "
							+ "for this operation to be successful, the "
							+ "requesting user must hold the 'write' permission "
							+ "on the specified project.",
					code = 204)
	@ApiResponses({
		@ApiResponse(code = 204, message = "deletion was successful"),
		@ApiResponse(code = 404, message = "no such project found")
	})
	public ResponseEntity<?>
	delete(@PathVariable("id")
			@Min(0)
			@ApiParam("ID of the project to delete")
			int id)
	{
		
		final Project project = this.service.load(id);
		
		if (project == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		this.service.delete(id);
		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@RequestMapping(value = "/",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@Transactional
	@ApiOperation(value = "create a project",
					notes = "Create a new project.  The requesting user will "
							+ "hold all permissions on the resulting entity.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "project created successfully",
						response = Project.class),
		@ApiResponse(code = 400, message = "bad request; project specified ID",
						response = NamedError.class),
		@ApiResponse(code = 409, message = "specified name already in use",
						response = NamedError.class)
	})
	public ResponseEntity<?>
	create(@RequestBody @Valid
			@ApiParam("new project; must not have an ID")
			final NewProject project,
			@AuthenticationPrincipal User user)
	{
		
		if (project.id() != null)
		{
			return new ResponseEntity<>(new NamedError("cannot-create-with-id-given"), 
						HttpStatus.BAD_REQUEST);
		}
		
		/* check for duplicate name */
		
		if (this.service.exists(project.name()))
		{
			return new ResponseEntity<>(new NamedError("name-exists"), 
						HttpStatus.CONFLICT);
		}
		
		Project p = this.service.save(project);
		
		/* set up our ACL, note that we don't have the ability to manage project ACLs */
		final Collection<VACLEntry> entries = new ArrayList<>();
		addResourceACLEntries(entries, user, null, null, p.id(), Boolean.TRUE);
		preloadedUserConfig.getDefaultResourceAdmin().forEach((key,value) 
				-> addResourceACLEntries(entries, user, key, value, p.id(), Boolean.FALSE));
		
		VACL acl = new VACL(new ProjectOID(p.id()), entries, false);
		
		acl = this.aclService.save(acl);
		
		
		return new ResponseEntity<>(p, HttpStatus.OK);
	}
	
	private void addResourceACLEntries(Collection<VACLEntry> entries, 
			User currentUser, String preloadedUserName, 
			String preloadedUserContext, int projectId, boolean isCreator) {
		User sUser = null;
		
		if(isCreator) {
			sUser = currentUser;
		} else if(!currentUser.getUsername().equalsIgnoreCase(preloadedUserName)) {
			sUser = userService.find(preloadedUserName, preloadedUserContext);
		}
		if(sUser != null)
		{
			final VSid sSid = new VSid(sUser);
			Stream.of(VPermission.values())
				.filter(perm -> perm.applicableTo(new ProjectOID(projectId)))
				.map(perm -> new VACLEntry(sSid, perm))
				.forEach(entries :: add);
		}
	}
	
	@RequestMapping(value = "/{id}",
			method = RequestMethod.PUT,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "update a project",
					notes = "Update the specified project, replacing its "
							+ "properties with those of the submitted object."
							+ "  In order for this operation to succeed, the "
							+ "requesting user must hold the 'write' permission "
							+ "on the project to be updated."
					)
	@ApiResponses({
		@ApiResponse(code = 200, 
						message = "project updated successfully; response "
								+ "reflects current state",
						response = Project.class),
		@ApiResponse(code = 404, message = "no such project found"),
		@ApiResponse(code = 409, message = "name already in use",
						response = NamedError.class)
	})
	public ResponseEntity<?>
	update(@PathVariable("id")
			@Min(0)
			@ApiParam("ID of the project to update")
				int id,
			@RequestBody
			@Valid
			/* WTF - redoc and swagger-ui aren't showing the structure 
			 * for this, despite it being in the resulting swagger.json ?!
			 */
			@ApiParam(value = "updated project", required = true, name = "project")
				final Project project)
	{
		Project existing = this.service.load(id);
		
		if (existing == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		existing = this.service.loadByName(project.name());
		
		if (existing != null && ! existing.id().equals(project.id()))
		{
			return new ResponseEntity<>(new NamedError("name-exists"), 
										HttpStatus.CONFLICT);
		}
		
		Project saved = this.service.save(project.id(id));
		
		return new ResponseEntity<>(saved, HttpStatus.OK);
	}
	
	
	/* security rules:
	 * user must be able to read project and either the user ID must be omitted
	 * OR the user must be requesting their own ACL entries OR the user must 
	 * have either the manage_project_acls authority or the manage_users 
	 * authority
	 */
	@PreAuthorize("@vsec.projectAllowed(#id, 'read') "
					+ "and ((#userID == #principal.id() "
					+ "			or hasAuthority('manage_project_acls')"
					+ "			or hasAuthority('manage_users'))"
					+ "		or"
					+ "		#userID == null)")
	@RequestMapping(value = "/{id}/acl",
					method = RequestMethod.GET,
					produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "retrieve the ACL of a specified project",
					notes = "Retrieve the ACL of the specified project.  In order "
							+ "for this operation to succeed, the requesting user "
							+ "must be able to read the project.  "
							+ "Additionally, either the "
							+ "user ID must be omitted OR the user must be "
							+ "requesting ACL entries for their own user ID "
							+ "OR the user must have EITHER the 'manage_project_acls' "
							+ "role OR the 'manage_users' role.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "ACL retrieved successfully",
						response = VACLDTO.class),
		@ApiResponse(code = 404, message = "project not found")
	})
	public ResponseEntity<?>
	getACL(@PathVariable("id")
			@Min(0) 
			@ApiParam("ID of the project for which to retrieve the ACL")
				int id,
			@RequestParam(value = "user-id", required = false)
			@Min(0)
			@ApiParam(value = "ID of the user for which to retrieve ACL entries; "
								+ "if omitted the user's own ACL entries will be "
								+ "returned OR (if the user holds either the 'manage_project_acls' "
								+ "role or the 'manage_users' role) all ACL "
								+ "entries will be returned",
						required = false)
				Integer userID,
			@AuthenticationPrincipal User principal)
	{
		Project project = this.service.load(id);
		
		if (project == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		VACL acl;
		
		if (userID != null)
		{
			User user = this.userService.load(userID);
			
			if (user == null)
			{
				acl = new VACL(new ProjectOID(id), Collections.emptySet(), true);
			}
			else
			{
				acl = this.aclService.load(new ProjectOID(id), user);
			}
		}
		else
		{
			/* if no user ID is specified, but the user can't manage 
			 * project ACLs, restrict the resulting ACL to only include 
			 * things that apply to them.
			 */
			if (principal.getAuthorities()
							.contains(User.ViewsRole.ROLE_MANAGE_PROJECT_ACLS
													.authority()))
			{
				acl = this.aclService.load(new ProjectOID(id));
			}
			else
			{
				acl = this.aclService.load(new ProjectOID(id), principal);
			}
		}
		
		Map<Integer, User> users = 
				this.userService
					.load(acl.represents(VSid.class)
							.stream()
							.map(s -> s.userID())
							.collect(Collectors.toSet()));
		
		VACLDTO dto = VACLDTO.create(acl, users :: get);
		
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('manage_project_acls') and "
				+ "@vsec.projectAllowed(#id, 'write')")
	@RequestMapping(value = "/{id}/acl",
					method = RequestMethod.PUT,
					produces = MediaType.APPLICATION_JSON_VALUE,
					consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "update the ACL for a specified project",
					notes = "Update the ACL entries for the specified project. "
							+ "The project's ACL will be replaced with the "
							+ "submitted entries.  In order for this operation "
							+ "to succeed, the requesting user must have both "
							+ "the 'manage_project_acls' role AND the ability "
							+ "to modify the project in question.",
					code = 204)
	@ApiResponses({
		@ApiResponse(code = 204, message = "ACL updated successfully"),
		@ApiResponse(code = 404, message = "project not found")
	})
	public ResponseEntity<?>
	saveACL(@PathVariable("id")
			@Min(0)
			@ApiParam("ID of the project for which to update the ACL")
				int id,
			@RequestBody
			@Valid
			@ApiParam("ACL containing updated entries; will replace any existing "
					+ "ACL entries for the specified project")
				final VACLDTO dto)
	{
		
		Project project = this.service.load(id);
		
		if (project == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		VACL acl = new VACL(new ProjectOID(id), dto.toACLEntries(), 
								dto.partial());
		
		acl = this.aclService.save(acl);
		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		
	}
	
	@ApiModel("new-project")
	private static final class NewProject
	extends Project
	{

		public NewProject(@JsonProperty("name") String name, 
							@JsonProperty("description") String description)
		{
			super(null, name, description);
		}

		@Override
		@JsonIgnore
		public Integer
		id()
		{
			// TODO Auto-generated method stub
			return super.id();
		}
	}
	
}
