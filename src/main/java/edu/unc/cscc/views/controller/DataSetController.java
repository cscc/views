package edu.unc.cscc.views.controller;


import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;
import org.apache.tika.detect.Detector;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonGetter;

import edu.unc.cscc.views.config.PreloadedUserConfig;
import edu.unc.cscc.views.model.Chart;
import edu.unc.cscc.views.model.DataSet;
import edu.unc.cscc.views.model.Project;
import edu.unc.cscc.views.model.security.DataSetOID;
import edu.unc.cscc.views.model.security.User;
import edu.unc.cscc.views.model.security.VACL;
import edu.unc.cscc.views.model.security.VACLDTO;
import edu.unc.cscc.views.model.security.VACLEntry;
import edu.unc.cscc.views.model.security.VPermission;
import edu.unc.cscc.views.model.security.VSid;
import edu.unc.cscc.views.service.VSec;
import edu.unc.cscc.views.service.storage.ChartStorageService;
import edu.unc.cscc.views.service.storage.DataSetStorage;
import edu.unc.cscc.views.service.storage.DataSetStorage.SortDataSetBy;
import edu.unc.cscc.views.service.storage.ProjectStorageService;
import edu.unc.cscc.views.service.storage.UserStorage;
import edu.unc.cscc.views.service.storage.VACLService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

/* FIXME - this API needs some thought as to how to support large datasets */

@Api(tags = "Datasets")
@Validated
@RestController
public class DataSetController
{
	
	/**
	 * Maximum file size for a dataset (in bytes).
	 */
	private static final int		MAX_FILE_SIZE = 1024 * 1024 * 16;
	
	
	@Autowired
	private DataSetStorage			service;
	@Autowired
	private ChartStorageService		chartService;
	
	@Autowired
	private ProjectStorageService	projectService;

	@Autowired
	PreloadedUserConfig preloadedUserConfig;
	
	@Autowired
	private VSec					sec;
	
	@Autowired
	private VACLService				aclService;
	
	@Autowired
	private UserStorage				userStorage;
	
	
	@PreAuthorize("@vsec.projectAllowed(#projectID, 'read')")
	@RequestMapping(value = "/projects/{project-id}/datasets/search",
					method = RequestMethod.GET,
					produces = "application/json")
	@ApiOperation(value = "search for datasets",
					notes = "Search for datasets matching a given query, or "
							+ "if no query is provided, list all datasets within "
							+ "a specified project.  In order for this operation "
							+ "to be successful, the requesting user must have "
							+ "the ability to read the specified project.  "
							+ "Only datasets readable by the requesting user will "
							+ "be returned.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "search completed successfully", 
						response = SearchResults.class),
		@ApiResponse(code = 404, message = "project not found")
	})
	public ResponseEntity<?>
	search(@AuthenticationPrincipal User user,
			@PathVariable("project-id")
			@Min(0)
			@ApiParam("ID of the project within which to search")
				int projectID,
				
			@RequestParam("query")
			@ApiParam("search term/query; omitted to list all datasets")
				final String term,
				
			@RequestParam(value = "with-user-acl", required = false, defaultValue = "true")
			@ApiParam(value = "whether to include ACLs representing the requesting "
								+ "users permissions for the resulting datasets",
						required = false, defaultValue = "true")
				boolean withUserACL,
				
			@RequestParam(value = "sort-by", required = false)
			@Pattern(regexp = "^(identifier|description|content-type)$")
			@ApiParam(value = "how the results should be sorted",
						allowableValues = "identifier, description, content-type",
						defaultValue = "identifier")
				 final String sortKey,
				 
			@RequestParam (value = "descending", required = false, defaultValue = "false")
			@ApiParam(value = "true if the results should be sorted in descending "
								+ "order, false otherwise",
						required = false, defaultValue = "false")
				final boolean descending,
				
			@RequestParam(value = "limit", required = false, defaultValue = "100") 
			@Min(1) @Max(500)
			@ApiParam(value = "maximum number of results", required = false,
						defaultValue = "100")
				final int limit,
			
			@RequestParam(value = "offset", required = false, defaultValue = "0")	
			@Min(0)
			@ApiParam(value = "starting offset of results; zero-indexed", 
						required = false, defaultValue = "0")
				final int offset)
	{
		SortDataSetBy sort = null;
		
		if ("description".equals(sortKey))
		{
			sort = SortDataSetBy.DESCRIPTION;
		}
		else if ("content-type".equals(sortKey))
		{
			sort = SortDataSetBy.CONTENT_TYPE;
		}
		else
		{
			sort = SortDataSetBy.IDENTIFIER;
		}
		
		final Project project = this.projectService.load(projectID);
		
		if (project == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		final List<Integer> ids = 
				this.service.search(user, term, project, limit, offset, sort, descending);
		
		final int count = this.service.count(user, term, project);
		
		final Map<Integer, DataSet> sets = 
				this.service.loadShallow(ids);
		
		final List<DataSet> orderedSets = ids.stream()
											.map(sets :: get)
											.collect(Collectors.toList());
		
		final Map<Integer, VACLDTO> acls = withUserACL ? new HashMap<>() : null;
		
		if (withUserACL)
		{
			/* TODO - batch load! */
			sets.values().stream()
				.forEach(set -> 
					acls.put(set.id(), 
							VACLDTO.create(this.aclService
												.load(new DataSetOID(set.id()), user), 
											f -> user)));
						
		}
		
		
		return new ResponseEntity<>(new SearchResults<>(orderedSets, count, acls), HttpStatus.OK);
	}
	
	
	@PreAuthorize("@vsec.datasetAllowed(#datasetID, 'read')")
	@RequestMapping(value = "/datasets/{id}/charts",
						method = RequestMethod.GET,
						produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "retrieve the charts which depend on a specified dataset",
					notes = "Retrieve all charts which depend on a specified dataset. "
							+ "In order for this operation to be successful, the "
							+ "requesting user must be able to read the specified "
							+ "dataset.  Only those charts readable by the requesting "
							+ "user will be present in the results.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "charts loaded successfully", 
						responseContainer = "List", 
						response = Chart.class),
		@ApiResponse(code = 404, message = "dataset not found")
	})
	public ResponseEntity<?>
	findCharts(@PathVariable("id")
				@Min(0)
				@ApiParam("ID of the dataset for which to find charts")
				final int datasetID)
	{
		
		if (! this.service.exists(datasetID))
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		final Set<Integer> ids = 
				new HashSet<>(this.chartService.findByDataSet(datasetID));
		
		
		if (ids.isEmpty())
		{
			return new ResponseEntity<>(Collections.emptySet(), HttpStatus.OK);
		}
		
		final Map<Integer, Chart> charts = this.chartService.load(ids);
		
		return new ResponseEntity<>(charts.values(), HttpStatus.OK);
	}
	
	@PreAuthorize("@vsec.datasetAllowed(#id, 'read')")
	@RequestMapping(method = RequestMethod.GET,
					value = "/datasets/{id}/shallow",
					produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "shallowly retrieve a specified dataset",
					notes = "Retrieve a dataset (not including the data) "
							+ "specified by ID.  In order for this operation "
							+ "to be successful, the requesting user must "
							+ "be able to read the requested dataset.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "dataset successfully loaded",
						response = DataSet.class),
		@ApiResponse(code = 404, message = "dataset not found")
	})
	public ResponseEntity<?>
	getShallow(@PathVariable("id")
				@Min(0) 
				@ApiParam("ID of the dataset to retrieve")
					int id)
	{
		
		DataSet set = this.service.loadShallow(id);
		
		if (set == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(set, HttpStatus.OK);
	}
	
	/* also secured programmatically for read->dataset check */
	@PreAuthorize("@vsec.projectAllowed(#projectID, 'read')")
	@RequestMapping(method = RequestMethod.GET,
					value = "/projects/{project-id}/datasets/by-identifier/{identifier}")
	@ApiOperation(value = "retrieve the data payload of a dataset by its identifier",
					notes = "Retrieve the data from a dataset identified by the "
							+ "given identifier within a specified project.  "
							+ "The type of the data will be reflected in the "
							+ "response's Content-Type header.  "
							+ "In order for this "
							+ "operation to succeed, the requesting user must "
							+ "have the ability to read the specified project "
							+ "AND the ability to read the requested dataset.",
					responseHeaders = {
							@ResponseHeader(name = "Content-Type", 
									description = "content type of the dataset's data")
					}
	)
	@ApiResponses({
		@ApiResponse(code = 200, message = "dataset loaded successfully"),
		@ApiResponse(code = 404, message = "project/dataset not found")
	})
	public void
	getByIdentifier(@PathVariable("project-id")
					@Min(0)
					@ApiParam("ID of the project from which to load dataset")
						int projectID,
					@PathVariable("identifier")
					@NotNull
					@Size(min = 1)
					@ApiParam(value = "identifier of the dataset to load",
								allowEmptyValue = false)
						String identifier,
					HttpServletResponse response) 
	throws IOException
	{
		Project project = this.projectService.load(projectID);
		
		if (project == null)
		{
			response.setStatus(HttpStatus.NOT_FOUND.value());
			return;
		}
		
		if (! this.service.exists(identifier, project))
		{
			response.setStatus(HttpStatus.NOT_FOUND.value());
			return;
		}
		
		DataSet ds = this.service.loadByIdentifier(identifier, project, true);
		
		if (! this.sec.datasetAllowed(ds.id(), VSec.PERM_READ))
		{
			throw new AccessDeniedException("Dataset not readable by current principal");
		}
		
		response.setContentType(ds.contentType());
		
		this.service.loadData(ds.id(), response.getOutputStream());
	}
	
	/* dataset secured via service call */
	@PreAuthorize("@vsec.projectAllowed(#projectID, 'read')")
	@RequestMapping(method = RequestMethod.GET,
					value = "/projects/{project-id}/datasets/by-identifier/{identifier}/shallow",
					produces = "application/json")
	@ApiOperation(value = "shallowly retrieve a dataset by identifier",
					notes = "Shallowly load a dataset with a given identifier "
							+ "within a specified project.  In order for "
							+ "this operation to succeed, the requesting "
							+ "user must be able to read the specified project "
							+ "and the requested dataset.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "dataset loaded successfully",
						response = DataSet.class),
		@ApiResponse(code = 404, message = "project/dataset not found",
						response = NamedError.class)
	})
	public ResponseEntity<?>
	getByIdentifierShallow(@PathVariable("project-id") 
							@Min(0)
							@ApiParam("ID of project from which to load dataset")
								int projectID,
							@PathVariable("identifier") 
							@Size(min = 1) @NotNull
							@ApiParam(value = "identifier of dataset to load",
										allowEmptyValue = false)
								final String identifier) 
	throws IOException
	{
		Project project = this.projectService.load(projectID);
		
		if (project == null)
		{
			return new ResponseEntity<>(new NamedError("project-not-found",
											"No project with an ID of " 
											+ projectID + " was found."), 
										HttpStatus.NOT_FOUND);
		}
		
		if (! this.service.exists(identifier, project))
		{
			return new ResponseEntity<>(new NamedError("dataset-not-found",
						String.format("No dataset with identifier '%s' was "
										+ "found within the specified project "
										+ "(ID %d).", identifier, project.id())), 
												HttpStatus.NOT_FOUND);
		}
		
		DataSet ds = this.service.loadByIdentifier(identifier, project, true);
		
		return new ResponseEntity<>(ds, HttpStatus.OK);
	}
	
	@PreAuthorize("@vsec.datasetAllowed(#id, 'read')")
	@RequestMapping(method = RequestMethod.GET,
					value = "/datasets/{id}")
	@ApiOperation(value = "retrieve the data payload of a dataset with a given ID",
					notes = "Retrieve the data from a dataset with a given ID. "
							+ "The type of the data will be reflected in the "
							+ "response's Content-Type header.  "
							+ "In order for this "
							+ "operation to succeed, the requesting user must "
							+ "have the ability to read the requested dataset.",
					responseHeaders = {
							@ResponseHeader(name = "Content-Type", 
									description = "content type of the dataset's data")
					}
	)
	@ApiResponses({
		@ApiResponse(code = 200, message = "dataset loaded successfully"),
		@ApiResponse(code = 404, message = "dataset not found")
	})
	public void
	getData(@PathVariable("id")
			@Min(0)
			@ApiParam("ID of the dataset to read")
				final int id, 
			HttpServletResponse response) 
	throws IOException
	{
		if (! this.service.exists(id))
		{
			response.setStatus(HttpStatus.NOT_FOUND.value());
			return;
		}
		
		final DataSet ds = this.service.loadShallow(id);
		
		response.setContentType(ds.contentType());
		
		this.service.loadData(id, response.getOutputStream());
	}
	
	/* TODO - add JSON add/update methods (base64 data); maybe multipart/related too? */
	
	@PreAuthorize("@vsec.projectAllowed(#projectID, 'read') "
					+ " and @vsec.projectAllowed(#projectID, 'create-datasets')")
	@RequestMapping(path = "/projects/{project-id}/datasets/", 
					method = RequestMethod.POST,
					consumes = "multipart/form-data",
					produces = "application/json")
	@ApiOperation(value = "create a new dataset",
					notes = "Create a new dataset within a specified project. "
							+ "The new dataset will have the given identifier and "
							+ "description, and have a data payload containing the "
							+ "contents of the submitted file.  In order for this "
							+ "operation to succeed, the requesting user must "
							+ "hold the 'create-datasets' permission on the "
							+ "project within which the dataset is to be created.")
	@ApiResponses({
		@ApiResponse(code = 200,
					message = "dataset created successfully; "
							+ "response reflects new dataset (sans data)",
						response = DataSet.class),
		@ApiResponse(code = 400, message = "dataset exceeds maximum size",
						response = NamedError.class),
		@ApiResponse(code = 404, message = "project does not exist"),
		@ApiResponse(code = 409, message = "dataset identifier already in use",
						response = NamedError.class)
	})
	@Transactional
	public ResponseEntity<?>
	addNewDataSet(@AuthenticationPrincipal User user,
					@PathVariable("project-id")
					@Min(0)
					@ApiParam("ID of the project within which to create dataset")
						final int projectID, 
						
					@RequestPart("identifier") 
					@Size(min = 1) @NotNull @Pattern(regexp = ".*\\w.*")
					@ApiParam(value = "identifier for the new dataset; must "
										+ "be unique within project",
								required = true, allowEmptyValue = false,
								example = "shiny-new-dataset-csv")
						final String identifier,
						
					@RequestPart(value = "description", required = false)
					@ApiParam(value = "human-friendly description of the new "
										+ "dataset; may be blank/omitted",
								example = "My shiny new dataset.")
						String description,
						
					@RequestPart("file")
					@ApiParam(value = "contents of the dataset (i.e. the "
									+ "actual data)",
								required = true, allowMultiple = false)
						final MultipartFile file) 
	throws IOException
	{
		/* since we can't set a default for @RequestPart args, do so here */
		if (StringUtils.isEmpty(description))
		{
			description = "";
		}
		
		if (file != null && file.getSize() > MAX_FILE_SIZE)
		{
			return new ResponseEntity<>(
					new NamedError("max-file-size-exceeded",
							String.format("File of size %d bytes exceeds maximum "
										+ "size of %d bytes.",
										file.getSize(), MAX_FILE_SIZE)), 
					HttpStatus.BAD_REQUEST);
		}
		
		Project project = this.projectService.load(projectID);
		
		if (project == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		if (this.service.exists(identifier, project))
		{
			return new ResponseEntity<>(new NamedError("identifier-exists"), 
											HttpStatus.CONFLICT);
		}
		
		InputStream is = file.getInputStream();
		
		String contentType = determineContentType(is);
		
		is.close();
		
		is = file.getInputStream();
				
		DataSet set = new DataSet(project, identifier, description, contentType, new byte[0]);
		
		set = this.service.save(set);
		
		/* add ACL entry */
		final DataSetOID oid = new DataSetOID(set.id());
		final Collection<VACLEntry> entries = new ArrayList<>();
		
		addResourceACLEntries(user, set.id().intValue(), null, null, Boolean.TRUE, entries);
		preloadedUserConfig.getDefaultResourceAdmin().forEach((key,value) 
				-> addResourceACLEntries(user, oid.getIdentifier(), key, value, Boolean.FALSE, entries));

		
		VACL acl = new VACL(oid, entries, false);
		
		acl = this.aclService.save(acl);
		
		/* save the data */
		
		this.service.saveData(set.id(), is);
		
		return new ResponseEntity<>(set.data(null),	HttpStatus.OK);
	}
	
	private void addResourceACLEntries(final User currentUser, int dsId, 
			String preloadedUserName, 
			String preloadedUserContext, Boolean isCreator, Collection<VACLEntry> entries) {
		
		User sUser = null;
		
		if(isCreator) {
			sUser = currentUser;
		} else if(!currentUser.getUsername().equalsIgnoreCase(preloadedUserName)) {
			sUser = userStorage.find(preloadedUserName, preloadedUserContext);
		}
		if(sUser != null)
		{
			final VSid sSid = new VSid(sUser);
			Stream.of(VPermission.values())
				.filter(perm -> perm.applicableTo(new DataSetOID(dsId)))
				.map(perm -> new VACLEntry(sSid, perm))
				.forEach(entries :: add);
		}
	}
	
	@RequestMapping(path = "/projects/{project-id}/datasets/{id}", 
					method = RequestMethod.PUT,
					consumes = "multipart/form-data",
					produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("@vsec.datasetAllowed(#setID, 'read') "
					+ " and @vsec.datasetAllowed(#setID, 'write') "
					+ " and @vsec.projectAllowed(#projectID, 'read')")
	@Transactional
	@ApiOperation(value = "update an existing dataset",
					notes = "Update an existing dataset within a specified project. "
							+ "The dataset will be updated with the given identifier and "
							+ "description, and have a data payload containing the "
							+ "contents of the submitted file.  In order for this "
							+ "operation to succeed, the requesting user must "
							+ "be able to read AND write the dataset in question, "
							+ "as well as read the project within which the dataset "
							+ "is being created.")
	@ApiResponses({
		@ApiResponse(code = 200,
						message = "dataset updated successfully; "
								+ "response reflects updated state (sans data)",
						response = DataSet.class),
		@ApiResponse(code = 400, message = "dataset exceeds maximum size",
						response = NamedError.class),
		@ApiResponse(code = 404, message = "dataset/project not found"),
		@ApiResponse(code = 409, message = "dataset identifier already in use",
						response = NamedError.class)
	})
	public ResponseEntity<?> 
	updateSet(@PathVariable("id")
				@Min(0)
				@ApiParam("ID of dataset to update")
					final int setID,
				@PathVariable("project-id")
				@Min(0)
				@ApiParam("ID of project within which to update dataset")
					final int projectID,

				@RequestPart("identifier") 
				@Size(min = 1) @NotNull @Pattern(regexp = ".*\\w.*")
				@ApiParam(value = "new identifier for the dataset; must "
									+ "be unique within project",
							required = true, allowEmptyValue = false,
							example = "shiny-updated-dataset-csv")
					final String identifier,
				
				@RequestPart(value = "description", required = false)
				@ApiParam(value = "new human-friendly description of the "
									+ "dataset; may be blank/omitted",
							example = "My shiny updated dataset.")
					String description,
					
				@RequestPart(value = "file", required = false)
				@ApiParam(value = "contents of the dataset (i.e. the "
								+ "actual data); omit to keep current contents",
							required = false, allowMultiple = false)
					final MultipartFile file)
	throws IOException 
	{
		Project project = this.projectService.load(projectID);
		
		if (project == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		DataSet set = this.service.load(setID);
		DataSet existing = this.service.loadByIdentifier(identifier, project, true);
		
		/* identifier conflict */
		if (existing != null
			&& ! existing.id().equals(set.id()))
		{
			return new ResponseEntity<>(new NamedError("identifier-exists"), 
										HttpStatus.CONFLICT);
		}
		
		InputStream is = null;
		
		if (file != null)
		{
			if (file.getSize() > MAX_FILE_SIZE)
			{
				return new ResponseEntity<>(
						new NamedError("max-file-size-exceeded",
								String.format("File of size %d bytes exceeds maximum "
											+ "size of %d bytes.",
											file.getSize(), MAX_FILE_SIZE)), 
						HttpStatus.BAD_REQUEST);
			}
			
			is = file.getInputStream();
			String contentType = determineContentType(is);
			
			is.close();
			
			is = file.getInputStream();
			

			set = set.data(null)
					.contentType(contentType);
		}
		
		if (set == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		
		
		set = set.identifier(identifier)
				.description(description)
				.project(project);
		
		set = this.service.save(set);
		
		if (is != null)
		{
			this.service.saveData(set.id(), is);
		}
		
		return new ResponseEntity<>(set.data(null), HttpStatus.OK);
	}
	
	
	@RequestMapping(path = "/datasets/{id}",
					method = RequestMethod.DELETE)
	@PreAuthorize("@vsec.datasetAllowed(#setID, 'write')")
	@ApiOperation(value = "delete a specified dataset",
					notes = "Delete a specified dataset.  This operation will "
							+ "only succeed if no charts depend on the "
							+ "specified dataset.  (This behavior may be "
							+ "overridden by the 'force' parameter.)  In "
							+ "order for this operation to succeed, the "
							+ "requesting user must have the 'write' permission "
							+ "on the specified dataset.",
					code = 204)
	@ApiResponses({
		@ApiResponse(code = 204, message = "deletion was successful",
						response = Void.class),
		@ApiResponse(code = 404, message = "dataset not found"),
		@ApiResponse(code = 409, 
						message = "dataset was in use and forced deletion was "
								+ "not specified; response indicates charts "
								+ "currently associated with the dataset",
						response = DataSetInUseError.class)		
	})
	public ResponseEntity<?>
	deleteSet(@PathVariable("id") 
				@Min(0)
				@ApiParam("ID of the dataset to delete")
					int setID,
				@RequestParam(value = "force", 
								defaultValue = "false",
								required = false) 
				@ApiParam(value = "true to force deletion even if charts "
									+ "depend on the dataset, false otherwise",
							defaultValue = "false", required = false)
					boolean force)
	{
		if (! this.service.exists(setID))
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		
		if (! force)
		{
			Collection<Integer> charts = this.chartService.findByDataSet(setID);
			
			if (! charts.isEmpty())
			{
				return new ResponseEntity<>(new DataSetInUseError(charts),
											HttpStatus.CONFLICT);
			}
		}
			
		this.service.delete(setID);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	/* additional security done programmatically since it depends on project,
	 * which isn't visible on call
	 */
	@PreAuthorize("@vsec.datasetAllowed(#id, 'read')")
	@RequestMapping(value = "/datasets/{id}/acl",
					method = RequestMethod.GET,
					produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "retrieve the ACL of a specified dataset",
		notes = "Retrieve the ACL of the specified dataset.  In order "
				+ "for this operation to succeed, the requesting user "
				+ "must be able to read the dataset.  "
				+ "Additionally, either the "
				+ "user ID must be omitted OR the user must be "
				+ "requesting ACL entries for their own user ID "
				+ "OR the user must have the 'manage-dataset-access-control' "
				+ "permission on the project to which the dataset belongs.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "ACL retrieved successfully",
				response = VACLDTO.class),
		@ApiResponse(code = 404, message = "dataset not found")
	})
	public ResponseEntity<?>
	getACL(@PathVariable("id")
			@Min(0)
			@ApiParam(value = "ID of the datset for which to get the ACL",
						required = true, example = "5")
				final int id,
			@RequestParam(value = "user-id", required = false)
			@Min(0)
			@ApiParam(value = "ID of the user for which the ACL is "
								+ "requested; if omitted the resulting ACL "
								+ "will contain either entries for all users "
								+ "(if the user holds the 'manage-dataset-access-control'"
								+ " permission for the dataset's project, OR "
								+ "entries corresponding to the requesting user",
						required = false, allowEmptyValue = false,
						example = "420")
				Integer userID,
			@AuthenticationPrincipal User principal)
	{
		DataSet ds = this.service.load(id);
		
		if (ds == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		if (! this.sec.projectAllowed(ds.project().id(), 
				VPermission.MANAGE_DATASET_ACLS))
		{
			if (! principal.id().equals(userID) && userID != null)
			{
				throw new AccessDeniedException(
							"Unable to retrieve ACL for user which is not "
							+ "principal without " 
							+ VPermission.MANAGE_DATASET_ACLS.identifier() 
							+ " permission");
			}
		}
		
		VACL acl;
		
		if (userID != null)
		{
			User user = this.userStorage.load(userID);
			
			if (user == null)
			{
				acl = new VACL(new DataSetOID(id), Collections.emptySet(), true);
			}
			else
			{
				acl = this.aclService.load(new DataSetOID(id), user);
			}
			
			
		}
		else
		{
			if (this.sec.projectAllowed(ds.project().id(), VPermission.MANAGE_DATASET_ACLS))
			{
				acl = this.aclService.load(new DataSetOID(id));
			}
			else
			{
				acl = this.aclService.load(new DataSetOID(id), principal);
			}
		}
		
		Assert.notNull(acl, "saved ACL was null");
		
		Map<Integer, User> users = 
				this.userStorage
					.load(acl.represents(VSid.class)
							.stream()
							.map(s -> s.userID())
							.collect(Collectors.toSet()));
		
		
		return new ResponseEntity<>(VACLDTO.create(acl, users :: get), 
									HttpStatus.OK);
	}
	
	@PreAuthorize("@vsec.datasetAllowed(#id, 'write')")
	@RequestMapping(value = "/datasets/{id}/acl",
					method = RequestMethod.PUT,
					consumes = MediaType.APPLICATION_JSON_VALUE,
					produces = "*/*")
	@ApiOperation(value = "update the ACL for a specified dataset",
		notes = "Update the ACL entries for the specified dataset. "
				+ "The dataset's ACL will be replaced with the "
				+ "submitted entries.  In order for this operation "
				+ "to succeed, the requesting user must have both "
				+ "the 'manage-dataset-access-control' permission on the "
				+ "project to which the dataset belongs AND the ability "
				+ "to modify the dataset in question.",
		code = 204)
	@ApiResponses({
		@ApiResponse(code = 204, message = "ACL updated successfully"),
		@ApiResponse(code = 404, message = "dataset not found")
	})
	public ResponseEntity<?>
	saveACL(@PathVariable("id")
			@Min(0)
			@ApiParam(value = "ID of the dataset for which to update the ACL",
						example = "42", allowEmptyValue = false)
				final int id, 
			@RequestBody @Valid
			@ApiParam(value = "new ACL for the dataset")
				final VACLDTO dto)
	{
		DataSet ds = this.service.load(id);
		
		if (ds == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		if (! this.sec.projectAllowed(ds.project().id(), 
										VPermission.MANAGE_DATASET_ACLS))
		{
			return new ResponseEntity<>(new NamedError("cannot-manage-dataset-acls"), 
										HttpStatus.FORBIDDEN);
		}
		
		VACL acl = new VACL(new DataSetOID(id), dto.toACLEntries(), dto.partial());
		
		acl = this.aclService.save(acl);
		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	
	private static final String
	determineContentType(InputStream is)
	throws IOException
	{
		try (BufferedInputStream bis = 
				new BufferedInputStream(is))
		{
		    AutoDetectParser parser = new AutoDetectParser();
		    Detector detector = parser.getDetector();
		    Metadata md = new Metadata();
		    md.add(Metadata.RESOURCE_NAME_KEY, "data");
		    org.apache.tika.mime.MediaType mediaType = detector.detect(bis, md);
		    return mediaType.toString();
		}
	}
	
	@ApiModel(value = "dataset-in-use-error", 
				description = "named error produced when deletion of an "
								+ "in-use dataset is attempted")
	private static class DataSetInUseError
	extends NamedError
	{

		private final Collection<Integer>	chartIDs;
		
		public DataSetInUseError(Collection<Integer> chartIDs)
		{
			super("dataset-in-use", 
					String.format("Dataset in use by charts [%s]",
							chartIDs.stream()
									.map(i -> i.toString())
									.collect(Collectors.joining(","))));
			this.chartIDs = new ArrayList<>(chartIDs);
		}
		
		@JsonGetter("chart-ids")
		@ApiModelProperty(value = "IDs of the charts on which the dataset depends",
							required = true)
		public Collection<Integer>
		chartIDs()
		{
			return Collections.unmodifiableCollection(this.chartIDs);
		}
		
		
		
	}
}
