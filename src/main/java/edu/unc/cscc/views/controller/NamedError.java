package edu.unc.cscc.views.controller;

import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonGetter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * A simple representation of a "named" error, i.e. an error with a unique
 * key for which front-end code may have special handling.  (Ex: a user forgot
 * a required property).  Not intended for unexpected errors (ex: database 
 * connection lost.)
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@ApiModel(value = "error", 
			description = "A uniquely-identifiable error condition, typically "
					+ "intended for special UI handling.")
public class NamedError
{
	private final String		name;
	private final String		message;
	
	public NamedError(String name)
	{
		this(name, "");
	}
	
	public NamedError(String name, String message)
	{
		Assert.notNull(name, "name must be non-null");
		Assert.notNull(message, "message must be non-null");
		this.name = name;
		this.message = message;
	}
	
	/**
	 * Get the name/key of the error.
	 * 
	 * @return error name/key, not <code>null</code>
	 */
	@JsonGetter("error")
	@ApiModelProperty(value = "name/key of the error", required = true, 
						example = "error-name")
	public String
	name()
	{
		return this.name;
	}
	
	/**
	 * Get a detailed message describing the error.
	 * 
	 * @return message, not <code>null</code>, may be empty
	 */
	@JsonGetter("message")
	@ApiModelProperty(value = "a detailed message describing the error",
						required = false, 
						example = "An error occurred when doing something.")
	public String
	message()
	{
		return this.message;
	}
}
