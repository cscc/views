package edu.unc.cscc.views.controller;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.exception.Auth0Exception;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import edu.unc.cscc.views.exception.UserExistsException;
import edu.unc.cscc.views.model.security.ChangeAuthSourceRequest;
import edu.unc.cscc.views.model.security.LocalCredentials;
import edu.unc.cscc.views.model.security.User;
import edu.unc.cscc.views.model.security.User.ViewsRole;
import edu.unc.cscc.views.service.Auth0CredentialsSource;
import edu.unc.cscc.views.service.Auth0Service;
import edu.unc.cscc.views.service.LocalUsernamePasswordCredentialsSource;
import edu.unc.cscc.views.service.UsernamePasswordCredentialsSourceManager;
import edu.unc.cscc.views.service.storage.LocalCredentialStorageService;
import edu.unc.cscc.views.service.storage.UserStorage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RequestMapping("/users")
@RestController
@Api(tags = {"Users"})
@Validated
public class UserController
{
	
	@Autowired
	private UserStorage									userService;
	
	@Autowired
	private LocalCredentialStorageService				localCredentialService;
	
	@Autowired
	private UsernamePasswordCredentialsSourceManager	credManager;

	@Autowired
	private Auth0Service                                auth0Service;

	@Value("${auth.protected-users}")
	private String[] 									protectedUsers;

	@Value("${auth.frontendUri}")
	private String										frontendUri;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@RequestMapping(value = "/current",
					method = RequestMethod.GET,
					produces = "application/json")
	@ApiOperation(value = "get the user whose authentication accompanied the request",
					notes = "Get the user who is authenticated for the duration of the "
							+ "request, i.e. the user corresponding to the authentication "
							+ "information that was submitted with the request.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "the user was retrieved successfully"),
		@ApiResponse(code = 403, 
					message = "the request did not contain valid authentication information")
	})
	public User
	current(@AuthenticationPrincipal final User principal)
	{
		return principal;
	}
	
	
	@PreAuthorize("hasAuthority('manage_users') or #id == principal.id()")
	@RequestMapping(method = RequestMethod.GET,
					value = "/{id}",
					produces = "application/json")
	@ApiOperation(value = "retrieve a user",
					notes = "Get the user with the given ID.  In order for this "
							+ "operation to succeed the requesting user MUST "
							+ "either have the 'manage_users' permission OR "
							+ "have the same ID as the user being requested.")
	public User
	byID(@AuthenticationPrincipal User principal, 
			@PathVariable("id") 
			@Min(0)
			@ApiParam(value = "ID of user to load")
			final int id)
	{
		return this.userService.load(id);
	}
	
	
	@PreAuthorize("hasAuthority('manage_users') "
				+ "and #id != principal.id()") /* suicide prevention line */
	@RequestMapping(method = RequestMethod.DELETE,
					value = "/{id}")
	@Transactional
	@ApiOperation(value = "delete a user",
					notes = "Delete the user with the given ID.  In order for "
							+ "this operation to succeed, the requesting user "
							+ "MUST have the 'manage_users' permission AND "
							+ "MUST NOT have the same ID as the user to be "
							+ "deleted.",
					code = 204)
	@ApiResponses({
		@ApiResponse(code = 204, message = "deletion was successful", 
						response = Void.class),
		@ApiResponse(code = 404, message = "the specified user was not found")
	})
	public ResponseEntity<?>
	delete(@PathVariable("id") 
			@Min(0) 
			@ApiParam(value = "ID of user to delete")
			int id)
	{
		
		if (! this.userService.exists(id))
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		this.userService.delete(id);
		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	/* further secured via service layer */
	@PreAuthorize("hasAuthority('manage_users') or "
			+ "#id == principal.id()")
	@RequestMapping(method = RequestMethod.PUT,
					value = "/{id}",
					consumes = "application/json",
					produces = "application/json")
	@Transactional
	@ApiOperation(value = "update a user",
					notes = "Update a user's properites to reflect those "
							+ "supplied. "
							+ "In order for this operation to succeed the "
							+ "requesting user MUST either have the same ID "
							+ "as the user being updated OR have the "
							+ "'manage_users' role.")
	@ApiResponses({
		@ApiResponse(code = 200, 
					message = "user was updated succesfully; response reflects updated user",
					response = User.class),
		@ApiResponse(code = 404, message = "user with specified ID not found")
	})
	public ResponseEntity<?>
	updateUser(@PathVariable("id") 
					@Min(0)
					@ApiParam("ID of user to update")
					int id, 
					@RequestBody 
					@Valid
					@ApiParam("updated user properties")
					final UserUpdateDTO dto)
	{
		final User existing = this.userService.load(id);
		
		if (existing == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		User saved = existing.name(dto.name == null ? "" : dto.name)
							.active(dto.active)
							.lastModified(Instant.now())
							.roles(Arrays.asList(dto.roles));
		
		saved = this.userService.save(saved);
		
		if (saved.getCredentialSourceID()
				.equals(LocalUsernamePasswordCredentialsSource.IDENTIFIER))
		{
			if (dto.password != null && ! dto.password.trim().isEmpty())
			{
				this.localCredentialService
					.save(this.localCredentialService
								.create(saved, dto.password));
			}
		}
		
		return new ResponseEntity<>(saved, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('manage_users')")
	@RequestMapping(method = RequestMethod.POST,
					value = "/changeToAuth0",
					consumes = "application/json",
					produces = "application/json")
	@Transactional
	@ApiOperation(value = "change credential source to auth0",
					notes = "Change a user to use auth0 as a credential source, creating their account if needed"
					    	+ "Will fail if a user account with the specifed user already exists")
	@ApiResponses({
		@ApiResponse(code = 200, 
					message = "user was updated succesfully; response reflects updated user",
					response = User.class),
		@ApiResponse(code = 400, message = "user is not allowed to have its auth source changed"),
		@ApiResponse(code = 404, message = "user with specified ID not found"),
		@ApiResponse(code = 409, message = "different user with specified username found"),
		@ApiResponse(code = 424, message = "communication with auth0 failed")
	})
	public ResponseEntity<?>
	changeUserToAuth0(@RequestBody 
					@Valid
					@ApiParam("User's auth0 email and their views userID")
					final ChangeAuthSourceRequest request)
	{
		final User existing = this.userService.load(request.getUserId());
		
		if (existing == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} 

		for (int i = 0; i < protectedUsers.length; i++) {
			if (protectedUsers[i].equals(existing.getUsername())) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		}
		
		try {
			User result = userService.changeUserAuthSource(request.getUserId(), request.getEmail(), Auth0CredentialsSource.IDENTIFIER);
			if (!auth0Service.userWithEmailExists(request.getEmail())) {
				auth0Service.createNewUser(result);
			}
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (UserExistsException e) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		} catch (Auth0Exception | MessagingException e) {
			userService.save(existing); // Undo change because Auth0 may not have been created
			return new ResponseEntity<>(HttpStatus.FAILED_DEPENDENCY);
		}
	}
	
	@PreAuthorize("hasAuthority('manage_users') or "
				+ "#id == principal.id()")
	@RequestMapping(value = "/{id}/password",
					method = RequestMethod.PUT,
					consumes = "text/plain")
	@Transactional
	@ApiOperation(value = "change the password of a local user",
					notes = "Change the password of a local user.  This operation "
							+ "will fail if used for a user with a non-local "
							+ "credential source.  In order for this operation "
							+ "to succeed the requesting user MUST either have "
							+ "the same ID as the user for which the password "
							+ "is being changed OR have the 'manage_users' role.")
	@ApiResponses({
		@ApiResponse(code = 204, message = "password successfully changed"),
		@ApiResponse(code = 404, message = "user not found"),
		@ApiResponse(code = 400, message = "request was made for non-local user")
	})
	public ResponseEntity<?>
	changeLocalUserPassword(@AuthenticationPrincipal User principal, 
							@PathVariable("id") 
							@Min(0)
							@ApiParam("ID of user for which to change password")
							final int id, 
							@RequestBody 
							@Valid
							@Size(min = 1)
							@ApiParam("updated password") String password)
	{
		
		if (! this.userService.exists(id))
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		final User user = this.userService.load(id);
		
		if (user == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		if (! LocalUsernamePasswordCredentialsSource.IDENTIFIER
				.equals(user.getCredentialSourceID()))
		{
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		LocalCredentials creds = this.localCredentialService.create(user, password);
		
		this.localCredentialService.save(creds);
		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@PreAuthorize("hasAuthority('manage_users')")
	@RequestMapping(value = "/remote",
					method = RequestMethod.POST,
					consumes = "application/json",
					produces = "application/json")
	@Transactional
	@ApiOperation(value = "create a remotely-authenticated user",
					notes = "Create a remote user, i.e. a user whose username "
							+ "and password will be handled/authenticated by "
							+ "an external credential source.  In order for this operation "
							+ "to succeed the requesting user MUST have the "
							+ "'manage_users' role AND the credential source specified "
							+ "must exist for the running instance.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "creation succeeded; created user returned",
						response = User.class),
		@ApiResponse(code = 404, message = "credential source does not exist"),
		@ApiResponse(code = 409, 
						message = "local user with specified username already exists",
						response = NamedError.class)	
	})
	public ResponseEntity<?>
	createRemoteUser(@RequestBody @Valid 
						@ApiParam("properties of user to create")
						RemoteUserCreationDTO dto)
	{
		/* check if the credential source exists */
		if (null == this.credManager.lookupSource(dto.credentialSourceIdentitier))
		{
			return ResponseEntity.notFound().build();
		}
		
		/* check if the user exists */
		
		if (this.userService.exists(dto.username, dto.credentialSourceIdentitier))
		{
			return new ResponseEntity<>(new NamedError("user-exists"), 
										HttpStatus.CONFLICT);
		}
		
		/* create the user */
		
		User user = new User(null, dto.username, 
								dto.credentialSourceIdentitier, 
								dto.name == null ? "" : dto.name, Instant.now(), dto.active,
								Arrays.asList(dto.roles));
		
		/* save 'em and dump it back */
		
		user = this.userService.save(user);
		
		return ResponseEntity.ok().body(user);
		
	}
	
	@PreAuthorize("hasAuthority('manage_users')")
	@RequestMapping(value = {"/", "/local"},
					method = RequestMethod.POST,
					consumes = "application/json",
					produces = "application/json")
	@Transactional
	@ApiOperation(value = "create an auth0 authenticated user",
					notes = "Create an auth0 user (i.e. a user whose username/password are handled by auth0). "
							+ "This requires the username to be their email. " 
							+ "In order for this operation "
							+ "to succeed the requesting user MUST have the "
							+ "'manage_users' role.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "creation succeeded; created user returned",
						response = User.class),
		@ApiResponse(code = 409, 
						message = "auth0 user with specified username already exists",
						response = NamedError.class),
	    @ApiResponse(code = 424,
		                message = "could not save user to auth0",
						response = NamedError.class)
	})
	public ResponseEntity<?>
	createLocalUser(@RequestBody @Valid 
					@ApiParam("properties of user to create")
					LocalUserCreationDTO dto)
	{
		
		/* check if the user exists */
		
		if (this.userService.exists(dto.username, 
									Auth0CredentialsSource.IDENTIFIER))
		{
			return new ResponseEntity<>(new NamedError("user-exists"), 
										HttpStatus.CONFLICT);
		}
		
		/* create the user */
		
		User user = new User(null, dto.username, 
								Auth0CredentialsSource.IDENTIFIER, 
								dto.name == null ? "" : dto.name, Instant.now(), dto.active,
								Arrays.asList(dto.roles));
		
		/* save 'em and dump it back */
		
		user = this.userService.save(user);
		try {
			auth0Service.createNewUser(user);
		} catch (Auth0Exception | MessagingException e) {
			logger.error("Could not save user to auth0", e);
			this.userService.delete(user.id());
			return new ResponseEntity<>(new NamedError("Could not save to Auth0"), HttpStatus.FAILED_DEPENDENCY);
		} catch (UserExistsException e) {
			logger.info("Created user that already exists, they can not log in to views.");
		}
		
		return new ResponseEntity<>(user, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/search",
					method = RequestMethod.GET,
					produces = "application/json")
	@ApiOperation(value = "search for or list users",
					notes = "Search for users (or if no search term given, list "
							+ "all users.)")
	@ApiResponses({
		@ApiResponse(code = 200, message = "search completed successfully")
	})
	public SearchResults<User>
	search(@AuthenticationPrincipal User user, 
			@RequestParam("query") 
			@ApiParam("search term; if omitted search will list all users")
			String searchTerm,
			@RequestParam(value = "limit",
							required = false, 
							defaultValue = "10")
			@Min(1) @Max(100)
			@ApiParam(value = "maximum number of results, starting from offset",
							defaultValue = "10")
			int limit,
			@RequestParam(value = "offset", 
							required = false,
							defaultValue = "0")
			@Min(0)
			@ApiParam(value = "offset (result number, zero-indexed) from which to "
						+ "start search",
						defaultValue = "0")
			int offset)
	{
		final String q = 
				(searchTerm == null 
					|| searchTerm.trim().isEmpty()) 
					? null	
					: searchTerm;

		List<Integer> ids = this.userService.search(user, q, limit, offset);

		Map<Integer, User> loaded = this.userService.load(ids);

		List<User> users = 
				ids.stream()
				.map(loaded :: get)
				.collect(Collectors.toList());
		
		return new SearchResults<>(users, this.userService.count(user, q));
	}
	
	/* user model changes done through DTO for ease of future extension */
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	@ApiModel("user-update-dto")
	private static class UserUpdateDTO
	{
		@JsonProperty(value = "name", defaultValue = "")
		@ApiModelProperty(value = "display name of user",
							example = "Sample User")
		public String			name;
		
		@JsonProperty("password")
		@ApiModelProperty(value = "(plaintext) password for user; will be "
								+ "ignored for non-local users",
							example = "hunter2",
							required = false)
		@Size(min = 1)
		public String			password;
		
		@JsonProperty(value = "active", defaultValue = "true")
		@ApiModelProperty(value = "whether the user is active or inactive")
		public boolean			active = true;
		
		@JsonProperty("roles")
		@ApiModelProperty(value = "roles held by the user",
							/* workaround for swagger-core issue #2352,
							 * see https://github.com/swagger-api/swagger-core/issues/2352
							 */
							allowableValues = "role_anonymous, role_all_users, "
									+ "manage_users, create_projects, "
									+ "manage_project_acls, superuser",
							dataType = "string" /* end of workaround */)
		public ViewsRole[]		roles = new ViewsRole[0];

		
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	@ApiModel("local-user-update-dto")
	private static class LocalUserCreationDTO
	extends UserUpdateDTO
	{
		@JsonProperty("username")
		@ApiModelProperty(value = "Auth0 email of the user",
							example = "user@example.com",
							required = true)
		@Size(min = 1)
		public String			username;
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	@ApiModel("remote-user-update-dto")
	private static class RemoteUserCreationDTO
	extends UserUpdateDTO
	{
		@JsonProperty("username")
		@Size(min = 1)
		@NotNull
		@ApiModelProperty(value = "username of the user",
							example = "example_user",
							required = true)
		public String			username;
		
		@JsonProperty("credential-source")
		@Size(min = 1)
		@NotNull
		@ApiModelProperty(value = "ID of the credential source used to "
								+ "authenticate the user",
							example = "sample-ldap-source",
							required = true)
		public String			credentialSourceIdentitier;
	}
	
}
