package edu.unc.cscc.views.controller;

import java.nio.charset.Charset;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.unc.cscc.views.config.PreloadedUserConfig;
import edu.unc.cscc.views.model.Chart;
import edu.unc.cscc.views.model.ChartResource;
import edu.unc.cscc.views.model.DataSet;
import edu.unc.cscc.views.model.Project;
import edu.unc.cscc.views.model.security.ChartOID;
import edu.unc.cscc.views.model.security.User;
import edu.unc.cscc.views.model.security.VACL;
import edu.unc.cscc.views.model.security.VACLDTO;
import edu.unc.cscc.views.model.security.VACLEntry;
import edu.unc.cscc.views.model.security.VPermission;
import edu.unc.cscc.views.model.security.VSid;
import edu.unc.cscc.views.service.VSec;
import edu.unc.cscc.views.service.storage.ChartStorageService;
import edu.unc.cscc.views.service.storage.DataSetStorage;
import edu.unc.cscc.views.service.storage.ProjectStorageService;
import edu.unc.cscc.views.service.storage.ResourceService;
import edu.unc.cscc.views.service.storage.UserStorage;
import edu.unc.cscc.views.service.storage.VACLService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Services all browser requests for charts.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@RestController
@Api(tags = {"Charts"})
@Validated
public class ChartController
{

	@Autowired
	private ChartStorageService		chartStorage;
	@Autowired
	private ProjectStorageService	projectService;
	@Autowired
	private ResourceService			resourceService;
	@Autowired
	private DataSetStorage			dsService;
	@Autowired
	PreloadedUserConfig preloadedUserConfig;
	
	@Autowired
	private VSec					sec;
	
	@Autowired
	private VACLService				aclService;
	
	@Autowired
	private UserStorage				userStorage;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());	
	
	/* move is supported by PUTing an existing chart (w/ existing ID) 
	 * into a project to which it currently doesn't belong
	 */
	@RequestMapping(value = "/projects/{project-id}/charts/{id}",
					method = RequestMethod.PUT,
					consumes = "application/json",
					produces = "application/json")
	@PreAuthorize("@vsec.chartAllowed(#id, @vsec.PERM_WRITE)")
	@ApiOperation(value = "update (or move) a chart",
					notes = 
						"Update a chart, replacing the current chart with "
						+ "the specified ID with the given chart.  Moving "
						+ "a chart between projects may be accomplished by "
						+ "PUTing a chart to a project to which it does not "
						+ "currently belong.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "The update/move was successful.",
					response = ChartDTO.class),
		@ApiResponse(code = 400, 
						message = 
							"The chart specified a dependency on a dataset "
							+ "which does not exist.",
						response = NamedError.class),		
		@ApiResponse(code = 404, message = "No such chart/project exists.", 
						response = void.class),
		@ApiResponse(code = 409, 
						message = "The updated chart was rejected because its name "
								+ "conflicts with an existing chart in the project.",
						response = NamedError.class)
	})
	public ResponseEntity<?>
	update(@PathVariable("id") 
			@ApiParam(value = "ID of the chart to update/move", required = true)
			@Min(0)
				int id, 
			@PathVariable("project-id") 
			@ApiParam(value = "ID of the project to which the chart should belong",
						required = true) 
				int projectID,
			@RequestBody 
			@Valid
			@ApiParam(value = "updated chart", required = true, name = "chart")
				final ChartDTO dto)
	{
		Chart chart = this.chartStorage.load(id);
		
		if (chart == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		Project project = this.projectService.load(projectID);
		
		if (project == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		Integer existingID = this.chartStorage.findID(dto.name, project);
		
		if (existingID != null 
			&& ! chart.id().equals(existingID))
		{
			return new ResponseEntity<>(new NamedError("name-exists"),
										HttpStatus.CONFLICT);
		}
		
		/* check that the required datasets exist, and load them so that 
		 * we can associate them with the chart
		 */
		Map<Integer, DataSet> sets = this.dsService.load(dto.requiredDatasets);
		
		if (sets.size() != dto.requiredDatasets.size())
		{
			return new ResponseEntity<>(new NamedError("dataset-does-not-exist"), 
											HttpStatus.BAD_REQUEST);
		}
		
		chart = chart.name(dto.name)
						.description(dto.description)
						.project(project)
						.datasets(sets.values());
		
		chart = this.chartStorage.save(chart);
		
		return new ResponseEntity<>(chart, HttpStatus.OK);
	}
	
	
	@RequestMapping(path = "/projects/{project-id}/charts/", 
					method = RequestMethod.POST,
					consumes = "application/json",
					produces = "application/json")
	@ApiOperation(value = "create a new chart",
					notes = "Create a new chart beneath a specified project.  "
							+ "The user which creates the chart will be granted "
							+ "read and write permissions as part of the creation "
							+ "process.")
	@Transactional
	public ResponseEntity<?>
	create(@PathVariable("project-id")
			@Min(0) 
			@ApiParam("ID of the project within which to create the chart")
			final int projectID,
			@AuthenticationPrincipal final User user,
			@RequestBody @Valid 
			@ApiParam(value = "new chart", required = true)
				final ChartDTO dto)
	{
		final String MAIN_RES = "main.js";
		
		/* check that the project exists */
		Project project = this.projectService.load(projectID);
		
		if (project == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		
		/* check that the identifier doesn't exist yet */
		
		
		if (this.chartStorage.exists(dto.name, project))
		{
			return new ResponseEntity<>(new NamedError("name-exists"), 
										HttpStatus.CONFLICT);
		}
		
		if (dto.mainResource == null 
			|| dto.mainResource.trim().isEmpty())
		{
			return new ResponseEntity<>(new NamedError("missing-main-resource"),
										HttpStatus.BAD_REQUEST);
		}
		
		/* check that the required datasets exist, and load them so that 
		 * we can associate them with the chart
		 */
		Map<Integer, DataSet> sets = this.dsService.load(dto.requiredDatasets);
		
		if (sets.size() != dto.requiredDatasets.size())
		{
			return new ResponseEntity<>(new NamedError("dataset-does-not-exist"), 
											HttpStatus.BAD_REQUEST);
		}
		
		
		/* save chart, then resource */
		
		final Chart chart = 
				this.chartStorage
						.save(new Chart(null, user, project, dto.name, 
								dto.description, Instant.now(), 
								Instant.now(), MAIN_RES,
								sets.values()));
		
		ChartResource res = 
				new ChartResource(null, MAIN_RES, chart, 
									dto.mainResource.getBytes(Charset.forName("UTF-8")));
		
		res = this.resourceService.save(res);
		
		/* finally add to ACL */
		
		List<VACLEntry> entries = new ArrayList<>();
		
		addResourceACLEntries(user, chart.id(), null, null, Boolean.TRUE, entries);
		preloadedUserConfig.getDefaultResourceAdmin().forEach((key,value) 
				-> addResourceACLEntries(user, chart.id(), key, value, Boolean.FALSE, entries));
		
		VACL acl = new VACL(new ChartOID(chart.id()), 
				entries, 
				false);
		
		acl = this.aclService.save(acl);
		
		return new ResponseEntity<>(Collections.singletonMap("id", chart.id()), 
									HttpStatus.OK);
	}


	private void addResourceACLEntries(final User currentUser, int chartId, 
			String preloadedUserName, 
			String preloadedUserContext, Boolean isCreator, List<VACLEntry> entries) {
		
		User sUser = null;
		
		if(isCreator) {
			sUser = currentUser;
		} else if(!currentUser.getUsername().equalsIgnoreCase(preloadedUserName)) {
			sUser = userStorage.find(preloadedUserName, preloadedUserContext);
		}
		if(sUser != null)
		{
			final VSid sSid = new VSid(sUser);
			Stream.of(VPermission.values())
				.filter(perm -> perm.applicableTo(new ChartOID(chartId)))
				.map(perm -> new VACLEntry(sSid, perm))
				.forEach(entries :: add);
		}
	}
	
	@RequestMapping(value = "/projects/{project-id}/charts/search",
					produces = "application/json",
					method = RequestMethod.GET)
	@ApiOperation(value = "search for a chart",
					notes = "Search for any charts within a specified project "
							+ "which contain a description/name "
							+ "containing a specified query term.  If no query "
							+ "is provided, this operation may be used to list "
							+ "the charts within a project.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "search completed successfully",
					response = SearchResults.class),
		@ApiResponse(code = 404, message = "specified project was not found/not accessible")
	})
	public ResponseEntity<?>
	search(@AuthenticationPrincipal User user,
			@RequestParam(value = "with-user-acl", required = false, defaultValue = "true")
			@ApiParam(value = "whether the results object should also contain "
								+ "ACLs reflecting the current user's permissions "
								+ "for each found chart", 
						defaultValue = "true")
				boolean withUserACL, 
			@PathVariable("project-id")
			@Min(0)
			@ApiParam(value = "ID of the project within which to search")
				int projectID,
			@RequestParam(value = "query", required = false) 
			@Size(min = 0)
			@ApiParam(value = "search term, omitted to list all charts within project")
				String searchTerm,
			@RequestParam(value = "limit", required = false, 
							defaultValue = "10")
			@Min(1) @Max(1000)
			@ApiParam(value = "maximum number of results to return",
						defaultValue = "10")
				int limit,
			@RequestParam(value = "offset", required = false,
							defaultValue = "0")
			@Min(0)
			@ApiParam(value = "offset from which to begin returning results; "
								+ "may be used to support paging",
						defaultValue = "0")
				int offset)
	{
		final String q = 
				(searchTerm == null || searchTerm.trim().isEmpty()) 
					? null 
					: searchTerm;
		
		final Project project = this.projectService.load(projectID);
		
		if (project == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		List<Integer> ids = this.chartStorage.search(user, q, project, limit, offset);
		
		Map<Integer, Chart> loaded = this.chartStorage.load(ids);
		
		List<Chart> charts = 
					ids.stream()
					.map(loaded :: get)
					.collect(Collectors.toList());
		
		final int count = this.chartStorage.count(user, q, project);
		final Map<Integer, VACLDTO> acls = withUserACL ? new HashMap<>() : null;
		
		if (withUserACL)
		{
			/* TODO - batch load! */
			charts.stream()
				.forEach(chart -> 
					acls.put(chart.id(), 
							VACLDTO.create(this.aclService.load(new ChartOID(chart.id()), user), f -> user)));
		}
		
		return new ResponseEntity<>(new SearchResults<>(charts, count, acls), 
										HttpStatus.OK);
	}
	

	/* partially secured by project service call; principal must be 
	 * able to access the project.  Chart access is unbounded.
	 */
	@RequestMapping(value = "/projects/{project-id}/charts/by-name/{name}",
					produces = "application/json",
					method = RequestMethod.GET)
	@ApiOperation(value = "get a chart with a given name within a project",
					notes = "Get a chart with a given name if one exists within "
							+ "the specified project")
	@ApiResponses({
		@ApiResponse(code = 200, 
						message = "chart retrieved successfully", 
						response = Chart.class),
		@ApiResponse(code = 404, 
					message = "specified chart or project not found/not accessible")
	})
	public ResponseEntity<?>
	getChart(@PathVariable("project-id") 
				@Min(0)
				@ApiParam("ID of the project from which to get chart with given name")
				int projectID, 
				@PathVariable("name") 
				@Size(min = 1)
				@ApiParam("name of chart to get")
				String chartName)
	{
		final Project project = this.projectService.load(projectID);
		
		if (project == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		Integer chartID = this.chartStorage.findID(chartName, project);
		
		if (chartID == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return this.getChart(chartID);
	}

	/* Not secured since we allow explicit requests for charts independent
	 * of security.  This allows chart->dashboard composition.
	 */
	@RequestMapping(value = "/charts/{id}",
					produces = "application/json",
					method = RequestMethod.GET)
	@ApiOperation(value = "retrieve a chart by ID",
					notes = "Retrieve the chart with the specified ID.  The "
							+ "chart will be validated to ensure that its main "
							+ "resource exists, however no other validation "
							+ "will be performed.")
	@ApiResponses({
		@ApiResponse(code = 200, 
						message = "chart retrieved successfully",
						response = Chart.class),
		@ApiResponse(code = 404, message = "chart not found"),
		@ApiResponse(code = 500, message = "chart main resource not found")
	})
	public ResponseEntity<?>
	getChart(@PathVariable("id") 
				@Min(0)
				@ApiParam(value = "ID of the chart to retrieve")
				int chartID)
	{
		Chart chart = this.chartStorage.load(chartID);
		
		if (chart == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		ChartResource mainResource = 
				this.resourceService.loadShallow(chart.mainResourcePath(), chart.id());
		
		
		if (mainResource == null)
		{
			return new ResponseEntity<>(Collections.singletonMap("error", 
											"main-resource-doesnt-exist"), 
									HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
		return new ResponseEntity<>(chart, HttpStatus.OK);
	}
	
	
	
	/* additional security done programmatically since it depends on project,
	 * which isn't visible on call
	 */
	@RequestMapping(method = RequestMethod.GET,
					value = "/charts/{id}/acl")
	@ApiOperation(value = "retrieve the ACL of a specified chart",
					notes = "Retrieve the ACL for a specified chart.  The resulting "
							+ "ACL will contain only those entries that pertain "
							+ "to the requesting user unless the requesting "
							+ "user has the 'manage ACLs' permission; in this case "
							+ "the resulting ACL will contain either entries "
							+ "pertaining to the specified user (if one is "
							+ "explicitly specified) or the requesting user "
							+ "(if one is not.)  In order to succeed, this "
							+ "operatation requires that "
							+ "the requesting user 1) be able read the chart "
							+ "for which the request is being made 2) have "
							+ "the ability to manage chart ACLs for the enclosing "
							+ "project (if another user's ACL is requested) or "
							+ "3) request only their own ACL entries.")
	@ApiResponses({
		@ApiResponse(code = 200, 
						message = "ACL retrieved successfully",
						response = VACLDTO.class),
		@ApiResponse(code = 404, message = "chart not found")
	})
	public ResponseEntity<?>
	getACL(@PathVariable("id") 
			@Min(0)
			@ApiParam("ID of the chart for which to retrieve the ACL") 
				int id,
			@RequestParam(value = "user-id", required = false) 
			@Min(0)
			@ApiParam("ID of user for which to get ACL entries; omit to get "
					+ "retrieve all accessible ACL entries")
				Integer userID,
			@AuthenticationPrincipal User principal)
	{
		logger.debug("Getting ACL for chart_id:" + id + "\tuser-id:" + userID
				+ "\tprincipal username:" + principal.getUsername()
				+ "\tprincipal id:" + principal.id());
		
		Chart chart = this.chartStorage.load(id);
		
		if (chart == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		if (! this.sec.projectAllowed(chart.project().id(), 
										VPermission.MANAGE_CHART_ACLS))
		{
			if (! principal.id().equals(userID) && userID != null)
			{
				throw new AccessDeniedException(
						"Unable to retrieve ACL for user which is not "
						+ "principal without " 
						+ VPermission.MANAGE_CHART_ACLS.identifier() 
						+ " permission");
			}
		}
		
		
		VACL acl;
		
		if (userID != null)
		{
			User user = this.userStorage.load(userID);
			
			if (user == null)
			{
				acl = new VACL(new ChartOID(id), Collections.emptySet(), true);
			}
			else
			{
				acl = this.aclService.load(new ChartOID(id), user);
			}
		}
		else
		{
			 if (this.sec.projectAllowed(chart.project().id(), VPermission.MANAGE_CHART_ACLS))
			 {
				 acl = this.aclService.load(new ChartOID(id));
			 }
			 else
			 {
				acl = this.aclService.load(new ChartOID(id), principal); 
			 }
		}
		
		Assert.notNull(acl,  "saved ACL was null");
		
		Map<Integer, User> users = 
				this.userStorage
					.load(acl.represents(VSid.class)
							.stream()
							.map(s -> s.userID())
							.collect(Collectors.toSet()));
		
		VACLDTO dto = VACLDTO.create(acl, users :: get);
		
		dto.toACLEntries().forEach(e -> e.permission().toString());
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	@PreAuthorize("@vsec.chartAllowed(#id, 'write')")
	@RequestMapping(method = RequestMethod.PUT,
					value = "/charts/{id}/acl")
	@ApiOperation(value = "update the ACL for a specific chart",
					notes = "Save a given ACL for the specified chart.  If the "
							+ "given ACL is 'partial', then only identities "
							+ "(users or user categories) represented by the "
							+ "ACL will have their permissions replaced by "
							+ "those in the ACL; if the ACL is not 'partial' "
							+ "its contents will unconditionally replace any "
							+ "ACL entries for the specified chart. The "
							+ "requesting user must be able to modify both the "
							+ "chart AND be able to manage chart ACLs for the "
							+ "project to which the chart belongs for this "
							+ "operation to succeed.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "ACL updated successfully"),
		@ApiResponse(code = 403, 
						message = "requesting user is not authorized to manage "
								+ "chart ACLs for the project to which the chart "
								+ "belongs OR requesting user is not authorized "
								+ "to modify the chart",
						response = NamedError.class),
		@ApiResponse(code = 404, message = "chart not found")
	})
	public ResponseEntity<?>
	saveACL(@PathVariable("id") 
			@Min(0)
			@ApiParam("ID of the chart for which to save/update ACL")
				int id, 
			@RequestBody VACLDTO dto)
	{
		Chart chart = this.chartStorage.load(id);
		
		if (chart == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		
		if (! this.sec.projectAllowed(chart.project().id(), 
										VPermission.MANAGE_CHART_ACLS))
		{
			return new ResponseEntity<>(new NamedError("cannot-manage-chart-acls"),
										HttpStatus.FORBIDDEN);
		}
		
		
		VACL acl = new VACL(new ChartOID(id), dto.toACLEntries(), dto.partial());
		
		acl = this.aclService.save(acl);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PreAuthorize("@vsec.chartAllowed(#id, @vsec.PERM_WRITE)")
	@RequestMapping(value = "/charts/{id}",
					method = RequestMethod.DELETE)
	@Transactional
	@ApiOperation(value = "delete a chart",
					notes = "Delete the chart with the given ID (if it exists.)",
					code = 204)
	@ApiResponses({
		@ApiResponse(code = 404, message = "No chart was found with the given ID"),
		@ApiResponse(code = 403, 
			message = "The user lacked permission to modify the specified chart"),
		@ApiResponse(code = 204, message = "The chart was deleted successfully.")
	})
	public ResponseEntity<?>
	delete(@PathVariable("id")
			@Min(0)
			@ApiParam("ID of chart to delete")
				int id)
	{
		Chart chart = this.chartStorage.load(id);
		
		if (chart == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		Set<Integer> resIDs = new HashSet<>(this.resourceService.find(chart));
		
		resIDs.forEach(this.resourceService :: delete);
		
		this.chartStorage.delete(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@ApiModel(value = "chart-dto",
				description = "DTO for chart manipulation")
	public static final class ChartDTO
	{
		@JsonProperty(value = "name", required = true)
		@NotNull
		@Size(min = 1)
		@ApiModelProperty(value = "chart name (unique within a project)",
							example = "unique-chart-name",
							required = true, allowEmptyValue = false)
		String name;
		
		@JsonProperty("description")
		@ApiModelProperty(value = "human-readable chart description",
							example = "My fancy chart",
							required = true, allowEmptyValue = true)
		String description;
		
		@JsonProperty(value = "main-resource", required = false)
		@ApiModelProperty(value = "chart main resource (i.e. the chart's source code)",
							required = true, allowEmptyValue = false)
		String mainResource;
		
		@JsonProperty(value = "required-datasets", required = false)
		@ApiModelProperty(value = "ID(s) of the dataset(s) that the chart depends on")
		List<Integer> requiredDatasets = new ArrayList<>();
	}

}
