package edu.unc.cscc.views.controller;

import java.io.IOException;
import java.time.Instant;
import java.util.Collection;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.exception.Auth0Exception;

import edu.unc.cscc.views.model.security.User;
import edu.unc.cscc.views.model.security.UsernamePasswordCredentialsSource;
import edu.unc.cscc.views.service.Auth0CredentialsSource;
import edu.unc.cscc.views.service.Auth0Service;
import edu.unc.cscc.views.service.LocalUsernamePasswordCredentialsSource;
import edu.unc.cscc.views.service.TokenAuthenticationService;
import edu.unc.cscc.views.service.UsernamePasswordCredentialsSourceManager;
import edu.unc.cscc.views.service.storage.UserStorage;
import io.jsonwebtoken.Jwt;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(tags = "Authentication")
public class AuthenticationController
{
	
	@Autowired
	private UsernamePasswordCredentialsSourceManager	credManager;
	
	@Autowired
	private UserStorage									userService;
	
	@Autowired
	private TokenAuthenticationService 					tokinService;

	@Autowired
	private Auth0Service								auth0Service;

	@Value("${auth.frontendUri}")
	private String										frontendUri;

	@Value("${auth.loginRedirect}")
	private String 										loginRedirectPath;
	
	/**
	 * Attempt to authenticate a given user/password against a specified 
	 * credential source, receiving a token which may be used for further API
	 * interaction (if successful).
	 * 
	 * @param username username of the user to authenticate
	 * @param password password of the user to authenticate
	 * @param credentialSourceID ID of the credential source against which the
	 * 	specified user/password information will be authenticated
	 * @return token (if authentication successful)
	 * @throws IOException
	 */
	@RequestMapping(value = "/auth",
					method = RequestMethod.GET,
					produces = "application/jwt")
	@ApiOperation(value = "authenticate a user using a given set of "
							+ "credentials",
					notes = "Attempt to authenticate a user using a given set of "
							+ "credentials (against a specified credential source) "
							+ "producing a JWT token that may be "
							+ "used for future API operations if successful.",
					response = Jwt.class)
	@ApiResponses({
		@ApiResponse(code = 401,
					message = "Authentication was attempted, but the attempt failed"),
		@ApiResponse(code = 400,
					message = "An invalid/nonexistant credential source was supplied")
	})
	@Valid
	public ResponseEntity<String>
	authenticate(@RequestParam("username") 
					@ApiParam("username of user to authenticate") 
					@NotNull
					@Size(min = 1)
						String username, 
					@RequestParam("password") 
					@ApiParam("plaintext password of user to authenticate") 
					@NotNull
						String password, 
					@RequestParam(required = false, value = "credential-source-id") 
					@ApiParam("ID of credential source against which the specified "
							+ "credentials will be authenticated")
					@Size(min = 2)
						String credentialSourceID)
	throws IOException
	{
		if (credentialSourceID == null)
		{
			credentialSourceID = LocalUsernamePasswordCredentialsSource.IDENTIFIER;
		}
		
		UsernamePasswordCredentialsSource src = 
				this.credManager.lookupSource(credentialSourceID);
		
		if (src == null)
		{
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		if (src.authenticate(username, password))
		{
			User user = this.userService.find(username, credentialSourceID);
			
			/* handle non-local users we're creating on demand */
			if (user == null)
			{
				if (credentialSourceID 
						== LocalUsernamePasswordCredentialsSource.IDENTIFIER)
				{
					return new ResponseEntity<>("local-user-doesnt-exist", 
							HttpStatus.INTERNAL_SERVER_ERROR);
				}
				
				
				user = new User(null, username, credentialSourceID, username, 
								Instant.now(), true, User.DEFAULT_ROLES);
				
				user = this.userService.createNewUser(user);
				
			}
			
			Assert.notNull(user, "saved user was null");
			
			if (user.active())
			{
				return new ResponseEntity<>(this.tokinService.createToken(user), 
						HttpStatus.OK);
			}
		}
		
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	}
	/**
	 * Attempt to authenticate a user using auth0
	 */
	@RequestMapping(value = "/auth/auth0-login",
					method = RequestMethod.GET,
					produces = "application/jwt")
	@ApiOperation(value = "authenticate a user using an auth0 access token",
					notes = "Attempt to authenticate a user using an Auth0 acces token "
							+ "producing a JWT token that may be "
							+ "used for future API operations if successful.",
					response = Jwt.class)
	@ApiResponses({
		@ApiResponse(code = 401,
					message = "Authentication was attempted, but the attempt failed"),
	})
	@Valid
	public ResponseEntity<String>
	auth0Authenticate(@RequestParam("accessToken") 
					@ApiParam("The idtoken of the user to authenticate") 
					@NotNull
					@Size(min = 1)
						String token)
	{
		String email;
		try {
			email = auth0Service.getUserEmail(token);
		} catch (Auth0Exception e) {
			return new ResponseEntity<>("Error communicating with Auth0", HttpStatus.UNAUTHORIZED);
		}
		User user = this.userService.find(email, Auth0CredentialsSource.IDENTIFIER);
		if (user == null) {
			return new ResponseEntity<>("User doesn't exist", 
							HttpStatus.UNAUTHORIZED);
		}
		return new ResponseEntity<String>(this.tokinService.createToken(user), HttpStatus.OK);
	}


	/**
	 * Enumerate the credential sources against which users may be 
	 * authenticated.
	 * 
	 * @return credential sources
	 */
	@RequestMapping(value = "/auth/sources",
					method = RequestMethod.GET,
					produces = "application/json")
	@ApiOperation(value = 
					"get available credential sources",
					notes = "Enumerate the credential sources against which "
							+ "users may be authenticated.",
					response = UsernamePasswordCredentialsSource.class,
					responseContainer = "list",
					responseReference = "views-credential-source")
	public Collection<UsernamePasswordCredentialsSource>
	credentialSources()
	{
		return this.credManager.sources();
	}
}
