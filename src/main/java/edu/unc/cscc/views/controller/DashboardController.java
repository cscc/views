package edu.unc.cscc.views.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import edu.unc.cscc.views.model.Chart;
import edu.unc.cscc.views.model.Dashboard;
import edu.unc.cscc.views.model.Project;
import edu.unc.cscc.views.model.security.ChartOID;
import edu.unc.cscc.views.model.security.DashboardOID;
import edu.unc.cscc.views.model.security.User;
import edu.unc.cscc.views.model.security.VACL;
import edu.unc.cscc.views.model.security.VACLDTO;
import edu.unc.cscc.views.model.security.VACLEntry;
import edu.unc.cscc.views.model.security.VPermission;
import edu.unc.cscc.views.model.security.VSid;
import edu.unc.cscc.views.service.VSec;
import edu.unc.cscc.views.service.storage.ChartStorageService;
import edu.unc.cscc.views.service.storage.DashboardStorageService;
import edu.unc.cscc.views.service.storage.ProjectStorageService;
import edu.unc.cscc.views.service.storage.VACLService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = {"Dashboards"})
@RestController
@Validated
public class DashboardController 
{
	
	@Autowired
	private ChartStorageService			chartService;
	@Autowired
	private DashboardStorageService		service;
	@Autowired
	private ProjectStorageService		projectService;
	
	@Autowired
	private VACLService					aclService;
	
	@Autowired
	private VSec						sec;
	
	

	@RequestMapping(value = {"/dashboards/{id}",
							"/projects/{projectID}/dashboards/{id}"},
					method = RequestMethod.GET,
					produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "retrieve a dashboard by ID",
					notes = "Retrieve the dashboard with the specified ID")
	@ApiResponses({
		@ApiResponse(code = 404, message = "no dashboard found with given ID"),
		@ApiResponse(code = 200, message = "dashboard retrieved successfully",
					response = Dashboard.class)
	})
	public ResponseEntity<?>
	get(@PathVariable("id") 
		@Min(0)
		@ApiParam("ID of the dashboard to retrieve")
		int id)
	{
		Dashboard db = this.service.load(id);
		
		if (db == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(db, HttpStatus.OK);
	}
	
	@PreAuthorize("@vsec.dashboardAllowed(#id, 'delete')")
	@RequestMapping(value = {"/dashboards/{id}",
								"/projects/{projectID}/dashboards/{id}"},
					method = RequestMethod.DELETE)
	@ApiOperation(value = "delete a dashboard",
					notes = "Delete the dashboard with the given ID.  In order "
							+ "for this operation to succeed the requesting user "
							+ "must hold the 'delete' permission on the specified "
							+ "dashboard.",
					code = 204)
	@ApiResponses({
		@ApiResponse(code = 204, message = "deletion succeeeded", response = Void.class),
		@ApiResponse(code = 404, message = "dashboard not found")
	})
	public ResponseEntity<?>
	delete(@PathVariable("id") 
			@Min(0)
			@ApiParam("ID of dashboard to delete")
			int id)
	{
		if (! this.service.exists(id))
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		this.service.delete(id);
		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@PreAuthorize("@vsec.dashboardAllowed(#id, 'write')")
	@RequestMapping(value = {"/dashboards/{id}", 
							"/projects/{projectID}/dashboards/{id}"},
			method = RequestMethod.PUT,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@Transactional
	@ApiOperation(value = "update a specified dashboard",
					notes = "Update the specified dashboard.  In order "
							+ "for this operation to succeed, the requesting "
							+ "user must hold the 'write' permission on the "
							+ "dashboard to update.")
	@ApiResponses({
		@ApiResponse(code = 200, message = "dashboard updated successfully", 
						response = Dashboard.class),
		@ApiResponse(code = 400, message = "dashboard contains non-existent chart",
						response = NamedError.class),
		@ApiResponse(code = 404, message = "dashboard not found"),
		@ApiResponse(code = 409, message = "name conflicts with another dashboard",
						response = NamedError.class)
	})
	public ResponseEntity<?>
	update(@PathVariable("id") 
			@Min(0)
			@ApiParam("ID of dashboard to update")
			int id, 
			@Validated @RequestBody DashboardDTO dto)
	{
		
		Dashboard db = this.service.load(id);
		
		if (db == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		Dashboard existing = this.service.loadByName(dto.name, db.project());
		
		if (existing != null 
			&& ! existing.id().equals(db.id()))
		{
			return new ResponseEntity<>(
						new NamedError("name-exists", 
										"name conflicts with dashboard with "
										+ "ID " + existing.id()),
						HttpStatus.CONFLICT);
		}
		
		Map<Integer, Chart> charts = this.chartService.load(dto.chartIDs);
		
		/* check that the charts actually exist */
		/* note that this is potentially vulnerable to someone creating a 
		 * dashboard out of charts that they can't see.  But... that still
		 * doesn't gain them anything, so I'm not sure this is a problem.
		 */
		if (charts.size() != dto.chartIDs.size())
		{
			return new ResponseEntity<>(
					new NamedError("chart-does-not-exist"),
					HttpStatus.BAD_REQUEST);
		}
		
		List<Chart> chartList = 
				dto.chartIDs.stream()
							.map(charts :: get)
							.collect(Collectors.toList());
		
		db = db.name(dto.name)
				.description(dto.description)
				.charts(chartList);
		
		db = this.service.save(db);
		
		return new ResponseEntity<>(db, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/projects/{projectID}/dashboards/",
					method = RequestMethod.POST,
					consumes = MediaType.APPLICATION_JSON_VALUE,
					produces = MediaType.APPLICATION_JSON_VALUE)
	@Transactional
	@ApiOperation(value = "create a dashboard",
					notes = "Create a dashboard within the specified project.")
	@ApiResponses({
		@ApiResponse(code = 200, 
						message = "dashboard created; response reflects "
								+ "created dashboard",
						response = Dashboard.class),
		@ApiResponse(code = 400, message = "dashboard contains non-existent chart",
						response = NamedError.class),
		@ApiResponse(code = 404, message = "dashboard not found"),
		@ApiResponse(code = 409, message = "name conflicts with another dashboard",
						response = NamedError.class)
	})
	public ResponseEntity<?>
	create(@AuthenticationPrincipal User user,
			@PathVariable("projectID") 
			@Min(0)
			@ApiParam("ID of project in which to create dashboard")
			final int projectID,
			@Valid @RequestBody DashboardDTO dto)
	{
		final Project project = this.projectService.load(projectID);
		
		if (project == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		Dashboard existing = this.service.loadByName(dto.name, project);
		
		if (existing != null)
		{
			return new ResponseEntity<>(
						new NamedError("name-exists"),
						HttpStatus.CONFLICT);
		}
		
		
		Map<Integer, Chart> charts = this.chartService.load(dto.chartIDs);
		
		if (charts.size() != dto.chartIDs.size())
		{
			return new ResponseEntity<>(
					new NamedError("chart-does-not-exist"),
					HttpStatus.BAD_REQUEST);
		}
		
		List<Chart> chartList = 
				dto.chartIDs.stream()
							.map(charts :: get)
							.collect(Collectors.toList());
		
		Dashboard db = 
				new Dashboard(null, dto.name, dto.description, project, chartList);
		
		db = this.service.save(db);
		
		List<VACLEntry> entries = new ArrayList<>();
		
		entries.add(new VACLEntry(new VSid(user), VPermission.WRITE));
		
		VACL acl = new VACL(new DashboardOID(db.id()), entries, false);
		
		acl = this.aclService.save(acl);
				
		return new ResponseEntity<>(db, HttpStatus.OK);
		
	}
	
	/* secured programmatically */
	@RequestMapping(value = "/projects/{projectID}/dashboards/search",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "search for a dashboard",
					notes = "Search for dashboards matching a given query.  If "
							+ "no query is provided, enumerate all dashboards "
							+ "within the project.  Only dashboards containing "
							+ "one or more charts readable by the requesting user "
							+ "will be produced.",
					code = 200)
	@ApiResponses({
		@ApiResponse(code = 200, message = "search completed successfully",
						response = DashboardSearchResults.class),
		@ApiResponse(code = 404, message = "project not found")
	})
	public ResponseEntity<?>
	search(@AuthenticationPrincipal User user,
			@PathVariable("projectID")
			@Min(0)
			@ApiParam("ID of project in which to search")
				int projectID,
			@RequestParam(value = "with-user-acl", required = false, 
							defaultValue = "true")
			@ApiParam(value = "whether to include ACL(s) reflecting the requesting "
								+ "user's permissions for the resulting charts and "
								+ "dashboards",
						required = false, defaultValue = "true")
				boolean withUserACL, 
			@RequestParam(value = "query", 
						defaultValue = "", 
						required = false)
			@ApiParam(value = "term for which to search, omitted to enumerate "
							+ "all dashboards",
						required = false)
				final String query,
			@RequestParam(value = "limit", 
							defaultValue = "100",
							required = false)
			@Min(1) @Max(100)
			@ApiParam(value = "maximum number of results", required = false,
						defaultValue = "100")
				final int limit,
			@RequestParam(value = "offset", 
							defaultValue = "0",
							required = false)
			@Min(0)
			@ApiParam(value = "offset from which to start returning results", 
						required = false, defaultValue = "0")
				final int offset)
	{
		
		Project project = this.projectService.load(projectID);
		
		if (project == null)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		List<Integer> ids = 
				this.service.search(project, query, user, limit, offset);
		
		Map<Integer, Dashboard> dbs = this.service.load(ids);
		
		List<Dashboard> dashboards = 
							ids.stream()
								.map(dbs :: get)
								.filter(d -> d != null)
								/* filter out the dashboards that the user can't read any
								 * charts for and can't modify
								 */
								.filter(d -> {
									return this.sec.dashboardAllowed(d.id(), 
															VPermission.WRITE)
										|| d.charts()
												.stream()
												.anyMatch(this.sec :: canReadChart);
								})
								.collect(Collectors.toList());
		
		final int count = this.service.count(project, query);
		
		Map<Integer, VACLDTO> chartACLs = withUserACL ? new HashMap<>() : null;
		Map<Integer, VACLDTO> acls = withUserACL ? new HashMap<>() : null;

				
		if (withUserACL)
		{
			/* TODO - batch load! */
			dashboards.stream()
				.forEach(d -> 
					acls.put(d.id(), 
							VACLDTO.create(this.aclService.load(new DashboardOID(d.id()), user), f -> user)));
			
			
			dashboards.stream()
						.flatMap(d -> d.charts().stream())
						.map(c -> c.id())
						.unordered()
						.distinct()
						.forEach(cid -> 
									chartACLs.put(cid,
											VACLDTO.create(this.aclService.load(new ChartOID(cid), user), f -> user)));
						
		}
		
		final DashboardSearchResults results = 
				new DashboardSearchResults(dashboards, count, acls, chartACLs);
		
		return new ResponseEntity<>(results, HttpStatus.OK);
	}
	
	@ApiModel("dashboard-search-results")
	public static final class DashboardSearchResults
	extends SearchResults<Dashboard>
	{
		
		private final Map<Integer, VACLDTO>		chartACLs;
		
		public DashboardSearchResults(List<Dashboard> results, int totalCount,
				Map<Integer, VACLDTO> acls, Map<Integer, VACLDTO> chartACLs)
		{
			super(results, totalCount, acls);
			this.chartACLs = chartACLs;
		}
		
		@JsonGetter("chart-acls")
		@ApiModelProperty("ACLs for the charts referenced by the result dashboards,"
							+ "indexed by chart ID")
		public Map<Integer, VACLDTO>
		chartACLs()
		{
			return this.chartACLs;
		}
		
	}
	
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	@ApiModel("dashboard-dto")
	public static final class DashboardDTO
	{
		@JsonProperty("name")
		@NotNull
		@Size(min = 1)
		@ApiModelProperty(value = "dashboard name", required = true,
							allowEmptyValue = false)
		public String			name;
		
		@JsonProperty(value = "description", required = false)
		@ApiModelProperty(value = "description of dashboard", required = false,
							allowEmptyValue = true)
		public String			description = null;
	
		@JsonProperty("chart-ids")
		@NotNull
		@ApiModelProperty(value = "IDs of charts which make up the dashboard, "
									+ "in order of suggested appearance",
							required = true)
		public List<Integer>	chartIDs;
	}
	
	
}
