package edu.unc.cscc.views.model;

import org.springframework.util.Assert;

public class ChartResource
implements VEntity
{

	public static final int MAX_SIZE = 2 * 1024 * 1024;
	
	private final Integer id;
	
	private final String path;
	
	private final Chart chart;
	
	private final byte[] data;
	
	public ChartResource(Integer id, String path, Chart chart, byte[] data)
	{ 
		this.id = id;
		this.path = path;
		this.chart = chart;
		
		if (data != null)
		{
			Assert.isTrue(data.length < MAX_SIZE, "Resource cannot exceed " 
					+ MAX_SIZE + " bytes");
			
		}
		this.data = data;
	}

	@Override
	public Integer 
	id()
	{
		return id;
	}
	
	public ChartResource 
	id(Integer id)
	{
		return new ChartResource(id, this.path(), this.chart(), this.data());
	}
	
	/* TODO - add real content type */
	public String
	contentType()
	{
		return "application/octet-stream";
	}

	public String 
	path()
	{
		return path;
	}
	
	public final Chart
	chart()
	{
		return this.chart;
	}

	public byte[] 
	data()
	{
		return data;
	}
	
	public ChartResource
	data(byte[] data)
	{
		return new ChartResource(id, this.path(), this.chart(), data); 
	}
	
	public boolean
	isShallow()
	{
		return this.data == null;
	}

	
	
}
