package edu.unc.cscc.views.model.security;

import java.util.Arrays;

import org.springframework.security.acls.model.ObjectIdentity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonCreator.Mode;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModel;

/**
 * A permission within views.  Note that we abuse the "mask" concept from
 * Spring Sec.  We simply use the "mask" as a permission identifier, not a 
 * bitmask.  This is fine, since we don't use their ACL logic either.
 * 
 * @author Robert Tomsick (rtomsick@unc.edu)
 *
 */
@ApiModel("permission")
public enum VPermission 
{
	
	READ {
		@Override
		public boolean applicableTo(ObjectIdentity object)
		{
			return (object instanceof DataSetOID)
					|| (object instanceof ProjectOID);
		}

		@Override
		public String identifier()
		{
			return "read";
		}
	},
	WRITE {
		@Override
		public boolean applicableTo(ObjectIdentity object)
		{
			return (object instanceof ChartOID)
					|| (object instanceof DashboardOID)
					|| (object instanceof DataSetOID)
					|| (object instanceof ProjectOID);
		}

		@Override
		public String identifier()
		{
			return "write";
		}
	},
	
	CREATE_CHARTS {
		@Override
		public boolean applicableTo(ObjectIdentity object)
		{
			return object instanceof ProjectOID;
		}

		@Override
		public String identifier()
		{
			return "create-charts";
		}
	},
	CREATE_DASHBOARDS {
		@Override
		public boolean applicableTo(ObjectIdentity object)
		{
			return object instanceof ProjectOID;
		}

		@Override
		public String identifier()
		{
			return "create-dashboards";
		}
	},
	CREATE_DATASETS {
		@Override
		public boolean applicableTo(ObjectIdentity object)
		{
			return object instanceof ProjectOID;
		}

		@Override
		public String identifier()
		{
			return "create-datasets";
		}
	},
	MANAGE_CHART_ACLS {
		@Override
		public boolean applicableTo(ObjectIdentity object)
		{
			return object instanceof ProjectOID;
		}

		@Override
		public String identifier()
		{
			return "manage-chart-access-control";
		}
	},
	MANAGE_DATASET_ACLS {
		@Override
		public boolean applicableTo(ObjectIdentity object)
		{
			return object instanceof ProjectOID;
		}

		@Override
		public String identifier()
		{
			return "manage-dataset-access-control";
		}
	};
	
	public abstract boolean	
	applicableTo(ObjectIdentity object);
	
	@JsonValue
	public abstract String
	identifier();
	
	@JsonCreator(mode = Mode.DELEGATING)
	public static final VPermission
	parse(String identifier)
	{
		return Arrays.stream(VPermission.values())
						.filter(v -> v.identifier().equals(identifier))
						.findFirst().orElse(null);
	}
	
}
