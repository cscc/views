package edu.unc.cscc.views.model.security;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.acls.model.Acl;
import org.springframework.security.acls.model.ObjectIdentity;
import org.springframework.security.acls.model.Sid;

/**
 * Minimalist ACL implementation for views. Simpler alternative to {@link Acl}.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public final class VACL 
{
	private final ObjectIdentity					oid;
	private final Map<Sid, Set<VACLEntry>>			aces;
	
	private final boolean							partial;
	
	
	public VACL(ChartOID oid, Collection<VACLEntry> entries, boolean partial)
	{
		this(oid, toACLMap(entries), partial);
	}
	
	public VACL(DashboardOID oid, Collection<VACLEntry> entries, boolean partial)
	{
		this(oid, toACLMap(entries), partial);
	}

	public VACL(DataSetOID oid, Collection<VACLEntry> entries, boolean partial)
	{
		this(oid, toACLMap(entries), partial);
	}
	
	public VACL(ProjectOID oid, Collection<VACLEntry> entries, boolean partial)
	{
		this(oid, toACLMap(entries), partial);
	}
	
	private VACL(ObjectIdentity oid, Map<Sid, Set<VACLEntry>> aces, boolean partial)
	{		
		this.oid = oid;
		this.aces = Collections.unmodifiableMap(aces);
		this.partial = partial;
	}
	
	private static final Map<Sid, Set<VACLEntry>>
	toACLMap(Collection<VACLEntry> entries)
	{
		Map<Sid, Set<VACLEntry>> m = new HashMap<>();
		
		for (final VACLEntry e : entries)
		{
			Set<VACLEntry> s = m.get(e.sid());
			
			if (s == null)
			{
				s = new HashSet<>();
				m.put(e.sid(), s);
			}
			
			s.add(e);
		}
		
		m.replaceAll((k, v) -> Collections.unmodifiableSet(v));
		
		return m;
	}
	
	public VACL
	add(VACLEntry entry)
	{
		Map<Sid, Set<VACLEntry>> e = new HashMap<>(this.aces);
		
		
		Set<VACLEntry> entries = e.get(entry.sid());
		
		if (entries == null)
		{
			entries = new HashSet<>();
			e.put(entry.sid(), entries);
		}
		
		entries.add(entry);
		
		return new VACL(this.objectIdentity(), e, this.isPartial());
	}
	
	public VACL
	remove(VACLEntry entry)
	{
		Map<Sid, Set<VACLEntry>> e = new HashMap<>(this.aces);
		
		Set<VACLEntry> entries = e.get(entry.sid());
		
		if (entries != null)
		{
			entries.remove(entry);
		}
		
		return new VACL(this.objectIdentity(), e, this.isPartial());
	}
	
	public ObjectIdentity
	objectIdentity()
	{
		return this.oid;
	}
	
	public Set<VACLEntry>
	entries()
	{
		return this.aces.values()
						.stream()
						.flatMap(es -> es.stream())
						.collect(Collectors.toCollection(HashSet :: new));
	}
	
	public Set<Sid>
	represents()
	{
		return Collections.unmodifiableSet(this.aces.keySet());
	}
	
	/**
	 * Get the {@link Sid identities} of the given type represented by the 
	 * ACL.
	 * 
	 * @param cls type of identity to get
	 * @return all entities of the given type represented by the ACL
	 */
	public <T extends Sid> Set<T>
	represents(Class<T> cls)
	{
		return this.aces.keySet()
						.stream()
						.filter(s -> cls.isAssignableFrom(s.getClass()))
						.map(s -> cls.cast(s))
						.collect(Collectors.toSet());
	}
	
	/**
	 * Whether the ACL is a partial one for the associated object, i.e. whether
	 * it contains entries for all {@link Sid identities} which hold permissions
	 * on the object or just a subset.  If partial, it is assumed that all 
	 * {@link Sid identities} that <i>are</i> represented by the ACL are 
	 * represented in full, i.e. the ACL either contains all entries for a 
	 * given identity or none.
	 * 
	 * @return <code>true</code> if partial, <code>false</code> otherwise
	 */
	public boolean
	isPartial()
	{
		return this.partial;
	}
	
	/**
	 * Whether the ACL grants all of the given permissions to any of the given
	 * {@link Sid identities}.  A permission will be considered granted if 
	 * the ACL contains an entry for it for <em>any</em> of the given identities
	 * (i.e. the same Sid does not need to have entries for all of the 
	 * given permissions; if each permission is held by at least one Sid holds
	 * each permission this method will return <code>true</code>.)
	 * 
	 * @param permissions
	 * @param sids
	 * @return
	 */
	public boolean
	grants(Collection<VPermission> permissions, List<Sid> sids)
	{
		
//		for (Permission p : permissions)
//		{
//			boolean granted = false;
//			for (Sid s : sids)
//			{
//				if (granted || ! this.aces.containsKey(s))
//				{
//					continue;
//				}
//				
//				for (VACLEntry e : this.aces.get(s))
//				{
//					granted = granted || e.grants(p);
//				}
//			}
//			
//			if (! granted)
//			{
//				return false;
//			}
//		}
//		
//		return true;
//		
		return permissions
				.stream()
				.allMatch(p -> 
							sids.stream()
								.anyMatch(s -> 
											this.aces.containsKey(s)
											&& this.aces.get(s)
													.stream()
													.anyMatch(x -> x.grants(p))));
	}
	
	public String
	toString()
	{
		return this.entries()
					.stream()
					.map(e -> "entry: " + e.permission().name() + "->" + e.sid())
					.collect(Collectors.joining(","));
	}

}
