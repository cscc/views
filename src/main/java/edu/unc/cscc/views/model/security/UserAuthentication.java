package edu.unc.cscc.views.model.security;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

public final class UserAuthentication 
implements Authentication
{

	private static final long serialVersionUID = 1L;
	private final User user;
	private boolean authenticated = true;

	public UserAuthentication(User user) 
	{
		this.user = user;
	}

	@Override
	public String
	getName() 
	{
		return user.name();
	}

	@Override
	public Collection<? extends GrantedAuthority> 
	getAuthorities()
	{
		return user.getAuthorities();
	}

	@Override
	public Object 
	getCredentials() 
	{
		/* nothing, since this corresponds to an authenticated user and 
		 * authentications are (from Spring's perspective) per-request we have
		 * no reason to retain this
		 */
		return null;
	}

	@Override
	public User 
	getDetails() 
	{
		return null;
	}

	@Override
	public User 
	getPrincipal()
	{
		return this.user;
	}

	@Override
	public boolean 
	isAuthenticated() 
	{
		return authenticated;
	}

	@Override
	public void 
	setAuthenticated(boolean authenticated)
	{
		this.authenticated = authenticated;
	}

}
