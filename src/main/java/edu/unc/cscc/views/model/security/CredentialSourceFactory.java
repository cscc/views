package edu.unc.cscc.views.model.security;

import java.util.Collection;

public interface CredentialSourceFactory
{

	public Collection<UsernamePasswordCredentialsSource>
	createSources();
	
}
