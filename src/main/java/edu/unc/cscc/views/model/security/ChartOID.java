package edu.unc.cscc.views.model.security;

import org.springframework.security.acls.model.ObjectIdentity;

import com.fasterxml.jackson.annotation.JsonGetter;

public class ChartOID
implements ObjectIdentity
{
	public static final String	TYPE = "chart";
	private static final long	serialVersionUID = 1L;
	
	private final int			id;
	
	public ChartOID(int id)
	{
		this.id = id;
	}
	
	@Override
	@JsonGetter("id")
	public Integer 
	getIdentifier()
	{
		return this.id;
	}

	@Override
	@JsonGetter("type")
	public String 
	getType()
	{
		return TYPE;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChartOID other = (ChartOID) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	

}
