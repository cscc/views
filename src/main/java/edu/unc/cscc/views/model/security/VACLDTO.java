package edu.unc.cscc.views.model.security;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.security.acls.domain.GrantedAuthoritySid;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * DTO for client-side ACL handling.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@ApiModel(value = "acl", description = "DTO for access control lists")
public final class VACLDTO
{

	private final Map<Integer, VACLDTOUserEntry>			userEntries;
	private final Map<VPermission, Boolean>					anonymous;
	private final Map<VPermission, Boolean>					allUsers;
	private final boolean									partial;
	
	@JsonCreator
	public VACLDTO(@JsonProperty(value = "all-users", required = false, defaultValue = "{}") Map<VPermission, Boolean> allUsers, 
					@JsonProperty(value = "anonymous", required = false, defaultValue = "{}") Map<VPermission, Boolean> anonymous,
					@JsonProperty(value = "user-entries", required = false, defaultValue = "{}") Map<Integer, VACLDTOUserEntry> userEntries,
					@JsonProperty(value = "partial", required = false, defaultValue = "true") boolean partial)
	{
		this.allUsers = allUsers == null ? new HashMap<>() : allUsers;
		this.anonymous = anonymous == null ? new HashMap<>() : anonymous;
		this.userEntries = userEntries == null ? new HashMap<>() : new HashMap<>(userEntries);
		this.partial = partial;
	}
	
	@JsonGetter("user-entries")
	@ApiModelProperty(value = "user-specific ACL entries, mapped by user ID")
	public Map<Integer, VACLDTOUserEntry>
	userEntries()
	{
		return Collections.unmodifiableMap(this.userEntries);
	}
	
	@JsonGetter("anonymous")
	@ApiModelProperty(value = "permission grants applicable to anonymous users",
						dataType = "edu.unc.cscc.views.model.security.VACLDTO$GrantMap")
	public Map<VPermission, Boolean>	
	anoynmous()
	{
		return this.anonymous;
	}
	
	@JsonGetter("all-users")
	@ApiModelProperty(value = "permission grants applicable to all users",
						dataType = "edu.unc.cscc.views.model.security.VACLDTO$GrantMap")
	public Map<VPermission, Boolean>	
	allUsers()
	{
		return this.allUsers;
	}
	
	@JsonGetter("partial")
	@ApiModelProperty(value = "Whether the ACL is partial, i.e. whether it contains "
						+ "all known access control information for the entity "
						+ "to which it applies; defaults to true.  In the case of "
						+ "persistence, a partial ACL will replace the permissions "
						+ "of only those users/roles represented in the ACL.  "
						+ "Users/roles not represented in the partial ACL will "
						+ "not have their permissions modified.",
						required = false)
	public boolean
	partial()
	{
		return this.partial;
	}
	
	
	public static final VACLDTO
	create(VACL acl, Function<Integer, User> userFn)
	{
		Map<Integer, VACLDTOUserEntry> e = new HashMap<>();
		
		acl.represents()
			.stream()
			.filter(s -> (s instanceof VSid))
			.map(s -> ((VSid) s))
			.forEach(vsid -> {
				
				User user = userFn.apply(vsid.userID());
				
				final Map<VPermission, Boolean> entries = new HashMap<>();
				
				for (VPermission permission : VPermission.values())
				{
					if (acl.grants(Collections.singleton(permission), 
									Collections.singletonList(vsid)))
					{
						entries.put(permission, true);
					}
				}
				
				e.put(user.id(), new VACLDTOUserEntry(user, entries));
			});
		
		Map<VPermission, Boolean>	 all = new HashMap<>();
		Map<VPermission, Boolean>	 anonymous = new HashMap<>();

		
		Arrays.asList(VPermission.values())
				.stream()
				.forEach(permission -> {
					
					if (acl.grants(Collections.singleton(permission), 
									Collections.singletonList(new GrantedAuthoritySid(User.ViewsRole.ALL_USERS.authority()))))
					{
						all.put(permission, true);
					}
					
					if (acl.grants(Collections.singleton(permission), 
							Collections.singletonList(new GrantedAuthoritySid(User.ViewsRole.ANONYMOUS_USERS.authority()))))
					{
						anonymous.put(permission, true);
					}
				});
		
		
		return new VACLDTO(all, anonymous, e, acl.isPartial());
	}
	
	public Collection<VACLEntry>
	toACLEntries()
	{
		Collection<VACLEntry> entries = new ArrayList<>();

		
		this.allUsers.forEach((perm, granted) -> {
			entries.add(new VACLEntry(new GrantedAuthoritySid(User.ViewsRole.ALL_USERS.authority()), 
											perm));
		});
		
		this.anonymous.forEach((perm, granted) -> {
			entries.add(new VACLEntry(new GrantedAuthoritySid(User.ViewsRole.ANONYMOUS_USERS.authority()), 
											perm));
		});
		
		this.userEntries
			.values()
			.stream()
			.flatMap(e -> 
				e.entries().entrySet()
					.stream()
					.filter(x -> x.getValue())
					.map(x -> new VACLEntry(new VSid(e.user().id()), 
											x.getKey())))
			.forEach(entries :: add);
		
		return entries;
		
	}
	
	/* workaround to swagger-core bug #2379.  We only need this for 
	 * primitive properties, since for some reason object-typed props are 
	 * not automatically readOnly
	 */
	@JsonSetter("partial")
	private void
	partial_hack(boolean p)
	{
		throw new IllegalArgumentException("Workaround called");
	}
	
	@ApiModel(value = "acl-user-entry",
				description = "an entry in an ACL, specifying the permissions "
						+ "that a user is granted or denied")
	public static class VACLDTOUserEntry
	{
		private final User							user;
		private final Map<VPermission, Boolean>		entries;
		
		@JsonCreator
		public VACLDTOUserEntry(@JsonProperty("user") User user, 
								@JsonProperty("entries") Map<VPermission, Boolean> entries)
		{
			this.user = user;
			this.entries = entries;
		}
		
		@JsonGetter("user")
		@ApiModelProperty("the user to which the ACL entry applies")
		public User
		user()
		{
			return this.user;
		}
		
		@JsonGetter("entries")
		@ApiModelProperty(value = "a map of permission identifiers to whether they "
							+ "are granted/denied",
							dataType = "edu.unc.cscc.views.model.security.VACLDTO$GrantMap")
		public Map<VPermission, Boolean>
		entries()
		{
			return this.entries;
		}
	}
	
	/* this entire class is a hack around the clusterdump of stupidity that
	 * is Map support in swagger/JSON Schema.  See there's no actual way to 
	 * declare the type of a map key in the latter, and the former seem
	 * oddly averse to any sort of datatype support that's not provided by
	 * JSON Schema.  So all Maps turn into "object" entries, with 
	 * 'additionalProperties' specifying the type of the value... but since 
	 * they're objects, there is no way to denote the key type.  Which means
	 * your API spec will indicate something that may or may not correspond
	 * to reality.  
	 * 
	 * Not a big deal *except* for when you're dealing with the not-uncommon
	 * case of a Map with an enum as the key.  
	 * 
	 * In those cases, you do something like this, and completely defeat the
	 * brevity of Map<YourEnum, ?>... 
	 * 
	 * Sigh.
	 * 
	 * See discussion in swagger-core bug #2356
	 */
	@ApiModel("grant-map")
	private abstract class GrantMap
	{
		
		@JsonProperty("read")
		@ApiModelProperty("ability to read an object; "
							+ "applicable only to charts, datasets, and projects")
		public boolean read;
		
		@ApiModelProperty("ability to modify an object or its properties; "
							+ "applicable only to charts, datasets, dashboards, and projects")
		@JsonProperty("write")
		public boolean write;
		
		@ApiModelProperty("ability to create charts; applicable only to projects")
		@JsonProperty("create-charts")
		public boolean createCharts;
		
		@JsonProperty("create-datasets")
		@ApiModelProperty("ability to create datasets; applicable only to projects")
		public boolean createDatasets;
		
		@JsonProperty("create-dashboards")
		@ApiModelProperty("ability to create dashboards; applicable only to projects")
		public boolean createDashboards;
		
		@JsonProperty("manage-chart-access-control")
		@ApiModelProperty("ability to manage chart ACLs; applicable only to projects")
		public boolean manageChartAccessControl;
		
		@ApiModelProperty("ability to manage dataset ACLs; applicable only to projects")
		@JsonProperty("manage-dataset-access-control")
		public boolean manageDatasetAccessControl;
		
	}
	
}
