package edu.unc.cscc.views.model;

import com.fasterxml.jackson.annotation.JsonGetter;

//@JsonTypeInfo(use = Id.NAME, 
//				include = As.PROPERTY, 
//				property = "__VIEWS_TYPE__")
public interface VEntity
{	
	@JsonGetter("id")
	public Integer id();

}
