package edu.unc.cscc.views.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonTypeName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * A project is a top-level container for charts and datasets.  
 * Think "directory".
 *
 * @author Robert Tomsick (rtomsick@unc.edu)
 * 
 */
@JsonTypeName("project")
@ApiModel(value = "project", 
			description = "A project is a top-level container for charts and "
						+ "datasets.  Analagous to a directory in a filesystem.")
public class Project
implements VEntity
{
	private final Integer		id;
	private final String		name;
	private final String		description;
	
	@JsonCreator
	public Project(@JsonProperty(value = "id", required = false) Integer id, 
					@JsonProperty("name") String name, 
					@JsonProperty(value = "description", required = false) String description)
	{
		Assert.notNull(name, "name cannot be null");
		this.id = id;
		this.name = name;
		this.description = description;
	}
	
	/**
	 * Get the ID of the project (if it has been persisted).
	 * 
	 * @return project ID, may be <code>null</code>
	 */
	@JsonGetter("id")
	@ApiModelProperty(value = "ID of the project", readOnly = false)
	@Min(0)
	public Integer
	id()
	{
		return this.id;
	}
	
	/**
	 * Set the ID of the project.
	 * 
	 * @param id ID, may be <code>null</code>
	 * @return copy of the project with the given ID set
	 */
	public final Project
	id(Integer id)
	{
		return new Project(id, this.name(), this.description());
	}
	
	/**
	 * Get the name of the project.
	 * 
	 * @return project name, not <code>null</code>
	 */
	@JsonGetter("name")
	@ApiModelProperty(value = "name of the project; must be unique", 
						required = true,
						example = "My Project",
						readOnly = false)
	@Size(min = 1)
	@NotNull
	@Pattern(regexp = ".*\\w.*")
	public final String
	name()
	{
		return this.name;
	}
	
	/**
	 * Get an (optional) human-readable description of the project.
	 *  
	 * @return description, may be <code>null</code>
	 */
	@JsonGetter("description")
	@ApiModelProperty(value = "human-readable description of the project", 
						required = false,
						example = "This is a description of an example project.",
						readOnly = false)
	public final String
	description()
	{
		return this.description;
	}
	
	/* --- begin swagger-core workaround --- */
	
	@JsonSetter("id")
	private void
	idHack(int id)
	{
		throw new IllegalArgumentException("Workaround setter called");
	}
	
	@JsonSetter("name")
	private void
	nameHack(String str)
	{
		throw new IllegalArgumentException("Workaround setter called");
	}
	
	@JsonSetter("description")
	private void
	descriptionHack(String d)
	{
		throw new IllegalArgumentException("Workaround setter called");
	}
	
	
	/* --- end workaround --- */

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Project other = (Project) obj;
		if (description == null)
		{
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null)
		{
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null)
		{
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
	
}
