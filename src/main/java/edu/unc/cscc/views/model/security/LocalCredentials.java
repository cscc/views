package edu.unc.cscc.views.model.security;

import java.time.Instant;

import org.springframework.util.Assert;


public class LocalCredentials
{

	private final User user;
	private final String hash;
	private final Instant lastModified;
	
	public LocalCredentials(User user, String hash, Instant lastModified)
	{
		Assert.notNull(user, "user cannot be null");
		Assert.notNull(hash, "hash cannot be null");
		Assert.notNull(lastModified, "last modified date cannot be null");
		this.user = user;
		this.hash = hash;
		this.lastModified = lastModified;
	}

	public User 
	user()
	{
		return user;
	}

	public String 
	hash()
	{
		return hash;
	}

	public Instant 
	lastModified() 
	{
		return lastModified;
	}
	
	
	
}
