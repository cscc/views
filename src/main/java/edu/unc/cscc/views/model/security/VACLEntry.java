package edu.unc.cscc.views.model.security;

import org.springframework.security.acls.domain.GrantedAuthoritySid;
import org.springframework.security.acls.model.Sid;

/* kinda like an AccessControlEntry, but doesn't have the upward assoc, so
 * VACL can be immutable.  Yes, this *is* more work than just doing our own
 * micro-ACL implementation...
 */
public final class VACLEntry
{

	private final Sid				sid;
	private final VPermission		permission;
	private final boolean			grant;
	
	public VACLEntry(VSid sid, VPermission permission)
	{
		this(sid, permission, true);
	}
	
	public VACLEntry(GrantedAuthoritySid sid, VPermission permission)
	{
		this(sid, permission, true);
	}
	
	private VACLEntry(Sid sid, VPermission permission, boolean grant)
	{
		this.sid = sid;
		this.permission = permission;
		this.grant = grant;
	}
	
	
	public final Sid
	sid()
	{
		return this.sid;
	}
	
	public final VPermission
	permission()
	{
		return this.permission;
	}
	
	public final boolean
	isGrant()
	{
		return this.grant;
	}
	
	public final boolean
	grants(VPermission permission)
	{
		return this.isGrant() 
				&& this.permission().equals(permission);
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + (grant ? 1231 : 1237);
		result = prime * result
				+ ((permission == null) ? 0 : permission.hashCode());
		result = prime * result + ((sid == null) ? 0 : sid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VACLEntry other = (VACLEntry) obj;
		if (grant != other.grant)
			return false;
		if (permission == null)
		{
			if (other.permission != null)
				return false;
		} else if (!permission.equals(other.permission))
			return false;
		if (sid == null)
		{
			if (other.sid != null)
				return false;
		} else if (!sid.equals(other.sid))
			return false;
		return true;
	}
	
	
}
