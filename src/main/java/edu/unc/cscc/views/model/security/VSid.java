package edu.unc.cscc.views.model.security;

import org.springframework.security.acls.model.Sid;

/**
 * {@link Sid} implementation that uses user ID for identity.
 * 
 * @author Robert Tomsick (rtomsick@unc.edu)
 *
 */
public class VSid 
implements Sid
{

	private static final long	serialVersionUID = 1L;

	
	private final int			userID;
	
	public VSid(int id)
	{
		this.userID = id;
	}
	
	public VSid(User user)
	{
		this(user.id());
	}
	
	
	public final int
	userID()
	{
		return this.userID;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + userID;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VSid other = (VSid) obj;
		if (userID != other.userID)
			return false;
		return true;
	}

	
}
