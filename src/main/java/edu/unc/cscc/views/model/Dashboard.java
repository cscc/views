package edu.unc.cscc.views.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonTypeName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * A dashboard consists of one or more charts in a user-specified order.  The
 * exact details of presentation are dependent on medium; a dashboard serves 
 * only as the logical grouping of charts along with some additional metadata.
 * 
 * @author Robert Tomsick (rtomsick@unc.edu)
 *
 */
@JsonTypeName("dashboard")
@ApiModel(value = "dashboard", 
			description = " A dashboard consists of one or more charts in a "
							+ "user-specified order.")
public final class Dashboard
implements VEntity
{

	private final Integer		id;
	private final String		name;
	private final String		description;
	private final Project		project;
	private final List<Chart>	charts;
	
	public Dashboard(Integer id,
					String name, String description,
					Project project,
					List<Chart> charts)
	{
		Assert.notNull(name, "name cannot be null");
		Assert.notNull(project, "project cannot be null");
		Assert.notNull(project.id(), "project missing ID");
		Assert.isTrue(charts.stream()
							.allMatch(c -> 
								c.project() != null
								&& project.id().equals(c.project().id())),
						"given chart that doesn't belong to project");
		
		this.id = id;
		this.name = name;
		this.description = description;
		this.project = project;
		this.charts = new ArrayList<>(charts);
	}
	
	/**
	 * Get the ID of the dashboard.
	 * 
	 * @return ID, may be <code>null</code>
	 */
	@JsonGetter("id")
	@ApiModelProperty(value = "ID of the dashboard", required = true)
	public final Integer
	id()
	{
		return this.id;
	}
	
	/**
	 * Set the ID of the dashboard.
	 * 
	 * @param id ID, may be <code>null</code>
	 * @return copy of the dashboard with the given ID set
	 */
	public final Dashboard
	id(Integer id)
	{
		return new Dashboard(id, this.name(), this.description(), 
								this.project(), this.charts());
	}
	
	/**
	 * Get the name of the dashboard.  The name is unique within a system.
	 * 
	 * @return name of the dashboard, not <code>null</code>
	 */
	@JsonGetter("name")
	@ApiModelProperty(value = "unique name of the dashboard", 
						example = "sample-dashboard", required = true)
	public final String
	name()
	{
		return this.name;
	}
	
	/**
	 * Set the name of the dashboard.
	 * 
	 * @param name name of the dashboard, not <code>null</code>
	 * @return copy of the dashboard with the given name set
	 */
	public final Dashboard
	name(String name)
	{
		return new Dashboard(this.id(), name, this.description(), 
								this.project(), this.charts());
	}
	
	/**
	 * Get an (optional) human-readable description of the dashboard.
	 * 
	 * @return description, may be <code>null</code>
	 */
	@JsonGetter("description")
	@ApiModelProperty(value = "human-readable description of the dashboard",
						example = "My sample dashboard")
	public final String
	description()
	{
		return this.description;
	}
	
	/**
	 * Set the description of the dashboard.
	 * 
	 * @param description description, may be <code>null</code>
	 * @return copy of the dashboard with the given description set
	 */
	public final Dashboard
	description(String description)
	{
		return new Dashboard(this.id(), this.name(), description, 
							this.project(),
							this.charts());
	}
	
	/**
	 * Get the charts that make up the dashboard.
	 * 
	 * @return charts which make up the dashboard, not <code>null</code>
	 */
	@JsonGetter("charts")
	@ApiModelProperty(value = "charts which make up the dashboard", required = true)
	public final List<Chart>
	charts()
	{
		return Collections.unmodifiableList(this.charts);
	}
	
	/**
	 * Set the charts that make up the dashboard, in the order in which
	 * they should exist in the dashboard.  The given charts must all belong
	 * to the {@link #project() project that the dashboard is associated with}.
	 * 
	 * @param charts charts, not <code>null</code>
	 * @return copy of the dashboard with the given charts set
	 */
	public final Dashboard
	charts(List<Chart> charts)
	{
		return new Dashboard(this.id(), this.name(), this.description(), 
								this.project(), charts);
	}
	
	/**
	 * Get the project that the dashboard is associated with.
	 * 
	 * @return project, not <code>null</code>
	 */
	@JsonGetter("project")
	@ApiModelProperty(value = "project that the dashboard is associated with",
						required = true)
	public final Project
	project()
	{
		return this.project;
	}
	
	/* --- begin swagger workaround --- */
	
	@JsonSetter("id")
	private void
	id_hack(int id)
	{
		throw new IllegalArgumentException("Workaround setter called");
	}
	
	@JsonSetter("name")
	private void
	name_hack(String n)
	{
		throw new IllegalArgumentException("Workaround setter called");
	}
	
	@JsonSetter("description")
	private void
	description_hack(String d)
	{
		throw new IllegalArgumentException("Workaround setter called");
	}
	
	@JsonSetter("project")
	private void
	project_hack(Project p)
	{
		throw new IllegalArgumentException("Workaround setter called");
	}
	
	@JsonSetter("charts")
	private void
	charts_hack(List<Chart> c)
	{
		throw new IllegalArgumentException("Workaround setter called");
	}
	/* --- end swagger workaround --- */
	
}
