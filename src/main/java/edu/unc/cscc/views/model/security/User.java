package edu.unc.cscc.views.model.security;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import edu.unc.cscc.views.model.VEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "user", description = "model of a user within views")
public final class User
implements UserDetails, VEntity
{

	public static final Collection<ViewsRole> DEFAULT_ROLES = 
			Arrays.asList(ViewsRole.ANONYMOUS_USERS, ViewsRole.ALL_USERS);
	
	
	@ApiModel
	public static enum ViewsRole
	{
		@JsonProperty("role_anonymous")
		ANONYMOUS_USERS("role_anonymous"),
		
		@JsonProperty("role_all_users")
		ALL_USERS("role_all_users"),
		
		@JsonProperty("manage_users")
		ROLE_MANAGE_USERS("manage_users"),
		
		@JsonProperty("create_projects")
		ROLE_CREATE_PROJECTS("create_projects"),
		
		@JsonProperty("manage_project_acls")
		ROLE_MANAGE_PROJECT_ACLS("manage_project_acls"),
		
		@JsonProperty("superuser")
		ROLE_SUPERUSER("superuser");
		
		private final String role;
		
		public String role()
		{
			return this.role;
		}
		
		public GrantedAuthority
		authority()
		{
			return new SimpleGrantedAuthority(this.role());
		}
		
		@Override
		public String
		toString()
		{
			return this.role();
		}
		
		private ViewsRole(String role)
		{
			this.role = role;
		}
		
		public static ViewsRole
		parse(String role)
		{
			for (ViewsRole r : ViewsRole.values())
			{
				if (r.role().equals(role))
				{
					return r;
				}
			}
			
			return null;
		}
	}
	
	
	private static final long serialVersionUID = 1L;
	
	private final Integer						id;
	private final String						username;
	private final String						credentialSourceIdentifier;
	private final String						name;
	
	private final boolean						active;
	
	private final Instant						lastModified;
	
	private final Collection<ViewsRole>			roles;
	
	
	public User(Integer id, String username, String credentialSourceIdentifier, 
				String name, Instant lastModified, boolean active,
				Collection<ViewsRole> roles)
	{
		Assert.notNull(username, "missing username");
		Assert.notNull(credentialSourceIdentifier, "missing credential source identifier");
		Assert.notNull(roles, "missing roles");
		this.id = id;
		this.username = username;
		this.credentialSourceIdentifier = credentialSourceIdentifier;
		this.name = (name == null ? "" : name);
		this.lastModified = lastModified;
		this.active = active;
		this.roles = new HashSet<>(roles);
		
		this.roles.add(ViewsRole.ALL_USERS);
		this.roles.add(ViewsRole.ANONYMOUS_USERS);
	}
	
	@JsonGetter("id")
	@ApiModelProperty(value = "the user's unique ID", required = true)
	public Integer
	id()
	{
		return this.id;
	}
	
	/**
	 * Set the ID of the user.
	 * 
	 * @param id new ID for the user; may be <code>null</code>
	 * @return copy of the user with the given ID set
	 */
	public User
	id(int id) 
	{
		return new User(id, this.username(), this.getCredentialSourceID(), 
						this.name(), this.lastModified(), this.active(),
						this.roles());
	}
	
	
	@JsonGetter("username")
	@Size(min = 1)
	@NotNull
	@ApiModelProperty(value = "the user's username, unique within a credential "
								+ "source", 
						example = "sample_username",
						required = true,
						allowEmptyValue = false)
	public String
	username()
	{
		return this.username;
	}
	
	@JsonGetter("name")
	@ApiModelProperty(value = "the user's display name, required but may be blank",
						required = true, allowEmptyValue = true,
						example = "Sample User")
	@NotNull
	public String
	name()
	{
		return this.name;
	}
	
	/**
	 * Set the name of the user.
	 * 
	 * @param name name, not <code>null</code>
	 * @return copy of the user with the given name set
	 */
	public User
	name(String name)
	{
		return new User(this.id(), this.username(), this.getCredentialSourceID(), 
						name, this.lastModified(), this.active(),
						this.roles());
	}
	
	@JsonGetter("active")
	@ApiModelProperty("whether or not the user is active; inactive users are "
						+ "unable to use views")
	public boolean
	active()
	{
		return this.active;
	}
	
	/**
	 * Set whether the user is active.
	 * 
	 * @param active <code>true</code> if the user is active, <code>false</code>
	 * otherwise
	 * @return copy of the user with the given state set
	 */
	public User
	active(boolean active)
	{
		return new User(this.id(), this.username(), this.getCredentialSourceID(), 
				this.name(), this.lastModified(), active,
				this.roles());
	}
	
	@JsonGetter("last-modified")
	@ApiModelProperty("timestamp of the last modification of the user")
	public Instant
	lastModified()
	{
		return this.lastModified;
	}
	
	/**
	 * Set the last modification time of the user.
	 * 
	 * @param when last modification time, not <code>null</code>
	 * @return copy of the user with the given last modification time set
	 */
	public User
	lastModified(Instant when)
	{
		return new User(this.id(), this.username(), this.getCredentialSourceID(), 
						this.name(), when, this.active(),
						this.roles());
	}
	
	@JsonGetter("credential-source-id")
	@ApiModelProperty(value = "identifier of the credential source against which the "
						+ "user must be authenticated",
						allowEmptyValue = false,
						required = true,
						example = "my-sample-cred-source")
	@Size(min = 1)
	public String
	getCredentialSourceID()
	{
		return this.credentialSourceIdentifier;
	}

	@JsonIgnore
	@Override
	public Collection<? extends GrantedAuthority>
	getAuthorities()
	{
		return this.roles().stream().map(r -> r.authority()).collect(Collectors.toSet());
	}
	
	
	@JsonGetter("roles")
	@ApiModelProperty(value = "roles held by the user",
						/* workaround for swagger-core issue #2352,
						 * see https://github.com/swagger-api/swagger-core/issues/2352
						 */
						allowableValues = "role_anonymous, role_all_users, "
								+ "manage_users, create_projects, manage_project_acls, superuser",
						dataType = "string" /* end of workaround */
						)
	public Collection<ViewsRole>
	roles()
	{
		return Collections.unmodifiableCollection(this.roles);
	}
	
	/**
	 * Set the roles of the user.
	 * 
	 * @param roles new roles for user
	 * @return copy of the user with the given roles set
	 */
	public User
	roles(Collection<ViewsRole> roles)
	{
		return new User(this.id(), this.username(), this.getCredentialSourceID(), 
				this.name(), this.lastModified(), this.active(),
				roles);
	}


	@JsonIgnore
	@Override
	public String
	getPassword()
	{
		return null;
	}


	@Override
	public String 
	getUsername()
	{
		return this.username();
	}

	@JsonIgnore
	@Override
	public boolean
	isAccountNonExpired()
	{
		return true;
	}

	@JsonIgnore
	@Override
	public boolean
	isAccountNonLocked()
	{
		return true;
	}

	@JsonIgnore
	@Override
	public boolean
	isCredentialsNonExpired()
	{
		return true;
	}

	@JsonIgnore
	@Override
	public boolean 
	isEnabled()
	{
		return this.active();
	}

	@JsonCreator
	public static final User
	create(@JsonProperty("id") Integer id, 
			@JsonProperty("username") String username, 
			@JsonProperty("credential-source-id") String credentialSourceIdentifier, 
			@JsonProperty("name") String name, 
			@JsonProperty("last-modified") Instant lastModified, 
			@JsonProperty("active") boolean active,
			@JsonProperty("roles") Collection<String> roles)
	{
		return new User(id, username, credentialSourceIdentifier, name, 
						lastModified, active, 
						roles.stream().map(ViewsRole :: parse)
								.collect(Collectors.toSet()));
	}
	
	/* --- begin swagger-core workarounds --- */
	
	@JsonSetter("id")
	private void
	id_hack(int id)
	{
		throw new IllegalArgumentException("Workaround setter called");
	}
	@JsonSetter("username")
	private void
	username_hack(String s)
	{
		throw new IllegalArgumentException("Workaround setter called");
	}
	@JsonSetter("credential-source-id")
	private void
	csid_hack(String s)
	{
		throw new IllegalArgumentException("Workaround setter called");
	}
	@JsonSetter("name")
	private void
	name_hack(String s)
	{
		throw new IllegalArgumentException("Workaround setter called");
	}
	@JsonSetter("last-modified")
	private void
	lm_hack(Object o)
	{
		throw new IllegalArgumentException("Workaround setter called");
	}
	@JsonSetter("active")
	private void
	active_hack(boolean a)
	{
		throw new IllegalArgumentException("Workaround setter called");
	}
	@JsonSetter("roles")
	private void
	roles_hack(Collection<String> r)
	{
		throw new IllegalArgumentException("Workaround setter called");
	}
	
	/* --- end workarounds --- */
}
