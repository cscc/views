package edu.unc.cscc.views.model.security;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

public class ChangeAuthSourceRequest { 
    
    private String email;
    
    @JsonGetter("email")
    public String getEmail() {
        return email;
    }

    @JsonSetter("email")
    public void setEmail(String email) {
        this.email = email;
    }
    private int userId;

    @JsonGetter("userId")
    public int getUserId() {
        return userId;
    }

    @JsonGetter("userId")
    public void setUserId(int userId) {
        this.userId = userId;
    }

}
