package edu.unc.cscc.views.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("email")
public class Email {
    
    private String recipient;
    private String body;
    private String subject;

    public Email() {
        
    }

    public Email(String recipient, String body, String subject) {
        this.body = body;
        this.subject = subject;
        this.recipient = recipient;
    }

    @JsonGetter("body")
    public String getBody() {
        return body;
    }

    @JsonSetter("body")
    public void setBody(String body) {
        this.body = body;
    }


    @JsonGetter("subject")
    public String getSubject() {
        return subject;
    }

    @JsonSetter("subject")
    public void setSubject(String subject) {
        this.subject = subject;
    }

    @JsonGetter("recipient")
    public String getRecipient() {
        return recipient;
    }

    @JsonSetter("recipient")
    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

}
