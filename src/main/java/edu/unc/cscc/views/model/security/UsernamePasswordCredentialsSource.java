package edu.unc.cscc.views.model.security;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonGetter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * A source against which username/password credentials may be checked.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@ApiModel("views-credential-source")
public interface UsernamePasswordCredentialsSource
{

	/**
	 * A unique identifier for the source.
	 * 
	 * @return identifier
	 */
	@JsonGetter("identifier")
	@ApiModelProperty(required = true, 
						value = "unique identifier for the credential source",
						example = "sample-cred-src")
	public String identifier();
	
	/**
	 * A human-readable name of the credential source.
	 * 
	 * @return name, not <code>null</code>
	 */
	@JsonGetter("name")
	@ApiModelProperty(value = "human-readable name of the credential source",
						required = true,
						example = "Sample Source")
	public String name();
	
	/**
	 * A human-readable description of the source.
	 * 
	 * @return description, not <code>null</code>
	 */
	@JsonGetter("description")
	@ApiModelProperty(value = "human-readable brief description of the credential "
						+ "source (required, but may be blank)",
					example = "Sample credential source", 
					required = true)
	public String description();
	
	/**
	 * Attempt to authenticate the given username/password against the 
	 * credentials known by the source.
	 * 
	 * @param username username to authenticate
	 * @param password password corresponding to given username
	 * @return <code>true</code> if the credentials are valid, <code>false</code>
	 * otherwise
	 * @throws IOException thrown if an IO error occurred while attempting
	 * authentication
	 */
	public boolean authenticate(String username, String password)
	throws IOException;
	
}
