package edu.unc.cscc.views.model;

import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonGetter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * A dataset.  contains data intended for consumption by one or more charts.  
 * The contents of a dataset are often in a format such CSV or some other 
 * format easily consumed by D3 and/or WebCharts, although no such restriction
 * is imposed on the dataset's {@link #contentType() content type} at the 
 * application level.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@ApiModel(value = "dataset",
			description = "A dataset contains data intended for consumption "
							+ "by one or more charts.  The contents of a "
							+ "dataset are often in a format such CSV or "
							+ "some other format easily consumed by D3 "
							+ "and/or WebCharts.")
public final class DataSet
implements VEntity
{
	
	private final Integer	id;
	private final Project	project;
	private final String	identifier;
	private final String	description;
	private final String	contentType;
	private final byte[]	data;
	
	public DataSet(Project project, String identifier, 
					String description, String contentType,
					byte[] data)
	{
		this(null, project, identifier, description, contentType, data);
	}
	
	public DataSet(Integer id, Project project, 
					String identifier, 
					String description, String contentType,
					byte[] data)
	{
		Assert.notNull(project, "missing project");
		Assert.notNull(identifier, "missing identifier");
		this.id = id;
		this.project = project;
		this.identifier = identifier;
		this.description = description == null ? "" : description;
		this.contentType = contentType;
		this.data = data;
	}
	
	@JsonGetter("id")
	@ApiModelProperty(value = "ID of the dataset", required = true)
	@Override
	public Integer
	id()
	{
		return this.id;
	}
	
	public DataSet
	id(Integer id)
	{
		return new DataSet(id, this.project(),
							this.identifier(), this.description(), 
							this.contentType(), this.data());
	}
	
	@JsonGetter("project")
	@ApiModelProperty(value = "project to which dataset belongs", required = true)
	public Project
	project()
	{
		return this.project;
	}
	
	@JsonGetter("identifier")
	@ApiModelProperty(value = "identifier of dataset", required = true,
						example = "chosen-languages-csv")
	public String
	identifier()
	{
		return this.identifier;
	}
	
	public DataSet
	identifier(String identifier)
	{
		return new DataSet(this.id(), this.project(),
							identifier, this.description(),
							this.contentType(), this.data());
	}
	
	@JsonGetter("description")
	@ApiModelProperty(value = "human-readable description of the dataset", 
						required = true,
						example = "Dataset of preferred languages of "
								+ "programmers in CSV format.",
						allowEmptyValue = true)
	public String
	description()
	{
		return this.description;
	}
	
	public DataSet
	description(String description)
	{
		return new DataSet(this.id(), this.project(), this.identifier(), description,
				this.contentType(), this.data());
	}
	
	@JsonGetter("content-type")
	@ApiModelProperty(value = "Content-Type of the dataset's payload.", 
						required = true, example = "text/csv")
	public String
	contentType()
	{
		return this.contentType;
	}
	
	public DataSet
	contentType(String contentType)
	{
		return new DataSet(this.id(), this.project(), this.identifier(), this.description(),
							contentType, this.data());
	}
	
	@JsonGetter("has-data")
	@ApiModelProperty(value = "Whether the dataset has data associated with it.  "
								+ "This property may be used to distinguish between "
								+ "shallow and fully-loaded datasets. If this "
								+ "property is false, then the 'data' property "
								+ "will not contain data and may not be present.", 
						required = true,
						example = "true")
	public boolean
	hasData()
	{
		return this.data != null;
	}
	
	@JsonGetter("data")
	@ApiModelProperty(value = "Data payload of the dataset.  If 'has-data' is "
								+ "'false', this property should not be used "
								+ "and may not be present.", required = false,
						example = "bGFuZ3VhZ2UsYWRvcHRpb24gcmF0ZQ0KQ2xvanVyZSw1JQ0KV29yc2UgbGFuZ3VhZ2VzLDk1JQ==", 
						dataType = "byte")
	public byte[]
	data()
	{
		return this.data;
	}
	
	public DataSet
	data(byte[] data)
	{
		return new DataSet(this.id(), this.project(), this.identifier(), this.description(),
				this.contentType(), data);
	}

	public DataSet
	project(Project project)
	{
		return new DataSet(this.id(), project, this.identifier(), this.description(),
								this.contentType(), this.data());
	}
	
}
