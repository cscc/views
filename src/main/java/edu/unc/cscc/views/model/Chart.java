package edu.unc.cscc.views.model;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonTypeName;

import edu.unc.cscc.views.model.security.User;

@JsonTypeName("chart")
public class Chart
implements VEntity
{
	
	private final Integer					id;
	
	private final User						creator;
	
	private final Project					project;
	
	private final Instant					created;
	private final Instant					lastModified;
		
	private final String					mainResource;
	
	private final String					name;
	private final String					description;
	
	private final Collection<DataSet>		requiredDatasets;
	

	public Chart(Integer id, User creator, Project project,
					String name, String description,
					Instant created, Instant lastModified, 
					String mainResourcePath, Collection<DataSet> requiredDatasets)
	{
		Assert.notNull(name, "name cannot be null");
		Assert.notNull(project, "project cannot be null");
		this.id = id;
		this.creator = creator;
		this.project = project;
		this.name = name;
		this.description = description;
		this.created = created;
		this.lastModified = lastModified;
		this.mainResource = mainResourcePath;
		this.requiredDatasets = new ArrayList<>(requiredDatasets);
	}

	@Override
	public Integer
	id()
	{
		return id;
	}
	

	@JsonGetter("name")
	public String 
	name()
	{
		return name;
	}
	
	public Chart
	name(String name)
	{
		return new Chart(this.id(), this.creator(), this.project(),
							name, this.description(), 
							this.created(), this.lastModified(),
							this.mainResourcePath(), this.datasets());
	}

	@JsonGetter("description")
	public String 
	description()
	{
		return description;
	}
	
	public Chart
	description(String description)
	{
		return new Chart(this.id(), this.creator(), this.project(),
				this.name(), description, 
				this.created(), this.lastModified(),
				this.mainResourcePath(), this.datasets());
	}

	public User
	creator()
	{
		return creator;
	}
	
	@JsonGetter("project")
	public Project
	project()
	{
		return this.project;
	}
	
	public Chart
	project(Project project)
	{
		return new Chart(this.id(), this.creator(), project,
				this.name(), description, 
				this.created(), this.lastModified(),
				this.mainResourcePath(), this.datasets());
	}
	
	@JsonGetter("creator-id")
	public final Integer
	creatorID()
	{
		return this.creator.id();
	}

	@JsonGetter("created")
	public Instant
	created()
	{
		return created;
	
	}

	@JsonGetter("last-modified")
	public Instant 
	lastModified() 
	{
		return lastModified;
	}

	@JsonGetter("main-resource-path")
	public String
	mainResourcePath()
	{
		return mainResource;
	}
	
	/**
	 * The datasets that are required for execution/use of this chart.
	 * 
	 * @return datasets, not <code>null</code>
	 */
	@JsonGetter("datasets")
	public Collection<DataSet>
	datasets()
	{
		return Collections.unmodifiableCollection(this.requiredDatasets);
	}

	/**
	 * Set the ID of the chart.
	 * 
	 * @param id ID of the chart, may be <code>null</code>
	 * @return copy of the chart with the given ID set, not <code>null</code>
	 */
	public Chart 
	id(Integer id)
	{
		return new Chart(id, this.creator(), this.project(), 
							this.name(), this.description(),
							this.created(), this.lastModified(), 
							this.mainResourcePath(), this.datasets());
	}

	/**
	 * Set the datasets that are required for execution/use of this chart.
	 * 
	 * @param datasets datasets, not <code>null</code>, may be empty
	 * @return copy of the chart with the given datasets set
	 */
	public Chart 
	datasets(Collection<DataSet> datasets)
	{
		return new Chart(this.id(), this.creator(), this.project(), 
				this.name(), this.description(),
				this.created(), this.lastModified(), 
				this.mainResourcePath(), datasets);
	}
}
