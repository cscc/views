package edu.unc.cscc.views.service.storage;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.InsertValuesStep2;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import edu.unc.cscc.views.model.Chart;
import edu.unc.cscc.views.model.DataSet;
import edu.unc.cscc.views.model.Project;
import edu.unc.cscc.views.model.security.User;
import edu.unc.cscc.views.model.security.VPermission;
import edu.unc.cscc.views.service.VSec;

import static org.jooq.impl.DSL.*;

@Service
public final class ChartStorageServiceImpl
implements ChartStorageService
{
	
	static final Table<?> 	TABLE = DSL.table(name("charts"));
	static final Table<?>	DS_CHART_TABLE = DataSetStorageImpl.DS_CHART_TABLE;

	private final DSLContext 				dsl;
	private final DataSetStorage			dataSetStorage;
	private final ProjectStorageService		projectService;
	private final UserStorage				userStorage;
	private final VSec						sec;
	
	@Autowired
	protected ChartStorageServiceImpl(DSLContext dsl, UserStorage userStorage, 
										ProjectStorageService projectService,
										DataSetStorage dataSetStorage,
										VSec sec)
	{
		this.dsl = dsl;
		this.projectService = projectService;
		this.userStorage = userStorage;
		this.dataSetStorage = dataSetStorage;
		this.sec = sec;
	}
	
	/* not secured for reasons for efficiency: we'd have to load the ds IDs
	 * to determine if we can read them, and exposing existence by ID isn't a 
	 * serious disclosure vuln.  What can they do with an ID?  Not much.
	 */
	@Override
	public boolean 
	exists(int id)
	{
		return this.dsl.fetchExists(this.dsl
					.select(field(name("id")))
					.from(TABLE)
					.where(field(name("id")).eq(id)));
	}
	
	@Override
	public boolean
	exists(String name, Project project)
	{
		Assert.notNull(project, "project was null");
		Assert.notNull(name, "name was null");
		
		if (project.id() == null)
		{
			return false;
		}
		
		return this.dsl.fetchExists(
						this.dsl
							.select(field(name("id")))
							.from(TABLE)
							.where(field(name("name"), String.class).eq(name))
							.and(field(name("id__projects")).eq(project.id())));
	}
	
	
	/* not secured since it's necessary to figure out interdependencies even
	 * if the initiating user can't read the charts in question
	 */
	@Override
	public Collection<Integer> 
	findByDataSet(int dataSetID) 
	{
		return this.dsl
					.select(field(name("id__charts")))
						.from(DataSetStorageImpl.DS_CHART_TABLE)
					.where(field(name("id__datasets")).eq(dataSetID))
					.fetch(field(name("id__charts"), Integer.class));
	}

	@Override
	public Chart
	load(int id) 
	{
		return this.load(Collections.singleton(id)).get(id);
	}	

	@Override
	public Map<Integer, Chart>
	load(Collection<Integer> ids)
	{
		Map<Integer, Collection<DataSet>> ds = 
				this.dataSetStorage.loadShallowByChart(ids);
		
		Map<Integer, Project> projects = 
				this.projectService
						.load(this.dsl
								.select(field(name("id")), field(name("id__projects")))
								.from(TABLE)
								.where(field(name("id")).in(ids))
								.fetch(field(name("id__projects"), Integer.class)));
		
		return this.dsl
					.selectFrom(TABLE)
					.where(field(name("id"), Integer.class).in(ids))
					.fetchMap(field(name("id"), Integer.class), r -> {
						
						User user = this.userStorage.load(r.get("creator", Integer.class));
						
						if (user == null)
						{
							return null;
						}
						
						final Integer id = r.get("id", Integer.class);
						
						final Collection<DataSet> datasets =
								(ds.containsKey(id) ? ds.get(id) : Collections.emptySet());
						
						
						Chart chart = 
								new Chart(id, user, 
											projects.get(r.get(name("id__projects"), Integer.class)),
											r.get(name("name"), String.class), 
											r.get(name("description"), String.class), 
											r.get(name("created"), Timestamp.class).toInstant(), 
											r.get(name("modified"), Timestamp.class).toInstant(), 
											r.get(name("main_resource_path"), String.class),
											datasets);
								
						return chart;
					});
	}

	@PreAuthorize("@vsec.chartAllowed(#id, 'write')")
	@Override
	public void 
	delete(int id)
	{
		this.dsl.deleteFrom(TABLE).where(field(name("id")).eq(id)).execute();
	}

	@PreAuthorize("(#chart.id() == null "
					+ " and @vsec.projectAllowed(#chart.project().id(), 'create-charts')) "
				+ " or @vsec.chartAllowed(#chart.id(), 'write')")
	@Override
	@Transactional
	public Chart 
	save(Chart chart)
	{
		Assert.notNull(chart.project(), "chart must have non-null project");
		
		if (chart.project().id() == null)
		{
			throw new IllegalArgumentException("Cannot save chart associated "
												+ "with non-existent project");
		}
		
		/* check that all of the associated data sets exist before saving... */
		
		if (chart.datasets().stream().anyMatch(d -> d.id() == null))
		{
			throw new IllegalArgumentException(
					"Cannot save chart associated with non-existant dataset");
		}
		
		
		if (chart.id() == null)
		{
			Integer id = 
					this.dsl
						.insertInto(TABLE)
							.set(field(name("creator")), chart.creator().id())
							.set(field(name("name")), chart.name())
							.set(field(name("id__projects")), chart.project().id())
							.set(field(name("description")), chart.description())
							.set(field(name("created")), Timestamp.from(Instant.now()))
							.set(field(name("modified")), Timestamp.from(Instant.now()))
							.set(field(name("main_resource_path")), chart.mainResourcePath())
						.returning(field(name("id"), Integer.class))
						.fetchOne()
						.get(field(name("id"), Integer.class));
			
			chart = chart.id(id);
					
		}
		else
		{
			this.dsl
				.update(TABLE)
					.set(field(name("creator")), chart.creator().id())
					.set(field(name("name")), chart.name())
					.set(field(name("id__projects")), chart.project().id())
					.set(field(name("description")), chart.description())
					.set(field(name("created")), Timestamp.from(Instant.now()))
					.set(field(name("modified")), Timestamp.from(Instant.now()))
					.set(field(name("main_resource_path")), chart.mainResourcePath())
				.where(field(name("id")).eq(chart.id()))
				.execute();
			
			/* nuke chart<->ds assoc */
			
			this.dsl.deleteFrom(DS_CHART_TABLE)
					.where(field(name("id__charts")).eq(chart.id()))
					.execute();
		}
		
		/* persist dataset associations */
		
		InsertValuesStep2<?, Integer, Integer> s = 
				this.dsl
					.insertInto(DS_CHART_TABLE)
					.columns(field(name("id__charts"), Integer.class), 
							field(name("id__datasets"), Integer.class));
		
		for (DataSet set : chart.datasets())
		{
			s = s.values(chart.id(), set.id());
		}
		
		s.execute();
		
		
		return chart;
	}

	/* secured programmatically */
	@Override
	public List<Integer> 
	search(User user, String search, Project project, Integer limit, Integer offset)
	{
		Assert.notNull(project, "project must not be null");
		if (project.id() == null)
		{
			return Collections.emptyList();
		}
		
		final int o = (offset == null) ? 0 : offset;
		final int l = (limit == null) ? 10 : limit;
		
		Assert.isTrue(o >= 0, "offset must be >= 0");
		Assert.isTrue(l < 100000 && l > 0, "length OOB");
		
		Condition searchClause;
		
		if (search == null || search.trim().isEmpty())
		{
			searchClause = DSL.trueCondition();
		}
		else
		{
			searchClause = 
					DSL.lower(field(name("name"), String.class))
						.contains(search.trim().toLowerCase())
						.or(DSL.lower(field(name("description"), String.class))
								.contains(search.trim().toLowerCase()));
		}
		
		
		
		List<Integer> found = 
				this.dsl.select(field(name("id")))
						.from(TABLE)
						.where(searchClause)
						.and(field(name("id__projects")).eq(project.id()))
						.fetch(field(name("id"), Integer.class));
		
		
		Map<Integer, List<Integer>> chartToDS =
				this.dsl.select(field(name("id__charts")), field(name("id__datasets")))
						.from(DS_CHART_TABLE)
						.where(field(name("id__charts")).in(found))
						.fetchGroups(field(name("id__charts"), Integer.class),
									field(name("id__datasets"), Integer.class));
		
		found = found.stream()
						.filter(cid -> 
							! chartToDS.containsKey(cid)
							|| chartToDS.get(cid)
									.stream()
									.allMatch(dsid -> 
										this.sec.datasetAllowed(dsid, VPermission.READ)))
						.collect(Collectors.toCollection(ArrayList :: new));
		
		
		if (found.size() < o)
		{
			return Collections.emptyList();
		}
		
		final int bound = (found.size() < o + l) ? found.size() : o + l;
		
		return found.subList(o, bound);
	}
	
	
	/* FIXME: secured programmatically, but wasteful! */
	@Override
	public int 
	count(User user, String search, Project project)
	{
		Assert.notNull(project, "project must not be null");
		
		if (project.id() == null)
		{
			return 0;
		}
		
		Condition searchClause;
		
		if (search == null || search.trim().isEmpty())
		{
			searchClause = DSL.trueCondition();
		}
		else
		{
			searchClause = 
					DSL.lower(field(name("name"), String.class))
						.contains(search.trim().toLowerCase())
						.or(DSL.lower(field(name("description"), String.class))
								.contains(search.trim().toLowerCase()));
		}
		
		
		
		List<Integer> found = 
				this.dsl.select(field(name("id")))
						.from(TABLE)
						.where(searchClause)
						.and(field(name("id__projects")).eq(project.id()))
						.fetch(field(name("id"), Integer.class));
		
		
		Map<Integer, List<Integer>> chartToDS =
				this.dsl.select(field(name("id__charts")), field(name("id__datasets")))
						.from(DS_CHART_TABLE)
						.where(field(name("id__charts")).in(found))
						.fetchGroups(field(name("id__charts"), Integer.class),
									field(name("id__datasets"), Integer.class));
		
		found = found.stream()
						.filter(cid -> 
							! chartToDS.containsKey(cid)
							|| chartToDS.get(cid)
									.stream()
									.allMatch(dsid -> 
										this.sec.datasetAllowed(dsid, VPermission.READ)))
						.collect(Collectors.toCollection(ArrayList :: new));
		
		
		return found.size();
		
	}

	/* not secured since ID disclosure isn't a security issue */
	@Override
	public Integer 
	findID(String name, Project project)
	{
		Assert.notNull(project, "project must not be null");
		
		if (project.id() == null)
		{
			return null;
		}
		
		return this.dsl.select(field(name("id")))
						.from(TABLE)
						.where(field(name("name"), String.class).eq(name))
						.and(field(name("id__projects")).eq(project.id()))
						.fetchOne(field(name("id"), Integer.class));
	}

}
