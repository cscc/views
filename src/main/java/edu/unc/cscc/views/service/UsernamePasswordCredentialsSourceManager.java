package edu.unc.cscc.views.service;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;

import edu.unc.cscc.views.model.security.UsernamePasswordCredentialsSource;

@Service
public class UsernamePasswordCredentialsSourceManager
{
	
	private final Map<String, UsernamePasswordCredentialsSource> sources;
	
	public UsernamePasswordCredentialsSourceManager()
	{
		this.sources = new ConcurrentHashMap<>();
	}
	
	public UsernamePasswordCredentialsSource
	lookupSource(String sourceID)
	{
		if (this.sources.containsKey(sourceID))
		{
			return this.sources.get(sourceID);
		}
		return null;
	}
	
	public void
	addSource(UsernamePasswordCredentialsSource source)
	{
		this.sources.put(source.identifier(), source);
	}

	public Collection<UsernamePasswordCredentialsSource>
	sources()
	{
		return Collections.unmodifiableCollection(this.sources.values());
	}

}
