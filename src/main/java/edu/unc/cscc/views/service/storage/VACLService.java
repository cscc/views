package edu.unc.cscc.views.service.storage;

import java.util.Collection;

import org.springframework.security.acls.domain.GrantedAuthoritySid;
import org.springframework.security.acls.model.Sid;
import org.springframework.security.core.GrantedAuthority;

import edu.unc.cscc.views.model.security.ChartOID;
import edu.unc.cscc.views.model.security.DashboardOID;
import edu.unc.cscc.views.model.security.DataSetOID;
import edu.unc.cscc.views.model.security.ProjectOID;
import edu.unc.cscc.views.model.security.User;
import edu.unc.cscc.views.model.security.VACL;
import edu.unc.cscc.views.model.security.VSid;

public interface VACLService 
{

	/**
	 * Save the given ACL to underlying storage.  If the given ACL is 
	 * {@link VACL#isPartial() partial}, then only the {@link Sid identities} 
	 * represented by the ACL will have their permissions updated, non-represented
	 * identities' entries will be unchanged
	 * 
	 * @param acl ACL to save
	 * @return copy of the ACL reflecting storage after persistence
	 */
	public VACL	save(VACL acl);
	
	public VACL load(ChartOID chartOID);
	
	public VACL load(DataSetOID datasetOID);
	
	public VACL load(ProjectOID projectOID);
	
	public VACL load(DashboardOID dashboardOID);
	
	/**
	 * Load the ACL entries for the specified chart that pertain to the given
	 * user either via {@link VSid direct association} or via
	 * {@link GrantedAuthoritySid granted authorities}.
	 * 
	 * @param chartOID
	 * @param user
	 * @return
	 */
	public VACL load(ChartOID chartOID, User user);
	
	public VACL load(DataSetOID datasetOID, User user);
	
	public VACL load(ProjectOID projectOID, User user);
	
	public VACL load(DashboardOID dashboardOID, User user);

	
	
	public VACL load(ChartOID chartOID, Collection<GrantedAuthority> authorities);
	
	public VACL load(DataSetOID datasetOID, Collection<GrantedAuthority> authorities);
	
	public VACL load(ProjectOID projectOID, Collection<GrantedAuthority> authorities);

	public VACL load(DashboardOID dashboardOID, Collection<GrantedAuthority> authorities);

}
