package edu.unc.cscc.views.service;

import java.io.IOException;

import edu.unc.cscc.views.model.security.UsernamePasswordCredentialsSource;

public class Auth0CredentialsSource implements UsernamePasswordCredentialsSource {

    public static final String IDENTIFIER = "auth0";

    @Override
    public String identifier() {
        return IDENTIFIER;
    }

    @Override
    public String name() {
        return "Auth0";
    }

    @Override
    public String description() {
        return "Authenticate using Auth0";
    }

    @Override
    public boolean authenticate(String username, String password) throws IOException {
        throw new UnsupportedOperationException("Auth0 login occurs by redirecting the user to /oauth2/authorization/auth0");
    }
    
}
