package edu.unc.cscc.views.service.storage;

import static org.jooq.impl.DSL.*;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import org.jooq.DSLContext;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StreamUtils;

import edu.unc.cscc.views.model.Chart;
import edu.unc.cscc.views.model.ChartResource;

@Service
public class ResourceServiceImpl
implements ResourceService
{
	
	private static final Table<?> TABLE = DSL.table(name("chart_resources"));
	
	private final ChartStorageService		chartService;
	private final DSLContext 				dsl;
	
	@Autowired
	protected ResourceServiceImpl(DSLContext dsl, ChartStorageService chartService)
	{
		this.dsl = dsl;
		this.chartService = chartService;
	}
	

	@Override
	public boolean
	exists(int id)
	{
		return this.dsl.fetchExists(
				this.dsl.selectOne()
						.from(TABLE)
						.where(field(name("id")).eq(id)));
	}

	@Override
	public ChartResource
	load(int id) 
	{
		return this.load(Collections.singleton(id)).get(id);
	}
	
	@Override
	public Map<Integer, ChartResource> 
	load(Collection<Integer> ids)
	{
		return this.dsl
				.selectFrom(TABLE)
				.where(field(name("id")).in(ids))
				.fetchMap(field(name("id"), Integer.class),
					r -> {
					
						final int chartID = r.get(name("chart"), Integer.class);
						
						final Chart chart = this.chartService.load(chartID);
						
						if (chart == null)
						{
							return null;
						}
						
						return new ChartResource(r.get(name("id"), Integer.class),
												r.get(name("path"), String.class),
												chart, r.get(name("data"), byte[].class));
						
					});
	}

	
	

	@Override
	public Collection<Integer> 
	find(Chart chart)
	{
		if (chart == null || chart.id() == null)
		{
			return Collections.emptyList();
		}
		
		return this.dsl
					.select(field(name("id")))
					.from(TABLE)
					.where(field(name("chart")).eq(chart.id()))
					.fetch(field(name("id"), Integer.class));
	}


	@Override
	public ChartResource 
	loadShallow(String path, int chartID) 
	{
		Chart chart = this.chartService.load(chartID);
		
		if (chart == null)
		{
			return null;
		}
		
		return this.dsl
					.select(field(name("id"), Integer.class), field(name("path")))
					.from(TABLE)
					.where(field(name("path")).eq(path))
					.and(field(name("chart")).eq(chartID))
					.fetchOne(r -> 
						new ChartResource(r.get(name("id"), Integer.class), path, 
											chart, null));
	}
		
	@Override
	public void
	loadData(int resourceID, OutputStream os) 
	throws IOException 
	{
		
		/* TODO - jOOQ doesn't have streaming support yet */
		
		byte[] b =
				this.dsl.select(field(name("data"), byte[].class))
						.from(TABLE)
						.where(field(name("id")).eq(resourceID))
						.fetchOne(field(name("data"), byte[].class));
		
		if (b == null)
		{
			return;
		}
		
		StreamUtils.copy(b, os);
		
	}

	@Override
	@Transactional
	public void 
	delete(int id) 
	{
		this.dsl
			.deleteFrom(TABLE)
			.where(field(name("id")).eq(id))
			.execute();
	}

	@Override
	@Transactional
	public ChartResource 
	save(ChartResource entity)
	{
		if (entity.chart().id() == null)
		{
			throw new IllegalArgumentException("Chart must exist");
		}
		
		if (entity.id() == null)
		{
			if (entity.isShallow())
			{
				throw new IllegalArgumentException(
						"Initial persistence of resource requires non-shallow "
						+ "resource");
			}
			
			Integer id = 
					this.dsl
						.insertInto(TABLE)
						.set(field(name("chart")), entity.chart().id())
						.set(field(name("data")), entity.data())
						.set(field(name("path")), entity.path())
						.returning(field(name("id"), Integer.class))
						.fetchOne().get(field(name("id"), Integer.class));
			
			Assert.notNull(id, "database failed to produce ID");
			
			return entity.id(id);
		}
		
		if (entity.isShallow())
		{
			this.dsl.update(TABLE)
					.set(field(name("path")), entity.path())
					.set(field(name("chart")), entity.chart().id())
					.where(field(name("id")).eq(entity.id()))
					.execute();
		}
		else
		{
			this.dsl.update(TABLE)
					.set(field(name("path")), entity.path())
					.set(field(name("chart")), entity.chart().id())
					.set(field(name("data")), entity.data())
					.where(field(name("id")).eq(entity.id()))
					.execute();
		}
		
		return entity;
	}

	@Override
	public boolean 
	exists(String path, int chartID)
	{
		return this.dsl
					.fetchExists(
						this.dsl
							.selectOne()
							.from(TABLE)
							.where(field(name("chart")).eq(chartID))
							.and(field(name("path")).eq(path))
							.limit(1));
	}
	
	
}
