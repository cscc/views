package edu.unc.cscc.views.service.storage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import edu.unc.cscc.views.model.DataSet;
import edu.unc.cscc.views.model.Project;
import edu.unc.cscc.views.model.security.User;

/**
 * Service for storage of {@link DataSet datasets}.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
public interface DataSetStorage
extends StorageService<DataSet>
{
	/**
	 * Enumeration of the various ways that datsets may be sorted during 
	 * search.
	 * 
	 * @author Rob Tomsick (rtomsick@unc.edu)
	 *
	 */
	public static enum SortDataSetBy
	{
		/**
		 * Sort by {@link DataSet#identifier() dataset identifier}
		 */
		IDENTIFIER,
		/**
		 * Sort by {@link DataSet#description() dataset description}
		 */
		DESCRIPTION,
		/**
		 * Sort by {@link DataSet#contentType() dataset content type}
		 */
		CONTENT_TYPE;
	}
	
	/**
	 * Determine whether a dataset with the given identifier exists in 
	 * the given project.
	 * 
	 * @param identifier identifier of dataset to check for, not <code>null</code>
	 * @param project project within which to check for dataset with the given
	 * identifier, not <code>null</code>
	 * @return <code>true</code> if the dataset exists, <code>false</code> 
	 * otherwise
	 */
	public boolean
	exists(String identifier, Project project);
	
	/**
	 * <p>
	 * Save the given dataset to the underlying storage.  If the given dataset
	 * exists, it will only have its {@link DataSet#data() data} payload updated
	 * if the given entity {@link DataSet#hasData() has data}.  If the given
	 * entity does not contain data, the other properties of the dataset will
	 * be updated, but the existing data left as-is.
	 * </p>
	 * 
	 * <p>
	 * If the given dataset does not exist then it <b>must</b> 
	 * {@link DataSet#hasData() have data} associated with it.
	 * </p>
	 * @param set dataset to save, not <code>null</code>
	 * @return copy of the dataset reflecting underlying storage, not 
	 * <code>null</code>
	 */
	@Override
	public DataSet
	save(DataSet set);
	
	/**
	 * Save the data in the given input stream to underlying storage as the 
	 * data for the specified dataset.  The given stream will be read until
	 * exhaustion, but will not be closed.  If the specified dataset does 
	 * not exist, no changes will occur.
	 * 
	 * @param id id of dataset, &gt; 0
	 * @param data data stream to save, not <code>null</code>
	 * @throws IOException thrown if an error occurred while reading the stream
	 */
	public void
	saveData(int id, InputStream data)
	throws IOException;
	
	/**
	 * Load the dataset with the given identifier if one exists in the 
	 * given project.
	 * 
	 * @param identifier identifier of dataset to load, not <code>null</code>
	 * @param project project from which to load dataset with the given
	 * identifier
	 * @param shallow <code>true</code> if the dataset should be loaded
	 * shallowly, <code>false</code> otherwise
	 * @return dataset with the given identifier, or <code>null</code> if no
	 * such dataset exists
	 */
	public DataSet
	loadByIdentifier(String identifier, Project project, boolean shallow);
	
	/**
	 * Load the data associated with the specified dataset, writing
	 * it to the given output stream.  The given stream will not be 
	 * explicitly flushed or closed.  If the specified dataset does
	 * not exist, the given stream will not be touched.
	 * 
	 * @param id ID of dataset to load
	 * @param os stream to which to write, not <code>null</code>
	 * @throws IOException thrown if writing to the given stream failed
	 */
	public void
	loadData(int id, OutputStream os)
	throws IOException;
	
	
	/**
	 * Load the specified dataset, excluding the associated data.
	 * 
	 * @param id ID of dataset to load
	 * @return dataset with the given ID, or <code>null</code> if no such 
	 * dataset exists
	 */
	public DataSet
	loadShallow(int id);
	
	/**
	 * Load the specified datasets, excluding the associated data.
	 * 
	 * @param ids IDs of datasets to load
	 * @return map of requested IDs to loaded datasets, not <code>null</code>;
	 *  any datasets that were not found will not have a mapping
	 */
	public Map<Integer, DataSet>
	loadShallow(Collection<Integer> ids);
	
	/**
	 * Shallowly load all datasets on which the specified chart depends.
	 * 
	 * @param chartID ID of the chart for which to load datasets, &ge; 0
	 * @return all datasets (sans data) associated with the specified chart, 
	 * not <code>null</code>, may be empty
	 */
	public Collection<DataSet>
	loadShallowByChart(int chartID);
	
	/**
	 * Shallowly load all datasets on which the specified charts depends.
	 * 
	 * @param chartIDs IDs of the charts for which to load datasets, all 
	 * entries must be &ge; 0
	 * @return mapping of chart ID to associated datasets (sans data), not
	 * <code>null</code>
	 */
	public Map<Integer, Collection<DataSet>>
	loadShallowByChart(Collection<Integer> chartIDs);
	
	/**
	 * Search for datasets matching the given search term which are available
	 * to the given user.
	 * 
	 * @param user user, not <code>null</code>
	 * @param term term for which to search, may be <code>null</code>
	 * @param project project within which to search, not <code>null</code>
	 * @param limit maximum number of results, may be <code>null</code> if 
	 * service-determined default should be used
	 * @param offset starting offset of search results, may be <code>null</code>
	 * if returned results should start from the first found dataset
	 * @param sortBy what to the sort results by, not <code>null</code>
	 * @param descending <code>true</code> to sort in descending order, 
	 * <code>false</code> otherwise
	 * @return IDs of the datasets matching the given term, not <code>null</code>
	 */
	public List<Integer>
	search(User user, String term, Project project, Integer limit, Integer offset, 
			SortDataSetBy sortBy, boolean descending);
	
	/**
	 * Count the number of datasets in underlying storage matching the given 
	 * search term/project which are available to the given user.
	 * 
	 * @param user user, not <code>null</code>
	 * @param term term for which to search, may be <code>null</code>
	 * @param project project within which to search, not <code>null</code>
	 * @return number of datasets, &ge; 0
	 */
	public int
	count(User user, String term, Project project);
	
}
