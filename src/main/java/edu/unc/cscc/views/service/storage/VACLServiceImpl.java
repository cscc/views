package edu.unc.cscc.views.service.storage;

import java.util.Collection;
import java.util.stream.Collectors;

import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.InsertValuesStep4;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.domain.GrantedAuthoritySid;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.unc.cscc.views.model.security.ChartOID;
import edu.unc.cscc.views.model.security.DashboardOID;
import edu.unc.cscc.views.model.security.DataSetOID;
import edu.unc.cscc.views.model.security.ProjectOID;
import edu.unc.cscc.views.model.security.User;
import edu.unc.cscc.views.model.security.VACL;
import edu.unc.cscc.views.model.security.VACLEntry;
import edu.unc.cscc.views.model.security.VPermission;
import edu.unc.cscc.views.model.security.VSid;

import static org.jooq.impl.DSL.*;

@Service
public final class VACLServiceImpl
implements VACLService
{
	
	private static final Table<?>	CHART_ACL_TABLE = DSL.table(name("chart_acl"));
	private static final Table<?>	DASHBOARD_ACL_TABLE = DSL.table(name("dashboard_acl"));
	private static final Table<?>	DS_ACL_TABLE = DSL.table(name("dataset_acl"));
	private static final Table<?>	PROJECT_ACL_TABLE = DSL.table(name("project_acl"));
	
	private final DSLContext		dsl;
	
	@Autowired
	public VACLServiceImpl(DSLContext dsl)
	{
		this.dsl = dsl;
	}

	@Override
	@Transactional
	public VACL 
	save(VACL acl)
	{
		final Collection<Integer> userIDs = 
				acl.represents()
					.stream()
					.filter(s -> s instanceof VSid)
					.map(s -> ((VSid) s).userID())
					.collect(Collectors.toList());
		
		final Collection<String> roles = 
				acl.represents()
					.stream()
					.filter(s -> s instanceof GrantedAuthoritySid)
					.map(s -> ((GrantedAuthoritySid) s).getGrantedAuthority())
					.collect(Collectors.toSet());
					
		
		Table<?> t;
		Condition idCondition;
		
		if (acl.objectIdentity() instanceof ChartOID)
		{
			t = CHART_ACL_TABLE;
			idCondition = DSL.field(name("chart")).eq(acl.objectIdentity().getIdentifier());
		}
		else if (acl.objectIdentity() instanceof ProjectOID)
		{
			t = PROJECT_ACL_TABLE;
			idCondition = DSL.field(name("project")).eq(acl.objectIdentity().getIdentifier());
		}
		else if (acl.objectIdentity() instanceof DashboardOID)
		{
			t = DASHBOARD_ACL_TABLE;
			idCondition = DSL.field(name("dashboard")).eq(acl.objectIdentity().getIdentifier());
		}
		else if (acl.objectIdentity() instanceof DataSetOID)
		{
			t = DS_ACL_TABLE;
			idCondition = DSL.field(name("dataset")).eq(acl.objectIdentity().getIdentifier());
		}
		else
		{
			throw new IllegalArgumentException("Unknown OID type");
		}
		
		final Condition userCondition = 
				DSL.or(DSL.field(name("id__users"), Integer.class).in(userIDs),
						DSL.field(name("role"), String.class).in(roles));
				
		
		
		this.dsl
			.deleteFrom(t)
			.where(idCondition)
			/* remove by user/role if partial, otherwise nuke it all */
			.and(acl.isPartial() ? userCondition : DSL.trueCondition())
			.execute();
		
		
		if (acl.objectIdentity() instanceof ChartOID)
		{
			ChartOID oid = (ChartOID) acl.objectIdentity();
			InsertValuesStep4<?, Integer, Integer, String, String> is = 
					this.dsl
					.insertInto(t)
					.columns(DSL.field(name("chart"), Integer.class),
							DSL.field(name("id__users"), Integer.class),
							DSL.field(name("role"), String.class),
							DSL.field(name("permission"), String.class));
			
			acl.entries()
				.stream()
				.filter(e -> e.isGrant())
				.forEach(e -> 
					is.values(oid.getIdentifier(), 
								(e.sid() instanceof VSid)
									? ((VSid) e.sid()).userID()
									: null, 
								(e.sid() instanceof GrantedAuthoritySid)
									? ((GrantedAuthoritySid) e.sid()).getGrantedAuthority()
									: null, 
								e.permission().identifier()));
			
			is.execute();
			
		}
		else if (acl.objectIdentity() instanceof ProjectOID)
		{
			ProjectOID oid = (ProjectOID) acl.objectIdentity();
			InsertValuesStep4<?, Integer, Integer, String, String> is = 
					this.dsl
					.insertInto(t)
					.columns(DSL.field(name("project"), Integer.class),
							DSL.field(name("id__users"), Integer.class),
							DSL.field(name("role"), String.class),
							DSL.field(name("permission"), String.class));
			
			acl.entries()
				.forEach(e -> 
					is.values(oid.getIdentifier(), 
								(e.sid() instanceof VSid)
									? ((VSid) e.sid()).userID()
									: null, 
								(e.sid() instanceof GrantedAuthoritySid)
									? ((GrantedAuthoritySid) e.sid()).getGrantedAuthority()
									: null, 
								e.permission().identifier()));
			
			is.execute();
		}
		else if (acl.objectIdentity() instanceof DashboardOID)
		{
			DashboardOID oid = (DashboardOID) acl.objectIdentity();
			InsertValuesStep4<?, Integer, Integer, String, String> is = 
					this.dsl
					.insertInto(t)
					.columns(DSL.field(name("dashboard"), Integer.class),
							DSL.field(name("id__users"), Integer.class),
							DSL.field(name("role"), String.class),
							DSL.field(name("permission"), String.class));
			
			acl.entries()
				.forEach(e -> 
					is.values(oid.getIdentifier(), 
								(e.sid() instanceof VSid)
									? ((VSid) e.sid()).userID()
									: null, 
								(e.sid() instanceof GrantedAuthoritySid)
									? ((GrantedAuthoritySid) e.sid()).getGrantedAuthority()
									: null, 
								e.permission().identifier()));
			
			is.execute();
		}
		else
		{
			DataSetOID oid = (DataSetOID) acl.objectIdentity();
			InsertValuesStep4<?, Integer, Integer, String, String> is = 
					this.dsl
					.insertInto(t)
					.columns(DSL.field(name("dataset"), Integer.class),
							DSL.field(name("id__users"), Integer.class),
							DSL.field(name("role"), String.class),
							DSL.field(name("permission"), String.class));
			
			acl.entries()
				.forEach(e -> 
					is.values(oid.getIdentifier(), 
								(e.sid() instanceof VSid)
									? ((VSid) e.sid()).userID()
									: null, 
								(e.sid() instanceof GrantedAuthoritySid)
									? ((GrantedAuthoritySid) e.sid()).getGrantedAuthority()
									: null, 
								e.permission().identifier()));
			
			is.execute();
		}
			
		return acl;
	}

	@Override
	public VACL 
	load(ChartOID chartOID)
	{
		return this.load(chartOID, (User) null);
	}

	@Override
	public VACL 
	load(DataSetOID datasetOID)
	{
		return this.load(datasetOID, (User) null);
	}
	
	@Override
	public VACL 
	load(DashboardOID dashboardOID)
	{
		return this.load(dashboardOID, (User) null);
	}
	
	

	@Override
	public VACL load(ProjectOID projectOID)
	{
		return this.load(projectOID, (User) null);
	}

	@Override
	public VACL 
	load(ChartOID chartOID, Collection<GrantedAuthority> authorities)
	{
		Collection<VACLEntry> entries = 
				this.dsl
					.selectFrom(CHART_ACL_TABLE)
					.where(DSL.field(name("chart"), Integer.class).eq(chartOID.getIdentifier()))
					.and(DSL.field(name("roles"), String.class)
							.in(authorities.stream()
											.map(a -> a.getAuthority())
											.collect(Collectors.toSet())))
					.fetch(r -> {
						
						final String role = r.get(name("role"), String.class);
						
						if (role == null)
						{
							return new VACLEntry(new VSid(r.get(name("id__users"), Integer.class)), 
											VPermission.parse(r.get(name("permission"), String.class)));
						}
						
						return new VACLEntry(new GrantedAuthoritySid(role), 
								VPermission.parse(r.get(name("permission"), String.class)));
					});
		
		return new VACL(chartOID, entries, true);
	}

	@Override
	public VACL 
	load(DataSetOID datasetOID, Collection<GrantedAuthority> authorities)
	{
		Collection<VACLEntry> entries = 
				this.dsl
					.selectFrom(DS_ACL_TABLE)
					.where(DSL.field(name("dataset"), Integer.class).eq(datasetOID.getIdentifier()))
					.and(DSL.field(name("roles"), String.class)
							.in(authorities.stream()
											.map(a -> a.getAuthority())
											.collect(Collectors.toSet())))
					.fetch(r -> {
						
						final String role = r.get(name("role"), String.class);
						
						if (role == null)
						{
							return new VACLEntry(new VSid(r.get(name("id__users"), Integer.class)), 
									VPermission.parse(r.get(name("permission"), String.class)));
						}
						
						return new VACLEntry(new GrantedAuthoritySid(role), 
								VPermission.parse(r.get(name("permission"), String.class)));
					});
		
		return new VACL(datasetOID, entries, true);
	}
	
	@Override
	public VACL 
	load(DashboardOID dashboardOID, Collection<GrantedAuthority> authorities)
	{
		Collection<VACLEntry> entries = 
				this.dsl
					.selectFrom(DASHBOARD_ACL_TABLE)
					.where(DSL.field(name("dashboard"), Integer.class).eq(dashboardOID.getIdentifier()))
					.and(DSL.field(name("roles"), String.class)
							.in(authorities.stream()
											.map(a -> a.getAuthority())
											.collect(Collectors.toSet())))
					.fetch(r -> {
						
						final String role = r.get(name("role"), String.class);
						
						if (role == null)
						{
							return new VACLEntry(new VSid(r.get(name("id__users"), Integer.class)), 
									VPermission.parse(r.get(name("permission"), String.class)));
						}
						
						return new VACLEntry(new GrantedAuthoritySid(role), 
								VPermission.parse(r.get(name("permission"), String.class)));
					});
		
		return new VACL(dashboardOID, entries, true);
	}
	
	@Override
	public VACL load(ProjectOID projectOID, Collection<GrantedAuthority> authorities)
	{
		Collection<VACLEntry> entries = 
				this.dsl
					.selectFrom(PROJECT_ACL_TABLE)
					.where(DSL.field(name("project"), Integer.class).eq(projectOID.getIdentifier()))
					.and(DSL.field(name("roles"), String.class)
							.in(authorities.stream()
											.map(a -> a.getAuthority())
											.collect(Collectors.toSet())))
					.fetch(r -> {
						
						final String role = r.get(name("role"), String.class);
						
						if (role == null)
						{
							return new VACLEntry(new VSid(r.get(name("id__users"), Integer.class)), 
									VPermission.parse(r.get(name("permission"), String.class)));
						}
						
						return new VACLEntry(new GrantedAuthoritySid(role), 
								VPermission.parse(r.get(name("permission"), String.class)));
					});
		
		return new VACL(projectOID, entries, true);
	}
	

	@Override
	public VACL load(ProjectOID projectOID, User user)
	{
		Condition userCondition = null;
		
		if (user == null)
		{
			userCondition = DSL.trueCondition();
		}
		else
		{
			userCondition = 
					DSL.or(
						DSL.field(name("role"))
							.in(user.getAuthorities()
									.stream()
									.map(a -> a.getAuthority())
									.collect(Collectors.toSet())),
						DSL.field(name("id__users"), Integer.class).eq(user.id()));
		}
		
		Collection<VACLEntry> entries = 
				this.dsl
					.selectFrom(PROJECT_ACL_TABLE)
					.where(DSL.field(name("project"), Integer.class).eq(projectOID.getIdentifier()))
					.and(userCondition)
					.fetch(r -> {
						
						final String role = r.get(name("role"), String.class);
						
						if (role == null)
						{
							return new VACLEntry(new VSid(r.get(name("id__users"), Integer.class)), 
									VPermission.parse(r.get(name("permission"), String.class)));
						}
						
						return new VACLEntry(new GrantedAuthoritySid(role), 
								VPermission.parse(r.get(name("permission"), String.class)));
					});
		
		return new VACL(projectOID, entries, user != null);
	}

	

	@Override
	public VACL
	load(ChartOID chartOID, User user)
	{
		
		Condition userCondition = null;
		
		if (user == null)
		{
			userCondition = DSL.trueCondition();
		}
		else
		{
			userCondition = 
					DSL.or(
						DSL.field(name("role"))
							.in(user.getAuthorities()
									.stream()
									.map(a -> a.getAuthority())
									.collect(Collectors.toSet())),
						DSL.field(name("id__users"), Integer.class).eq(user.id()));
		}
		
		Collection<VACLEntry> entries = 
				this.dsl
					.selectFrom(CHART_ACL_TABLE)
					.where(DSL.field(name("chart"), Integer.class).eq(chartOID.getIdentifier()))
					.and(userCondition)
					.fetch(r -> {
						
						final String role = r.get(name("role"), String.class);
						
						if (role == null)
						{
							return new VACLEntry(new VSid(r.get(name("id__users"), Integer.class)), 
									VPermission.parse(r.get(name("permission"), String.class)));
						}
						
						return new VACLEntry(new GrantedAuthoritySid(role), 
								VPermission.parse(r.get(name("permission"), String.class)));
					});
		
		return new VACL(chartOID, entries, user != null);
	}

	@Override
	public VACL
	load(DataSetOID datasetOID, User user)
	{
		Condition userCondition = null;
				
		if (user == null)
		{
			userCondition = DSL.trueCondition();
		}
		else
		{
			userCondition = 
					DSL.or(
						DSL.field(name("role"))
							.in(user.getAuthorities()
									.stream()
									.map(a -> a.getAuthority())
									.collect(Collectors.toSet())),
						DSL.field(name("id__users"), Integer.class).eq(user.id()));
		}
		
		Collection<VACLEntry> entries = 
				this.dsl
					.selectFrom(DS_ACL_TABLE)
					.where(DSL.field(name("dataset"), Integer.class).eq(datasetOID.getIdentifier()))
					.and(userCondition)
					.fetch(r -> {
						
						final String role = r.get(name("role"), String.class);
						
						if (role == null)
						{
							return new VACLEntry(new VSid(r.get(name("id__users"), Integer.class)), 
												VPermission.parse(r.get(name("permission"), String.class)));
						}
						
						return new VACLEntry(new GrantedAuthoritySid(role), 
								VPermission.parse(r.get(name("permission"), String.class)));
					});
		
		return new VACL(datasetOID, entries, user != null);
	}
	
	
	@Override
	public VACL
	load(DashboardOID dashboardOID, User user)
	{
		Condition userCondition = null;
				
		if (user == null)
		{
			userCondition = DSL.trueCondition();
		}
		else
		{
			userCondition = 
					DSL.or(
						DSL.field(name("role"))
							.in(user.getAuthorities()
									.stream()
									.map(a -> a.getAuthority())
									.collect(Collectors.toSet())),
						DSL.field(name("id__users"), Integer.class).eq(user.id()));
		}
		
		Collection<VACLEntry> entries = 
				this.dsl
					.selectFrom(DASHBOARD_ACL_TABLE)
					.where(DSL.field(name("dashboard"), Integer.class).eq(dashboardOID.getIdentifier()))
					.and(userCondition)
					.fetch(r -> {
						
						final String role = r.get(name("role"), String.class);
						
						if (role == null)
						{
							return new VACLEntry(new VSid(r.get(name("id__users"), Integer.class)), 
												VPermission.parse(r.get(name("permission"), String.class)));
						}
						
						return new VACLEntry(new GrantedAuthoritySid(role), 
								VPermission.parse(r.get(name("permission"), String.class)));
					});
		
		return new VACL(dashboardOID, entries, user != null);
	}

}
