package edu.unc.cscc.views.service.storage;

import edu.unc.cscc.views.model.security.LocalCredentials;
import edu.unc.cscc.views.model.security.User;

public interface LocalCredentialStorageService
{

	public LocalCredentials create(User user, String password);

	public LocalCredentials save(LocalCredentials credentials);
	
	public LocalCredentials load(User user);
	
	public void delete(LocalCredentials credentials);
	
}
