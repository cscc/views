package edu.unc.cscc.views.service.storage;

import java.util.Collection;
import java.util.Map;

public interface StorageService<T>
{
	/**
	 * Determine whether an entity of the type handled by this service exists 
	 * within the underlying storage.
	 * 
	 * @param id ID of entity, &ge; 0
	 * @return <code>true</code> if the specified entity exists, 
	 * <code>false</code> otherwise
	 */
	public boolean 
	exists(int id);

	/**
	 * Load the entity with the given ID.
	 * 
	 * @param id ID of entity to load, &ge; 0
	 * @return entity, if it exists, <code>null</code> if no such entity
	 * with the given ID exists
	 */
	public T 
	load(int id);
	
	public Map<Integer, T>
	load(Collection<Integer> ids);
	
	/**
	 * Delete the given entity (if it exists) from the underlying storage.
	 * 
	 * @param id ID of entity to delete, &ge; 0
	 */
	public void 
	delete(int id);
	
	
	/**
	 * Persist the given entity to the underlying storage. I
	 * 
	 * @param entity entity to persist, not <code>null</code>
	 * @return copy of the saved entity, reflecting underlying storage
	 */
	public T 
	save(T entity);
	
}
