package edu.unc.cscc.views.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.mail.MessagingException;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.auth0.client.auth.AuthAPI;
import com.auth0.client.mgmt.ManagementAPI;
import com.auth0.exception.APIException;
import com.auth0.exception.Auth0Exception;
import com.auth0.json.auth.UserInfo;
import com.auth0.json.mgmt.users.Identity;
import com.auth0.json.mgmt.users.User;
import com.auth0.net.Request;

import edu.unc.cscc.views.exception.UserExistsException;

@Service
public class Auth0Service {

    @Value("${auth.defaultConnection}")
    private String connection;

    @Value("${auth.frontendUri}")
	  private String frontendUri;

	  @Value("${auth.loginRedirect}")
	  private String loginRedirectPath;
    
    @Autowired
    private AuthAPI authApi;

    @Autowired
    private ManagementAPI mgmt;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
    // Pulled from https://www.baeldung.com/java-generate-secure-password
    private char[] generateRandomPassword() {
        String upperCaseLetters = RandomStringUtils.random(2, 65, 90, true, true);
        String lowerCaseLetters = RandomStringUtils.random(2, 97, 122, true, true);
        String numbers = RandomStringUtils.randomNumeric(3);
        String specialChar = RandomStringUtils.random(2, 33, 47, false, false);
        String totalChars = RandomStringUtils.randomAlphanumeric(10);
        String combinedChars = upperCaseLetters.concat(lowerCaseLetters)
          .concat(numbers)
          .concat(specialChar)
          .concat(totalChars);
        List<Character> pwdChars = combinedChars.chars()
          .mapToObj(c -> (char) c)
          .collect(Collectors.toList());
        Collections.shuffle(pwdChars);
        String password = pwdChars.stream()
          .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
          .toString();
        logger.debug("New user passwor ----> MMM: " + password + " :MMM");
        return password.toCharArray();
    }

    public Optional<String> getUserIdForConnection(User user, String connectionName) {
      for (Identity identity : user.getIdentities()) {
        if (identity.getConnection().equals(connectionName)) {
          return Optional.of(identity.getProvider() + "|" + identity.getUserId());
        }
      } 
      return Optional.ofNullable(null);
    }

  public void createNewUser(edu.unc.cscc.views.model.security.User user) throws Auth0Exception, MessagingException, UserExistsException {
    com.auth0.json.mgmt.users.User auth0User = new User(connection);
    auth0User.setEmail(user.getUsername());
    auth0User.setBlocked(false);
    auth0User.setEmailVerified(true);
    auth0User.setPassword(generateRandomPassword());
    Request<User> request = mgmt.users().create(auth0User);
    try {
      request.execute();
      authApi.resetPassword(user.getUsername(), connection).execute();
    } catch (APIException ex) {
      if (ex.getStatusCode() == 409) {
        throw new UserExistsException("User " + user.getUsername() + " already exists in Auth0", ex);
      }
      throw ex;
    }
  }
  
  public String getUserEmail(String accessToken) throws Auth0Exception {
    UserInfo userInfo = authApi.userInfo(accessToken).execute();
    return (String) userInfo.getValues().get("email");
  }

  public boolean userWithEmailExists(String email) throws Auth0Exception {
    List<User> users = mgmt.users().listByEmail(email, null).execute();
    return users.size() > 0;
  }

}
