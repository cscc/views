package edu.unc.cscc.views.service.storage;

import java.util.List;

import edu.unc.cscc.views.model.Dashboard;
import edu.unc.cscc.views.model.Project;
import edu.unc.cscc.views.model.security.User;

public interface DashboardStorageService
extends StorageService<Dashboard>
{
	
	public List<Integer>
	search(Project project, String query, User user, int limit, int offset);
	
	/**
	 * Count the number of dashboards in the given project matching the given
	 * query string.
	 * 
	 * @param project project in which to search, not <code>null</code>
	 * @param query search term, may be <code>null</code>
	 * @return count of results
	 */
	public int
	count(Project project, String query);

	public Dashboard loadByName(String name, Project project);
}
