package edu.unc.cscc.views.service.storage;

import java.util.List;

import edu.unc.cscc.views.model.Project;
import edu.unc.cscc.views.model.security.User;

public interface ProjectStorageService
extends StorageService<Project>
{
	/**
	 * Load the project with the given name.
	 * 
	 * @param name project name, not <code>null</code>
	 * @return project with the given name, or <code>null</code> if no such
	 * project exists
	 */
	public Project loadByName(String name);
	
	/**
	 * Search for projects whose name or description contains/is similar to 
	 * the given query; alternatively list all accessible projects if no 
	 * query is provided.
	 * 
	 * @param query query for which to search for projects or <code>null</code>
	 * to list all visible projects
	 * @param user user for whom search is being performed, not <code>null</code>
	 * @param limit maximum number of results
	 * @param offset offset of results
	 * @return IDs of projects, ordered by {@link Project#name() name}
	 */
	public List<Integer>
	search(String query, User user, int limit, int offset);
	
	/**
	 * Count the number of known projects whose name or description 
	 * contains/is similar to the given query.
	 * 
	 * @param query query for which to search for projects
	 * @param user user for whom search is being performed, not <code>null</code>
	 * @returns count of projects, &ge; 0
	 */
	public int
	count(String query, User user);

	/**
	 * Determine whether a project with the given name exists.
	 * 
	 * @param name project name, not <code>null</code>
	 * @return <code>true</code> if a project with the given name exists, 
	 * <code>false</code> otherwise
	 */
	public boolean exists(String name);
	
}
