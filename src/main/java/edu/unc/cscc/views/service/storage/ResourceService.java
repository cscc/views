package edu.unc.cscc.views.service.storage;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;

import edu.unc.cscc.views.model.Chart;
import edu.unc.cscc.views.model.ChartResource;

public interface ResourceService 
extends StorageService<ChartResource>
{
	
	public Collection<Integer>
	find(Chart chart);
	
	public boolean
	exists(String path, int chartID);
	

	public ChartResource
	loadShallow(String path, int chartID);
	

	public void
	loadData(int resourceID, OutputStream os) 
		throws IOException;
}
