package edu.unc.cscc.views.service.storage;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.jooq.DSLContext;
import org.jooq.InsertValuesStep3;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import edu.unc.cscc.views.model.Chart;
import edu.unc.cscc.views.model.Dashboard;
import edu.unc.cscc.views.model.Project;
import edu.unc.cscc.views.model.security.User;

@Service
public class DashboardStorageServiceImpl
implements DashboardStorageService
{

	private static final Table<?>		TABLE = DSL.table(name("dashboards"));
	private static final Table<?>		LINK_TABLE = DSL.table(name("dashboards_charts"));
	
	private final DSLContext			dsl;
	
	private final ChartStorageService	chartService;
	private final ProjectStorageService	projectService;
	
	@Autowired
	public DashboardStorageServiceImpl(
			DSLContext dsl, ChartStorageService chartService, 
			ProjectStorageService projectService)
	{
		this.dsl = dsl;
		this.chartService = chartService;
		this.projectService = projectService;
	}
	
	@Override
	public boolean 
	exists(int id)
	{
		return this.dsl
					.fetchExists(
						this.dsl
							.selectOne()
							.from(TABLE)
							.where(field(name("id")).eq(id)));
	}
	
	@Override
	public Dashboard 
	load(int id)
	{
		return this.load(Collections.singleton(id)).get(id);
	}
	
	@Override
	public Dashboard
	loadByName(String name, Project project)
	{
		Assert.notNull(project, "cannot load for null project");
		Assert.notNull(project.id(), "project had null ID");
		Integer id = 
				this.dsl
					.select(field(name("id"), Integer.class))
					.from(TABLE)
					.where(field(name("name")).eq(name))
					.and(field(name("id__projects")).eq(project.id()))
					.fetchOptional(field(name("id"), Integer.class))
					.orElse(null);
		if (id == null)
		{
			return null;
		}
		
		return this.load(id);
	}
	

	@Override
	public Map<Integer, Dashboard> 
	load(Collection<Integer> ids)
	{
		
		if (ids.isEmpty())
		{
			return Collections.emptyMap();
		}
		
		Map<Integer, List<Integer>> chartSets = 
				this.dsl
					.select(field(name("id__charts")), field(name("id__dashboards")))
					.from(LINK_TABLE)
					.where(field(name("id__dashboards")).in(ids))
					.orderBy(field(name("position")).asc())
					.fetchGroups(field(name("id__dashboards"), Integer.class),
								field(name("id__charts"), Integer.class));
		
		Map<Integer, Chart> charts = 
				this.chartService
					.load(chartSets.values()
									.stream()
									.flatMap(l -> l.stream())
									.collect(Collectors.toSet()));
		
		Set<Integer> projectIDs = 
				this.dsl.select(field(name("id__projects"), Integer.class))
						.from(TABLE)
						.where(field(name("id")).in(ids))
						.fetch(field(name("id__projects"), Integer.class))
						.stream().collect(Collectors.toCollection(HashSet :: new));
		
		Map<Integer, Project> projects = 
				this.projectService.load(projectIDs);
		
		Assert.isTrue(projects.size() == projectIDs.size(), 
						"found nonsensical number of projects");
		
		return this.dsl
					.selectFrom(TABLE)
					.where(field(name("id")).in(ids))
					.fetchMap(field(name("id"), Integer.class),
								r -> {
									
									final int id = r.get("id", Integer.class);
									
									return new Dashboard(id, 
											r.get("name", String.class), 
											r.get("description", String.class), 
											projects.get(r.get("id__projects", Integer.class)),
											(chartSets.containsKey(id)
												? chartSets.get(id)
														.stream()
														.map(charts :: get)
														.collect(Collectors.toList())
												: Collections.emptyList()));
									
								});

	}

	@Transactional
	@Override
	public Dashboard 
	save(Dashboard dashboard)
	{
		Assert.notNull(dashboard, "cannot save null dashboard");
		
		if (dashboard.charts().stream().anyMatch(c -> c.id() == null))
		{
			throw new IllegalArgumentException(
					"Cannot save dashboard which refers to non-existant charts");
		}
		
		if (dashboard.id() != null)
		{
			/* nuke associations */
			this.dsl.deleteFrom(LINK_TABLE)
					.where(field(name("id__dashboards")).eq(dashboard.id()))
					.execute();
			
			this.dsl
				.update(TABLE)
				.set(field(name("name")), dashboard.name())
				.set(field(name("description")), dashboard.description())
				.where(field(name("id")).eq(dashboard.id()))
				.execute();
		}
		else
		{
			Integer id = 
					this.dsl
						.insertInto(TABLE)
						.set(field(name("name")), dashboard.name())
						.set(field(name("description")), dashboard.description())
						.set(field(name("id__projects")), dashboard.project().id())
						.returning(field(name("id"), Integer.class))
						.fetchOne().get(field(name("id"), Integer.class));
			
			dashboard = dashboard.id(id);		
		}
		
		if (dashboard.charts().isEmpty())
		{
			return dashboard;
		}
		
		/* associations */
		InsertValuesStep3<?, Integer, Integer, Integer> ins =
				this.dsl
					.insertInto(LINK_TABLE)
					.columns(field(name("id__dashboards"), Integer.class),
							field(name("id__charts"), Integer.class),
							field(name("position"), Integer.class));
		
		int i = 0;
		for (Chart chart : dashboard.charts())
		{
			ins = ins.values(dashboard.id(), chart.id(), i++);
		}
		
		ins.execute();
		
		return dashboard;
	}

	@Transactional
	@Override
	public void 
	delete(int id)
	{
		Assert.isTrue(id >= 0, "id was out of range");
		this.dsl.deleteFrom(TABLE).where(field(name("id")).eq(id)).execute();
	}

	/* not secured since we don't want to load the entire DB entity each time */
	@Override
	public List<Integer> 
	search(Project project, String query, User user, int limit, int offset)
	{
		return this.dsl
					.select(field(name("id")))
					.from(TABLE)
					.where(DSL.lower(field(name("name"), String.class))
								.contains(query.toLowerCase())
							.or(DSL.lower(field(name("description"), String.class))
										.contains(query.toLowerCase())))
					.and(field(name("id__projects")).eq(project.id()))
					.limit(limit).offset(offset)
					.fetch(field(name("id"), Integer.class));
	}

	@Override
	public int
	count(Project project, String query)
	{
		return this.dsl.fetchCount(
						this.dsl
							.select(field(name("id")))
							.from(TABLE)
							.where(DSL.lower(field(name("name"), String.class))
										.contains(query.toLowerCase())
									.or(DSL.lower(field(name("description"), String.class))
												.contains(query.toLowerCase())))
							.and(field(name("id__projects")).eq(project.id())));
	}

	

}
