package edu.unc.cscc.views.service.storage;

import java.util.Collection;
import java.util.List;

import edu.unc.cscc.views.model.Chart;
import edu.unc.cscc.views.model.DataSet;
import edu.unc.cscc.views.model.Project;
import edu.unc.cscc.views.model.security.User;

public interface ChartStorageService
extends StorageService<Chart>
{
	
	/**
	 * Find the charts that the given user is allowed to access, matching
	 * the given search string.
	 * 
	 * @param user user
	 * @param search search string for chart name/description, <code>null</code>
	 * to list all charts
	 * @param project project project within which to search for a matching 
	 * chart, not <code>null</code>
	 * @param limit maximum number of results, may be <code>null</code> if 
	 * service-determined default should be used
	 * @param offset starting offset of search results, may be <code>null</code>
	 * if returned results should start from the first found chart
	 * @return IDs of charts that the user is allowed to access
	 */
	public List<Integer> 
	search(User user, String search, Project project, Integer limit, Integer offset);
	
	/**
	 * Count the charts that the given user is allowed to access, matching
	 * the given search string.
	 * 
	 * @param user user
	 * @param search search string for chart name/description, <code>null</code>
	 * to list all charts
	 * @param project project project within which to search for a matching 
	 * chart, not <code>null</code>
	 * @returns number of matching charts, &ge; 0
	 */
	public int
	count(User user, String search, Project project);
	
	/**
	 * Determine whether a chart with the given name exists within the given 
	 * project.  Does not consider security privileges when determining existence.
	 * 
	 * @param name name of chart to check for existence, not <code>null</code>
	 * @param project project in which to check for chart, not <code>null</code>
	 * @return <code>true</code> if such a chart exists, <code>false</code> 
	 * otherwise
	 */
	public boolean
	exists(String name, Project project);
	
	/**
	 * Find the ID of the chart with the given name within the given project.  
	 * 
	 * @param name name of chart to load
	 * @param project project from which to load chart with the given name
	 * @return ID chart with the given name, or <code>null</code> if no such chart
	 * found
	 */
	public Integer findID(String name, Project project);
	
	/**
	 * Find the IDs of the charts which depend on the {@link DataSet data set} 
	 * with the given ID.
	 * 
	 * @param dataSetID ID of {@link DataSet data set} for which to find charts
	 * @return IDs of charts which depend on the specified data set, not 
	 * <code>null</code>
	 */
	public Collection<Integer> findByDataSet(int dataSetID);
	
}
