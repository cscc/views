package edu.unc.cscc.views.service.storage;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import edu.unc.cscc.views.exception.UserExistsException;
import edu.unc.cscc.views.model.security.User;
import edu.unc.cscc.views.model.security.User.ViewsRole;

@Service
public final class UserStorageImpl 
implements UserStorage 
{

	private static final Table<?> TABLE = DSL.table(name("users"));
	
	private final DSLContext dsl;
	
	@Autowired
	public UserStorageImpl(DSLContext dsl) 
	{
		this.dsl = dsl;
	}
	
	@Override
	public boolean 
	exists(int id) 
	{
		
		if (id < 0)
		{
			return false;
		}
		
		return this.dsl.fetchExists(
				this.dsl.selectOne()
						.from(TABLE)
						.where(field(name("id")).eq(id)));
	}
	
	
	@Override
	public List<Integer>
	search(User user, String term, Integer limit, Integer offset)
	{

		final int o = (offset == null) ? 0 : offset;
		final int l = (limit == null) ? 10 : limit;
		
		Assert.isTrue(o >= 0, "offset OOB");
		Assert.isTrue(l > 0, "length OOB");
		
		Condition searchClause;
		
		if (term == null || term.trim().isEmpty())
		{
			searchClause = DSL.trueCondition();
		}
		else
		{
			searchClause = 
					DSL.lower(field(name("username"), String.class))
						.contains(term.trim().toLowerCase())
						.or(DSL.lower(field(name("name"), String.class))
								.contains(term.trim().toLowerCase()));
		}
		
		return this.dsl.select(field(name("id")))
						.from(TABLE)
						.where(searchClause)
						.orderBy(field(name("username")))
						.limit(l)
						.offset(o)
						.fetch(field(name("id"), Integer.class));
		
		
	}
	
	

	@Override
	public int 
	count(User user, String term)
	{
		Condition searchClause;

		if (term == null || term.trim().isEmpty())
		{
			searchClause = DSL.trueCondition();
		}
		else
		{
			searchClause = 
					DSL.lower(field(name("username"), String.class))
						.contains(term.trim().toLowerCase())
						.or(DSL.lower(field(name("name"), String.class))
								.contains(term.trim().toLowerCase()));
		}
		
		return this.dsl.fetchCount(
					this.dsl.select(field(name("id")))
							.from(TABLE)
							.where(searchClause));
	}

	@Override
	public User 
	load(int id) 
	{
		return this.load(Collections.singleton(id)).get(id);
	}

	@Override
	public Map<Integer, User> 
	load(Collection<Integer> ids)
	{
		Field<String[]> rolesField = field(name("roles"), String[].class);
		Field<Integer> idField = field(name("id"), Integer.class);
		return this.dsl
				.select(idField, rolesField,
						field(name("username")), field(name("name")),
						field(name("credential_source")), field(name("modified")),
						field(name("active")))
				.from(TABLE)
				.where(idField.in(ids))
				.fetchMap(idField,
					r -> {
						/* the snippet below is what we *should* do.  Instead,
						 * we use the above explicit select of a col. list to
						 * work around jOOQ #6452/#6454 for HSQL.
						 */
						
						/*
						final Collection<GrantedAuthority> roles = 
								Stream.of(r.get(field(name("roles"), String[].class), String[].class))
										.map(SimpleGrantedAuthority :: new)
										.collect(Collectors.toList());
						*/
						
						final Collection<ViewsRole> roles = 
								Stream.of(r.get(rolesField))
									.map(ViewsRole :: parse)
									.collect(Collectors.toList());
											
						return new User(r.get(idField), 
										r.get(name("username"), String.class), 
										r.get(name("credential_source"), String.class),
										r.get(name("name"), String.class), 
										r.get(name("modified"), Timestamp.class).toInstant(),
										r.get(name("active"), Boolean.class), 
										roles);
					});
	}

	@Override
	@Transactional
	public void
	delete(int id) 
	{
		this.dsl.deleteFrom(TABLE)
				.where(field(name("id")).eq(id))
				.execute();
	}

	/* User must either be able to manage users or must be editing themselves
	 * AND must not be persisting a user with authorities that they do not 
	 * have.
	 */
	@PreAuthorize("hasAuthority('manage_users')"
				+ " or (principal != null "
				+ "		and #user.id() == principal.id()"
				+ "		and principal.getAuthorities().containsAll(#user.getAuthorities()))")
	@Override
	@Transactional
	public User 
	save(User user) 
	{		
		return this.saveInternal(user);
	}
	
	
	/* deliberately not secured */
	@Override
	public User 
	createNewUser(User user)
	{
		if (user.id() != null)
		{
			throw new IllegalArgumentException("User must not have ID");
		}
		
		if (this.exists(user.username(), user.getCredentialSourceID()))
		{
			throw new IllegalArgumentException("username/credential-source-id already exist");
		}
		
		return this.saveInternal(user);
	}

	private User
	saveInternal(User user)
	{
		Integer id = user.id();
		boolean exists = id != null && this.exists(id);
		
		if (! exists)
		{
			id = this.dsl.insertInto(TABLE)
							.set(field(name("username")), user.username())
							.set(field(name("credential_source")), user.getCredentialSourceID())
							.set(field(name("name")), user.name())
							.set(field(name("modified")), Timestamp.from(user.lastModified()))
							.set(field(name("active")), user.active())
							.set(field(name("roles"), String[].class), 
									user.roles()
										.stream()
										.map(g -> g.role())
										.toArray(l -> new String[l]))
						.returning(field(name("id"), Integer.class))
						.fetchOne().get(field(name("id"), Integer.class));
			
			
		}
		else
		{
			this.dsl.update(TABLE)
						.set(field(name("username")), user.username())
						.set(field(name("credential_source")), user.getCredentialSourceID())
						.set(field(name("name")), user.name())
						.set(field(name("modified")), Timestamp.from(user.lastModified()))
						.set(field(name("active")), user.active())
						.set(field(name("roles"), String[].class), 
								user.roles()
									.stream()
									.map(g -> g.role())
									.toArray(l -> new String[l]))
						.where(field(name("id")).eq(id))
						.execute();
		}
		
		return user.id(id);
	}
	
	@Override
	public boolean 
	exists(String username, String credentialSourceIdentifier)
	{
		return this.dsl.fetchExists(
				this.dsl
					.selectFrom(TABLE)
					.where(field(name("username")).eq(username))
					.and(field(name("credential_source"))
							.eq(credentialSourceIdentifier)));
	}

	@Override
	public User 
	find(String username, String credentialSourceIdentifier) 
	{
		Integer id = 
				this.dsl
					.select(field(name("id"), Integer.class))
					.from(TABLE)
					.where(field(name("username")).eq(username))
					.and(field(name("credential_source")).eq(credentialSourceIdentifier))
					.fetchOne(field(name("id"), Integer.class));
		
		if (id == null)
		{
			return null;
		}
		
		return this.load(id);
	}
	
	public User changeUserAuthSource(int userId, String username, String credentialSourceIdentifier) throws UserExistsException
	{
		if (this.exists(username, credentialSourceIdentifier)) {
			throw new UserExistsException(String.format("A user with username %s and credentialSourceIdentifier %s already exists", username, credentialSourceIdentifier));
		}
		User user = load(userId);
		User newUser = new User(userId, username, credentialSourceIdentifier, user.name(), Instant.now(), user.active(), user.roles());
		return this.saveInternal(newUser);
	}
	

}
