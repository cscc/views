package edu.unc.cscc.views.service.storage;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Table;
import org.jooq.UpdateSetMoreStep;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StreamUtils;

import edu.unc.cscc.views.model.DataSet;
import edu.unc.cscc.views.model.Project;
import edu.unc.cscc.views.model.security.User;
import edu.unc.cscc.views.model.security.VPermission;
import edu.unc.cscc.views.service.VSec;

@Service
public class DataSetStorageImpl 
implements DataSetStorage 
{
	static final Table<?>			DS_CHART_TABLE = DSL.table(name("charts_datasets"));
	
	private static final Table<?>	TABLE = DSL.table(name("datasets"));
	
	
	private final DSLContext				dsl;
	
	private final ProjectStorageService		projectService;
	
	private final VSec						sec;
	
	@Autowired
	public DataSetStorageImpl(DSLContext dsl, ProjectStorageService projectService,
								VSec sec) 
	{
		this.dsl = dsl;
		this.projectService = projectService;
		this.sec = sec;
	}
	
	
	@Override
	public boolean 
	exists(int id) 
	{
		return this.dsl
					.fetchExists(
							this.dsl
								.selectOne()
								.from(TABLE)
								.where(DSL.field(name("id")).eq(id)))
				&& this.sec.datasetAllowed(id, VPermission.READ);
	}
	
	

	/* intentionally not secured */
	@Override
	public boolean 
	exists(String identifier, Project project)
	{
		Assert.notNull(project, "project must be non-null");
		
		if (project.id() == null)
		{
			return false;
		}
		
		return this.dsl
					.fetchExists(this.dsl.selectOne()
									.from(TABLE)
									.where(DSL.field(name("identifier")).eq(identifier))
									.and(DSL.field(name("id__projects")).eq(project.id())));
	}


	@PreAuthorize("@vsec.datasetAllowed(#id, 'read')")
	@Override
	public DataSet 
	load(int id) 
	{
		List<Integer> l = new ArrayList<>();
		l.add(id);
		return this.load(l).get(id);
	}
	

	@PreFilter("@vsec.datasetAllowed(filterObject, 'read')")
	@Override
	public Map<Integer, DataSet> 
	load(Collection<Integer> ids)
	{
		return this.load(ids, false);
	}
	
	private Map<Integer, DataSet>
	load(Collection<Integer> ids, boolean shallow)
	{
		if (ids.isEmpty())
		{
			return Collections.emptyMap();
		}
		
		final List<Field<?>> fields = new ArrayList<>();
		
		fields.addAll(Arrays.asList(DSL.field(name("id")), DSL.field(name("id__projects")), 
							DSL.field(name("identifier")), DSL.field(name("description")), 
							DSL.field(name("content_type"))));
		
		if (! shallow)
		{
			fields.add(DSL.field(name("data")));
		}
		
		if (ids.isEmpty())
		{
			return Collections.emptyMap();
		}
		
		Map<Integer, Project> projects = 
				this.projectService
						.load(this.dsl
								.select(DSL.field(name("id__projects")))
								.from(TABLE)
								.where(DSL.field(name("id"), Integer.class).in(ids))
								.fetch(DSL.field(name("id__projects"), Integer.class)));
		
		
		
		return this.dsl
				.select(fields)
				.from(TABLE)
				.where(DSL.field(name("id")).in(ids))
				.fetchMap(DSL.field(name("id"), Integer.class), 
						r -> 
							new DataSet(r.get(name("id"), Integer.class), 
										projects.get(r.get(name("id__projects"), Integer.class)),
										r.get(name("identifier"), String.class),
										r.get(name("description"), String.class), 
										r.get(name("content_type"), String.class),
										(shallow ? null : r.get(name("data"), byte[].class))));
	}

	@PostAuthorize("returnObject == null "
					+ "or @vsec.datasetAllowed(returnObject.id(), 'read')")
	@Override
	public DataSet 
	loadByIdentifier(String identifier, Project project, boolean shallow)
	{
		Assert.notNull(project, "cannot load dataset for null project");
	
		if (project.id() == null)
		{
			return null;
		}
		
		final Integer id = 
				this.dsl
					.select(DSL.field(name("id"), Integer.class))
					.from(TABLE)
					.where(DSL.field(name("identifier")).eq(identifier))
					.and(DSL.field(name("id__projects")).eq(project.id()))
					.limit(1)
					.fetchOne(DSL.field(name("id"), Integer.class));
		
		if (id == null)
		{
			return null;
		}
		
		if (shallow)
		{
			return this.loadShallow(id);
		}
		else
		{
			return this.load(id);
		}
		
	}


	@PreAuthorize("@vsec.datasetAllowed(#id, 'read')")
	@Override
	public void 
	loadData(int id, OutputStream os) 
	throws IOException 
	{
		if (! this.exists(id))
		{
			return;
		}
		
		/* TODO - jOOQ doesn't support loading LOBs into outputstreams
		 * so we don't do anything special here for now... but we will!
		 */
		
		byte[] b = this.dsl
						.select(DSL.field(name("data"), byte[].class))
						.from(TABLE)
						.where(DSL.field(name("id")).eq(id))
						.fetchOne(DSL.field(name("data"), byte[].class));
		
		StreamUtils.copy(b, os);
	}

	@Override
	public DataSet 
	loadShallow(int id) 
	{
		List<Integer> l = new ArrayList<>();
		l.add(id);
		return this.loadShallow(l).get(id);
	}
	
	

	@Override
	public Map<Integer, DataSet> 
	loadShallow(Collection<Integer> ids)
	{
		return this.load(ids, true);
	}

	@Override
	public Collection<DataSet> 
	loadShallowByChart(int chartID)
	{
		return this.loadShallowByChart(Collections.singleton(chartID)).get(chartID);
	}


	@Override
	public Map<Integer, Collection<DataSet>> 
	loadShallowByChart(Collection<Integer> chartIDs)
	{
		/* yes, we could do this all in one query.  It'd also be more complex
		 * and there's no notable performance boost from doing it outside of
		 * pathological cases.  So we don't.
		 */
		
		Map<Integer, List<Integer>> m = 
				this.dsl
					.select(DSL.field(name("id__charts"), Integer.class), 
								DSL.field(name("id__datasets"), Integer.class))
					.from(DS_CHART_TABLE)
					.where(DSL.field(name("id__charts"), Integer.class).in(chartIDs))
					.fetchGroups(DSL.field(name("id__charts"), Integer.class), 
									DSL.field(name("id__datasets"), Integer.class));
		
		Set<Integer> dsIDs = 
				m.values()
					.stream()
					.flatMap(l -> l.stream())
					.collect(Collectors.toCollection(HashSet :: new));
		
		
		Map<Integer, DataSet> ds = this.loadShallow(dsIDs);
		
		Map<Integer, Collection<DataSet>> ret = new HashMap<>();
		
		m.forEach((chartID, d) -> {
			ret.put(chartID, 
					d.stream()
						.map(ds :: get)
						.collect(Collectors.toCollection(HashSet :: new)));
		});
		
		return ret;
	}


	@PreAuthorize("@vsec.datasetAllowed(#id, 'write')")
	@Override
	public void 
	delete(int id)
	{
		this.dsl.deleteFrom(TABLE)
				.where(DSL.field(name("id")).eq(id))
				.execute();
	}
	
	
	@PreAuthorize("@vsec.datasetAllowed(#id, 'write')")
	@Override
	public void 
	saveData(int id, InputStream data) 
	throws IOException
	{
		DataSet set = this.loadShallow(id);
		
		if (set == null)
		{
			return;
		}
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream(1024 * 512);
		
		
		
		IOUtils.copy(data, bos);
		
		
		set = set.data(bos.toByteArray());
		
		this.save(set);
	}
	
	@PreAuthorize("(@vsec.projectAllowed(#dataset.project().id(), 'create-datasets') and #dataset.id() == null)"
					+ " or "
					+ "@vsec.datasetAllowed(#dataset.id(), 'write')")
	@Override
	@Transactional
	public DataSet
	save(DataSet dataset) 
	{
		Assert.notNull(dataset, "cannot save null dataset");
		
		Assert.notNull(dataset.project(), "cannot save dataset for null project");
		
		if (dataset.project().id() == null)
		{
			throw new IllegalArgumentException(
					"Cannot save dataset associated with non-existent project");
		}
		
		if (dataset.id() == null) 
		{
			if (! dataset.hasData())
			{
				throw new IllegalArgumentException(
						"Cannot perform initial save of dataset without data");
			}
		
			final Integer id = 
					this.dsl.insertInto(TABLE)
							.set(field(name("id__projects")), dataset.project().id())
							.set(field(name("identifier")), dataset.identifier())
							.set(field(name("content_type")), dataset.contentType())
							.set(field(name("description")), dataset.description())
							.set(field(name("data")), dataset.data())
							.returning(field(name("id"), Integer.class))
							.fetchOne()
							.get(field(name("id"), Integer.class));
			Assert.notNull(id, "database failed to produce ID");
			
			return dataset.id(id);
			
		}
		else
		{
			UpdateSetMoreStep<?> s = 
					this.dsl
						.update(TABLE)
						.set(field(name("id__projects")), dataset.project().id())
						.set(field(name("identifier")), dataset.identifier())
						.set(field(name("content_type")), dataset.contentType())
						.set(field(name("description")), dataset.description());
			
			if (dataset.hasData())
			{
				s = s.set(field(name("data")), dataset.data());
			}
					
			s.where(field(name("id")).eq(dataset.id()))
				.execute();
		}
		
		return dataset;
	}


	/* FIXME - this is super inefficient -- we should figure out ACL stuff
	 * at the DB layer (which we can do easily).  This is a HACK for us to ship
	 * sooner rather than later.
	 */
	@Override
	public List<Integer> 
	search(User user, String term, Project project, Integer limit, Integer offset, 
			SortDataSetBy sortBy, final boolean descending)
	{
		
		Assert.notNull(project, "project must be non-null");
		
		if (project.id() == null)
		{
			return Collections.emptyList();
		}
		
		final int o = (offset == null) ? 0 : offset;
		final int l = (limit == null) ? 10 : limit;
		
		Assert.isTrue(o >= 0, "offset OOB");
		Assert.isTrue(l < 1000 && l > 0, "length OOB");
		
		Field<?> orderByField = null;
		
		switch (sortBy)
		{
			case CONTENT_TYPE:
				orderByField = DSL.field(name("content_type"));
				break;
			case DESCRIPTION:
				orderByField = DSL.field(name("description"));
				break;
			default:
			case IDENTIFIER: 
				orderByField = DSL.field(name("identifier"));
				break;
		}
		
		
		Condition c = DSL.trueCondition();
		
		if (term != null && ! term.trim().isEmpty())
		{
			c = DSL.lower(DSL.field(name("identifier"), String.class))
					.contains(term.trim().toLowerCase())
					.or(DSL.lower(DSL.field(name("description"), String.class))
							.contains(term.trim().toLowerCase()));
		}
		
		
		
		List<Integer> found = 
				this.dsl
					.select(DSL.field(name("id"), Integer.class))
						.from(TABLE)
					.where(c)
					.and(DSL.field(name("id__projects")).eq(project.id()))
					.orderBy(descending
								? orderByField.desc() 
								: orderByField.asc())
					.fetch(DSL.field(name("id"), Integer.class))
					.stream()
					.filter(id -> this.sec.datasetAllowed(id, VPermission.READ))
					.collect(Collectors.toCollection(ArrayList :: new));
		
		if (found.size() < o)
		{
			return Collections.emptyList();
		}
		
		final int bound = (found.size() < o + l) ? found.size() : o + l;
		
		return found.subList(o, bound);
	}
	
	/* FIXME - this is super inefficient -- we should figure out ACL stuff
	 * at the DB layer (which we can do easily).  This is a HACK for us to ship
	 * sooner rather than later.
	 */
	@Override
	public int
	count(User user, String term, Project project)
	{
		
		Assert.notNull(project, "cannot count for null project");
		
		if (project.id() == null)
		{
			return 0;
		}
		
		
		Condition c = DSL.trueCondition();
		
		if (term != null && ! term.trim().isEmpty())
		{
			c = DSL.lower(DSL.field(name("identifier"), String.class))
					.contains(term.trim().toLowerCase())
					.or(DSL.lower(DSL.field(name("description"), String.class))
							.contains(term.trim().toLowerCase()));
		}
		
		return (int) this.dsl
					.select(DSL.field(name("id"), Integer.class))
						.from(TABLE)
					.where(c)
					.and(DSL.field(name("id__projects")).eq(project.id()))
					.fetch(DSL.field(name("id"), Integer.class))
					.stream()
					.filter(id -> this.sec.datasetAllowed(id, VPermission.READ))
					.count();
	}

}
