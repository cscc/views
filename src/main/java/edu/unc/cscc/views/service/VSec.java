package edu.unc.cscc.views.service;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.domain.GrantedAuthoritySid;
import org.springframework.security.acls.model.Sid;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import edu.unc.cscc.views.model.Chart;
import edu.unc.cscc.views.model.security.ChartOID;
import edu.unc.cscc.views.model.security.DashboardOID;
import edu.unc.cscc.views.model.security.DataSetOID;
import edu.unc.cscc.views.model.security.ProjectOID;
import edu.unc.cscc.views.model.security.User;
import edu.unc.cscc.views.model.security.UserAuthentication;
import edu.unc.cscc.views.model.security.VACL;
import edu.unc.cscc.views.model.security.VPermission;
import edu.unc.cscc.views.model.security.VSid;
import edu.unc.cscc.views.service.storage.VACLService;

@Component(value = "vsec")
public class VSec
{

	public static final VPermission		PERM_READ = VPermission.READ;
	public static final VPermission		PERM_WRITE = VPermission.WRITE;
	
	private final VACLService			aclService;
	
	@Autowired
	public VSec(VACLService aclService)
	{
		Assert.notNull(aclService, "missing ACL service");
		this.aclService = aclService;
	}
	
	/**
	 * Convenience method for determining whether a chart is accessible
	 * to the active principal (based on permissions held for the associated
	 * principal and datasets.)
	 * 
	 * @param chart chart
	 * @return <code>true</code> if the principal can read the chart, 
	 * <code>false</code> otherwise
	 */
	public boolean
	canReadChart(Chart chart)
	{
		return this.projectAllowed(chart.project().id(), VPermission.READ)
				&& chart.datasets()
						.stream()
						.allMatch(d -> this.datasetAllowed(d.id(), 
												VPermission.READ));
	}
	
	public boolean
	chartAllowed(Integer id, String ... permissions)
	{
		return this.chartAllowed(id, 
						Stream.of(permissions)
								.map(VPermission :: parse)
								.filter(x -> x != null)
								.toArray(VPermission[] :: new));
	}
	
	/**
	 * Determine whether the current principal holds the given permissions on
	 * the chart with the given ID.
	 * 
	 * @param id
	 * @param permissions
	 * @return
	 */
	public boolean
	chartAllowed(Integer id, VPermission ... permissions)
	{
		if (permissions == null || permissions.length < 1)
		{
			return true;
		}
		
		if (id == null)
		{
			return false;
		}
		
		User user = currentUser();
		
		if (user != null
			&& user.getAuthorities().contains(User.ViewsRole.ROLE_SUPERUSER.authority()))
		{
			return true;
		}
		
		VACL acl = null;
		
		List<Sid> sids = getSids(user);
		
		
		if (user != null)
		{
			acl = this.aclService.load(new ChartOID(id), user);
		}
		else
		{
			acl = this.aclService.load(new ChartOID(id), Collections.singleton(User.ViewsRole.ANONYMOUS_USERS.authority()));
		}
		
		return acl.grants(Arrays.asList(permissions), sids);
		
	}
	
	public boolean
	datasetAllowed(Integer id, String ... permissions)
	{
		return this.datasetAllowed(id, 
						Stream.of(permissions)
								.map(VPermission :: parse)
								.filter(x -> x != null)
								.toArray(VPermission[] :: new));
	}
	
	
	/**
	 * Determine whether the current principal holds the given permissions on
	 * the dataset with the given ID.
	 * 
	 * @param id
	 * @param permissions
	 * @return
	 */
	public boolean
	datasetAllowed(Integer id, VPermission ... permissions)
	{
		if (permissions == null || permissions.length < 1)
		{
			return true;
		}
		
		if (id == null)
		{
			return false;
		}
		
		User user = currentUser();
		
		if (user != null
			&& user.getAuthorities().contains(User.ViewsRole.ROLE_SUPERUSER.authority()))
		{
			return true;
		}
		
		VACL acl = null;
		
		List<Sid> sids = getSids(user);
		
		
		if (user != null)
		{
			acl = this.aclService.load(new DataSetOID(id), user);
		}
		else
		{
			acl = this.aclService.load(new DataSetOID(id), Collections.singleton(User.ViewsRole.ANONYMOUS_USERS.authority()));
		}
		
		return acl.grants(Arrays.asList(permissions), sids);
	}
	
	public boolean
	dashboardAllowed(Integer id, String ... permissions)
	{
		return this.dashboardAllowed(id, 
						Stream.of(permissions)
								.map(VPermission :: parse)
								.filter(x -> x != null)
								.toArray(VPermission[] :: new));
	}
	
	
	/**
	 * Determine whether the current principal holds the given permissions on
	 * the dashboard with the given ID.
	 * 
	 * @param id
	 * @param permissions
	 * @return
	 */
	public boolean
	dashboardAllowed(Integer id, VPermission ... permissions)
	{
		if (permissions == null || permissions.length < 1)
		{
			return true;
		}
		
		if (id == null)
		{
			return false;
		}
		
		User user = currentUser();
		
		if (user != null
			&& user.getAuthorities().contains(User.ViewsRole.ROLE_SUPERUSER.authority()))
		{
			return true;
		}
		
		VACL acl = null;
		
		List<Sid> sids = getSids(user);
		
		
		if (user != null)
		{
			acl = this.aclService.load(new DashboardOID(id), user);
		}
		else
		{
			acl = this.aclService.load(new DashboardOID(id), Collections.singleton(User.ViewsRole.ANONYMOUS_USERS.authority()));
		}
		
		return acl.grants(Arrays.asList(permissions), sids);
	}
	
	public boolean
	projectAllowed(Integer id, String ... permissions)
	{
		return this.projectAllowed(id, 
						Stream.of(permissions)
								.map(VPermission :: parse)
								.toArray(VPermission[] :: new));
	}
	
	/**
	 * Determine whether the current principal holds the given permissions on
	 * the project with the given ID.
	 * 
	 * @param id
	 * @param permissions
	 * @return
	 */
	public boolean
	projectAllowed(Integer id, VPermission ... permissions)
	{
		if (permissions == null || permissions.length < 1)
		{
			return true;
		}
		
		if (id == null)
		{
			return false;
		}
		
		User user = currentUser();
		
		if (user != null
			&& user.getAuthorities().contains(User.ViewsRole.ROLE_SUPERUSER.authority()))
		{
			return true;
		}
		
		VACL acl = null;
		
		List<Sid> sids = getSids(user);
		
		
		if (user != null)
		{
			acl = this.aclService.load(new ProjectOID(id), user);
		}
		else
		{
			acl = this.aclService.load(new ProjectOID(id), Collections.singleton(User.ViewsRole.ANONYMOUS_USERS.authority()));
		}
		
		return acl.grants(Arrays.asList(permissions), sids);
	}
	
	private static final User
	currentUser()
	{
		Authentication auth = 
				SecurityContextHolder.getContext().getAuthentication();
		
		if (auth instanceof UserAuthentication)
		{
			return ((UserAuthentication) auth).getPrincipal();
		}
		
		return null;
	}
	
	private final List<Sid>
	getSids(User user)
	{
		if (user == null)
		{
			return Collections.singletonList(new GrantedAuthoritySid(User.ViewsRole.ANONYMOUS_USERS.authority()));
		}
		
		List<Sid> sids = new ArrayList<>();
		
		sids.add(new GrantedAuthoritySid(User.ViewsRole.ANONYMOUS_USERS.authority()));
		sids.addAll(user.getAuthorities()
						.stream()
						.map(g -> new GrantedAuthoritySid(g))
						.collect(Collectors.toSet()));
		sids.add(new VSid(user.id()));
		
		return sids;
		
	}
	
}
