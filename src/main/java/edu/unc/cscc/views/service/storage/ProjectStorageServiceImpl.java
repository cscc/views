package edu.unc.cscc.views.service.storage;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jooq.DSLContext;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.unc.cscc.views.model.Project;
import edu.unc.cscc.views.model.security.User;
import edu.unc.cscc.views.model.security.VPermission;
import edu.unc.cscc.views.service.VSec;

@Service
public class ProjectStorageServiceImpl
implements ProjectStorageService
{
	
	private static final Table<?>		TABLE = DSL.table(name("projects"));
	
	private final DSLContext			dsl;
	
	private final VSec					sec;
	
	@Autowired
	public ProjectStorageServiceImpl(DSLContext dsl, VSec sec)
	{
		this.sec = sec;
		this.dsl = dsl;
	}

	@PreAuthorize("@vsec.projectAllowed(#id, 'read')")
	@Override
	public Project
	load(int id)
	{
		return this.load(new ArrayList<>(Collections.singleton(id))).get(id);
	}
	
	@PostAuthorize("returnObject == null or @vsec.projectAllowed(returnObject.id(), 'read')")
	@Override
	public Project 
	loadByName(String name)
	{
		Integer id = 
				this.dsl.select(field(name("id")))
						.from(TABLE)
						.where(field(name("name")).eq(name))
						.fetchOne(field(name("id"), Integer.class));
		
		if (id == null)
		{
			return null;
		}
		
		return this.load(id);
	}

	@Override
	public boolean 
	exists(String name)
	{
		if (name == null)
		{
			throw new IllegalArgumentException("name must not be null");
		}
		
		return this.dsl.fetchExists(this.dsl
									.select(field(name("id")))
									.from(TABLE)
									.where(field(name("name")).eq(name)));
	}

	@PreFilter("@vsec.projectAllowed(filterObject, 'read')")
	@Override
	public Map<Integer, Project> 
	load(Collection<Integer> ids)
	{
		if (ids.isEmpty())
		{
			return Collections.emptyMap();
		}
		
		return this.dsl
					.selectFrom(TABLE)
					.where(field(name("id")).in(ids))
					.fetchMap(field(name("id"), Integer.class), 
							r -> new Project(r.get(name("id"), Integer.class),
											r.get(name("name"), String.class), 
											r.get(name("description"), String.class)));
	}

	@PreAuthorize("(#project.id() == null and hasAuthority('create_projects')) "
					+ " or (#project.id() != null and @vsec.projectAllowed(#project.id(), 'write'))")
	@Override
	@Transactional
	public Project 
	save(Project project)
	{
		
		if (project.id() == null)
		{
			Integer id = 
					this.dsl
						.insertInto(TABLE)
							.set(field(name("name")), project.name())
							.set(field(name("description")), project.description())
						.returning(field(name("id"), Integer.class))
						.fetchOne()
						.get(field(name("id"), Integer.class));
			
			return project.id(id);
		}
		else
		{
			this.dsl
				.update(TABLE)
					.set(field(name("name")), project.name())
					.set(field(name("description")), project.description())
				.where(field(name("id")).eq(project.id()))
				.execute();
		}
		
		return project;
		
	}

	@PreAuthorize("@vsec.projectAllowed(#id, 'write')")
	@Transactional
	@Override
	public void 
	delete(int id)
	{
		if (id < 0)
		{
			return;
		}
		
		this.dsl.deleteFrom(TABLE).where(field(name("id")).eq(id)).execute();
	}

	@Override
	public List<Integer> 
	search(String query, User user, int limit, int offset)
	{
	    int off = offset;
	    int remaining = limit;
	    
		List<Integer> l = new ArrayList<>();
		
		while (remaining > 0)
		{
		    List<Integer> res = this.unfilteredSearch(query, remaining, off);
		    
		    if (res.isEmpty())
		    {
		        break;
		    }
		    
		    for (final int id : res)
		    {
		        if (this.sec.projectAllowed(id, VPermission.READ))
		        {
		            l.add(id);
		        }
		    }
		    
		    off += limit;
		    remaining = limit - l.size();
		}
		
		return l;
		
	}

	protected List<Integer>
	unfilteredSearch(String query, int limit, int offset)
	{
	    if (StringUtils.isBlank(query))
        {
            return this.dsl
                    .select(field(name("id"), Integer.class))
                    .from(TABLE)
                    .orderBy(field(name("name")).asc())
                    .limit(limit).offset(offset)
                    .fetch(field(name("id"), Integer.class));
        }
        
        return this.dsl
                    .select(field(name("id"), Integer.class))
                    .from(TABLE)
                    .where(DSL.lower(field(name("name"), String.class))
                                .contains(query.trim().toLowerCase()))
                    .or(DSL.lower(field(name("description"), String.class))
                                .contains(query.trim().toLowerCase()))
                    .orderBy(field(name("name")).asc())
                    .limit(limit).offset(offset)
                    .fetch(field(name("id"), Integer.class));
	}
	
	
	/* secured programmatically */
	@Override
	public int 
	count(String query, User user)
	{
		List<Integer> projects;
		
		if (query == null)
		{
			projects =
					this.dsl
						.select(field(name("id"), Integer.class))
						.from(TABLE)
						.fetch(field(name("id"), Integer.class));
		}
		else
		{
			projects =
					this.dsl
						.select(field(name("id"), Integer.class))
						.from(TABLE)
						.where(DSL.lower(field(name("name"), String.class))
									.contains(query.trim().toLowerCase()))
						.or(DSL.lower(field(name("description"), String.class))
									.contains(query.trim().toLowerCase()))
						.fetch(field(name("id"), Integer.class));
		}
		
		return (int) projects.stream()
						.filter(id -> this.sec.projectAllowed(id, VPermission.READ))
						.count();
	}

	@Override
	public boolean 
	exists(int id)
	{
		return this.dsl
				.fetchExists(this.dsl
							.selectOne()
							.from(TABLE)
							.where(field(name("id")).eq(id)));
	}

}
