package edu.unc.cscc.views.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import edu.unc.cscc.views.model.security.LocalCredentials;
import edu.unc.cscc.views.model.security.User;
import edu.unc.cscc.views.model.security.UsernamePasswordCredentialsSource;
import edu.unc.cscc.views.service.storage.LocalCredentialStorageService;
import edu.unc.cscc.views.service.storage.UserStorage;

@Service
public final class LocalUsernamePasswordCredentialsSource 
implements UsernamePasswordCredentialsSource 
{
	
	public static final String IDENTIFIER = "local";

	private final PasswordEncoder					encoder;
	private final UserStorage						storage;
	private final LocalCredentialStorageService		credentialStorage;
	
	@Autowired
	public LocalUsernamePasswordCredentialsSource(UserStorage storage, 
				LocalCredentialStorageService credentialStorage)
	{
		this.storage = storage;
		this.credentialStorage = credentialStorage;
		this.encoder = new BCryptPasswordEncoder();
	}
	
	@Override
	public String 
	identifier() 
	{
		return IDENTIFIER;
	}
	
	@Override
	public String
	name()
	{
		return "Views";
	}

	@Override
	public String 
	description() 
	{
		return "Credentials local to views";
	}

	@Override
	public boolean
	authenticate(String username, String password) 
	throws IOException
	{
		
		User u = this.storage.find(username, IDENTIFIER);
		
		if (u == null)
		{
			return false;
		}
		
		LocalCredentials credentials = this.credentialStorage.load(u);
		
		return this.encoder.matches(password, credentials.hash());
	}

}
