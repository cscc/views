package edu.unc.cscc.views.service.storage;

import java.util.List;

import edu.unc.cscc.views.exception.UserExistsException;
import edu.unc.cscc.views.model.security.User;

public interface UserStorage
extends StorageService<User>
{

	/**
	 * Determine whether a user with the given username and using the 
	 * specified credential source is known to the system.
	 * 
	 * @param username username
	 * @param credentialSourceIdentifier ID of credential source
	 * @return <code>true</code> if such a user exists, <code>false</code>
	 * otherwise
	 */
	public boolean exists(String username, String credentialSourceIdentifier);
	
	public User find(String username, String credentialSourceIdentifier);
	
	/**
	 * Search for a user matching the given search term, accessible to the 
	 * given user.
	 * 
	 * @param user user for whom results should be restricted, not <code>null</code>
	 * @param term term for which to search or <code>null</code> to search
	 * for all users
	 * @param offset starting offset of search results, may be <code>null</code>
	 * if returned results should start from the first found user
	 * @param sortBy what to the sort results by, not <code>null</code>
	 * @return IDs of the users matching the given search term, not <code>null</code>
	 */
	public List<Integer>
	search(User user, String term, Integer limit, Integer offset);
	
	/**
	 * Count the number of users matching the given search term, accessible to the 
	 * given user.
	 * 
	 * @param user user for whom results should be restricted, not <code>null</code>
	 * @param term term for which to search or <code>null</code> to search
	 * for all users
	 * @returns count of users, &ge; 0
	 */
	public int
	count(User user, String term);

	/**
	 * Create the given user.  Similar to {@link #save(User)}, but requires that
	 * the given user not already exist in underlying storage.
	 * 
	 * @param user user to create
	 * @return copy of the user reflecting the underlying storage
	 */
	public User createNewUser(User user);


	/**
	 * Change the user's auth source to the passed auth source. Be careful, this could easily render a user unable to log in.
	 * 
	 * @param userId the id of the user to change
	 * @param username the user's new username
	 * @param credentialSourceIdentifier the user's new credential source
	 * @return The User object with the updated information
	 * @throws UserExistsException if a user with the passed username and credneitalSourceIdentifier already exists
	 */
	public User changeUserAuthSource(int userId, String username, String credentialSourceIdentifier) throws UserExistsException;
}
