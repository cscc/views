package edu.unc.cscc.views.service;

import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

import edu.unc.cscc.views.model.security.User;
import edu.unc.cscc.views.model.security.UserAuthentication;
import edu.unc.cscc.views.service.storage.UserStorage;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class TokenAuthenticationService
{

    private static final String AUTH_HEADER_NAME = "Authorization";

    private final UserStorage		userService;
    private final String 			secret;

    public TokenAuthenticationService(String secret, UserStorage userService)
    {
    	this.secret = Base64.getEncoder().encodeToString(secret.getBytes(Charset.forName("UTF-8")));
    	this.userService = userService;
    }

    public Authentication
    getAuthentication(HttpServletRequest request)
    {
        String token = request.getHeader(AUTH_HEADER_NAME);
        
        
        if (token == null)
        {
        	return null;
        }
        
        /* parse out the bearer stuff */

        if (! token.startsWith("Bearer "))
        {
        	return null;
        }
        
        token = token.substring(7);
        
        final User user = parseToken(token);
        
        if (user != null) 
        {
            return new UserAuthentication(user);
        }
        
        return null;
    }
    
    public User
    parseToken(String token)
    {    	
    	Claims claims = null;
    	
		try
		{
			claims = Jwts.parser()
			        .setSigningKey(secret)
			        .parseClaimsJws(token)
			        .getBody();
		} 
		catch (ExpiredJwtException e)
		{
			throw new AccessDeniedException("JWT token expired", e);
		} 
    	
    	String credSource = claims.get("credential-source", String.class);
    	
    	
        UserDetails userDetails = 
        		this.userService.find(claims.getSubject(), credSource);
        
        if (userDetails instanceof User)
        {
        	return (User) userDetails;
        }
        
        return null;
    }
    
    public String
    createToken(User user)
    {
    	 Date now = new Date();
         Date expiration = new Date(now.getTime() + TimeUnit.DAYS.toMillis(3l));
         return Jwts.builder()
                 .setId(UUID.randomUUID().toString())
                 .setSubject(user.username())
                 .claim("credential-source", user.getCredentialSourceID())
                 .setIssuedAt(now)
                 .setExpiration(expiration)
                 .signWith(SignatureAlgorithm.HS512, secret)
                 .compact();
    }
}