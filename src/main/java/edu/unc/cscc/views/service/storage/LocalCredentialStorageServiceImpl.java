package edu.unc.cscc.views.service.storage;

import java.sql.Timestamp;
import java.time.Instant;

import org.jooq.DSLContext;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.unc.cscc.views.model.security.LocalCredentials;
import edu.unc.cscc.views.model.security.User;

import static org.jooq.impl.DSL.*;

@Service
public class LocalCredentialStorageServiceImpl
implements LocalCredentialStorageService
{
	
	private static final Table<?>		TABLE = DSL.table(name("local_credentials"));
	
	private final DSLContext 			dsl;
	
	private final UserStorage			userStorage;
	
	private final PasswordEncoder		encoder;
	
	@Autowired
	public LocalCredentialStorageServiceImpl(DSLContext dsl, UserStorage userStorage)
	{
		this.dsl = dsl;
		this.userStorage = userStorage;
		this.encoder = new BCryptPasswordEncoder();
	}
	
	

	@Override
	public LocalCredentials 
	create(User user, String password)
	{
		
		final String hash = this.encoder.encode(password);
	
		return new LocalCredentials(user, hash, Instant.now());
	}



	@Override
	@Transactional
	public LocalCredentials 
	save(LocalCredentials credentials) 
	{
		if (! this.userStorage.exists(credentials.user().id()))
		{
			throw new IllegalArgumentException(
					"Cannot save credentials for non-existent user");
		}
		
		/* nuke existing entry */
		
		this.delete(credentials);
		
		this.dsl.insertInto(TABLE)
				.set(field(name("id__users"), Integer.class), credentials.user().id())
				.set(field(name("password_hash"), String.class), credentials.hash())
				.set(field(name("modified"), Timestamp.class),
						Timestamp.from(credentials.lastModified()))
				.execute();
		
		return credentials;
	}

	@Override
	public LocalCredentials 
	load(User user) 
	{
		return 
			this.dsl.selectFrom(TABLE)
				.where(field(name("id__users")).eq(user.id()))
				.fetchOne(r -> 
					new LocalCredentials(user, r.get(name("password_hash"), String.class),
										r.get(name("modified"), Timestamp.class).toInstant()));
		
	}

	@Override
	@Transactional
	public void 
	delete(LocalCredentials credentials) 
	{
		if (! this.userStorage.exists(credentials.user().id()))
		{
			throw new IllegalArgumentException("Cannot delete credentials for "
						+ "user that doesn't exist");
		}
		
		this.dsl.deleteFrom(TABLE)
				.where(field(name("id__users")).eq(credentials.user().id()))
				.execute();
		
	}
	
	

}
