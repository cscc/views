package edu.unc.cscc.views.config.credentials;

import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import edu.unc.cscc.views.model.security.CredentialSourceFactory;
import edu.unc.cscc.views.model.security.UsernamePasswordCredentialsSource;
import edu.unc.cscc.views.service.LocalUsernamePasswordCredentialsSource;

@Configuration
public class LocalCredentialSourceConfig
{
	
	@Autowired
	private LocalUsernamePasswordCredentialsSource	local;
	
	@Bean
	public CredentialSourceFactory
	localFactory()
	{
		return new CredentialSourceFactory()
		{
			
			@Override
			public Collection<UsernamePasswordCredentialsSource> 
			createSources()
			{
				return Collections.singleton(LocalCredentialSourceConfig.this.local);
			}
		};
	}

}
