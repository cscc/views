package edu.unc.cscc.views.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import edu.unc.cscc.views.service.TokenAuthenticationService;
import edu.unc.cscc.views.service.storage.UserStorage;

@Configuration
public class TokenConfig
{
	@Bean
	@Autowired
	public TokenAuthenticationService
	tokenService(@Value("${jwt.secret}") String secretKey,
					UserStorage userService)
	{
		return new TokenAuthenticationService(secretKey, userService);
	}

}
