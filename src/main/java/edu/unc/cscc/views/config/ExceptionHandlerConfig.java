package edu.unc.cscc.views.config;

import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import edu.unc.cscc.views.controller.NamedError;

@ControllerAdvice
@Component
public class ExceptionHandlerConfig
{
	@ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public NamedError 
    handle(MethodArgumentNotValidException exception)
	{
		return new NamedError("invalid-request",
    			exception.getBindingResult().getFieldErrors()
			                .stream()
			                .map(error -> 
			                		"field [" + error.getField() + "]: " + 
			                		error.getDefaultMessage())
			                .collect(Collectors.joining("; ")));
    }


    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public NamedError
    handle(ConstraintViolationException exception)
    {
        return new NamedError("invalid-request",
        			exception.getConstraintViolations()
				                .stream()
				                .map(ConstraintViolation::getMessage)
				                .collect(Collectors.joining("; ")));
    }
	
}
