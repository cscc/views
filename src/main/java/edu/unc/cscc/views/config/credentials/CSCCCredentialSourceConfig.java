package edu.unc.cscc.views.config.credentials;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.unc.cscc.views.model.security.CredentialSourceFactory;
import edu.unc.cscc.views.model.security.UsernamePasswordCredentialsSource;

/**
 * This class provides an example of an externalized credential source which
 * discovers some additional sources from an endpoint (in this case, a CSCC
 * Drupal instance), and provides authentication connectivity against the 
 * discovered source(s).
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
@Configuration
public class CSCCCredentialSourceConfig
{
	
	private final Logger		logger = LoggerFactory.getLogger(this.getClass());

	@Value("${auth.discovery}")
	private String discoveryEndpoint;
	
	@Bean
	public CredentialSourceFactory
	productionStudySource()
	{
		try	
		{
			this.logger.debug("trying discovery for test studies at: " + this.discoveryEndpoint);
			return new CSCCDrupalCredentialSourceFactory(this.discoveryEndpoint, true);
		} 
		catch (KeyManagementException | NoSuchAlgorithmException
				| KeyStoreException e)
		{
			this.logger.error("Failed to create credential source factory", e);
			return new CredentialSourceFactory()
			{
				
				@Override
				public Collection<UsernamePasswordCredentialsSource> createSources()
				{
					return Collections.emptyList();
				}
			};
		}
	}
	
	
	private static final class CSCCDrupalCredentialSourceFactory
	implements CredentialSourceFactory
	{
		private final Logger		logger;
		private final String		discoveryEndpoint;
		private final RestTemplate	template;
		
		public CSCCDrupalCredentialSourceFactory(final String discoveryEndpoint, 
													boolean allowSelfSigned) 
		throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException
		{
			this.discoveryEndpoint = discoveryEndpoint;
			
			HttpComponentsClientHttpRequestFactory factory = 
					new HttpComponentsClientHttpRequestFactory();
			
			factory.setConnectTimeout(300);
			factory.setConnectionRequestTimeout(300);
			factory.setReadTimeout(10000);
			
			if (allowSelfSigned)
			{
			      SSLConnectionSocketFactory socketFactory = 
			    		  new SSLConnectionSocketFactory(
			    				  new SSLContextBuilder()
			    				  		.loadTrustMaterial(null, new TrustSelfSignedStrategy())
			    				  		.build());
			      
			      factory.setHttpClient(HttpClients.custom().setSSLSocketFactory(socketFactory).build());
			}
			
			this.template = new RestTemplate(factory);
			this.logger = LoggerFactory.getLogger(this.getClass());
		}

		@Override
		public Collection<UsernamePasswordCredentialsSource> 
		createSources()
		{
			try
			{
				/* try to discover the studies that are available */
				
				final StudyDiscovery discovery = 
						this.template
								.getForObject(this.discoveryEndpoint, StudyDiscovery.class);
				
				
				return 
					discovery.studies
								.entrySet()
								.stream()
								.map(e -> {
									final String id = e.getKey();
									final DiscoveredStudy study = e.getValue();

									return new CSCCDrupalAuthenticationCredentialSource(
											study.url, 
											id, study.name, study.description, template);
								})
								.collect(Collectors.toList());
			} 
			catch (RestClientException e)
			{
				this.logger.error("Study discovery / credential source creation failed", e);
				return Collections.emptyList();
			}
		}
	}
	
	private static final class CSCCDrupalAuthenticationCredentialSource
	implements UsernamePasswordCredentialsSource
	{
		private final String		studyUrl;
		private final String		studyName;
		private final String		name;
		private final String		description;

		private final RestTemplate	template;
		
		private final Logger		logger;
		
		public CSCCDrupalAuthenticationCredentialSource(
				String url, String studyName, String name, 
				String description, RestTemplate template)
		{
			this.studyUrl = url;
			this.studyName = studyName;
			this.name = name;
			this.description = description;
			
			this.template = new RestTemplate(template.getRequestFactory());
			
			
			/* suppress ex throws for errors, since we check response codes
			 * ourselves
			 */
			this.template.setErrorHandler(new DefaultResponseErrorHandler(){

				@Override
				public void handleError(ClientHttpResponse response)
						throws IOException
				{}
				
			});
			
			this.logger = LoggerFactory.getLogger(this.getClass());
		}

		@Override
		public String 
		identifier()
		{
			return "cscc-" + this.studyName;
		}

		@Override
		public String name()
		{
			return this.name;
		}

		@Override
		public String description()
		{
			return this.description;
		}

		@Override
		public boolean 
		authenticate(String username, String password)
		throws IOException
		{
			
			Map<String, String> params = new HashMap<>();
			
			params.put("username", username);
			params.put("password", password);
			
			HttpHeaders headers = new HttpHeaders();
			
			headers.setContentType(MediaType.APPLICATION_JSON);
			
			HttpEntity<Map<String, String>> e = new HttpEntity<>(params, headers);
			
			
			ResponseEntity<?> response = 
							this.template
								.exchange(studyUrl, HttpMethod.POST, e, Object.class);
			
			if (response.getStatusCode() == HttpStatus.OK)
			{
				return true;
			}
			
			if (response.getStatusCode() == HttpStatus.FORBIDDEN)
			{
				return false;
			}
			
			
			this.logger.error("Unexpected response from endpoint: " 
								+ response.getStatusCode()
								+ " : " + response.getBody());
			
			return false;
			
		}
		
	}
	
	
	static final class StudyDiscovery
	{
		@JsonProperty("api-version")
		private String apiVersion;
		
		@JsonProperty("studies")
		private Map<String, DiscoveredStudy> studies;
	}
	
	static final class DiscoveredStudy
	{
		@JsonProperty
		private String		name;
		
		@JsonProperty(required = false)
		private String		description;
		
		@JsonProperty(value =  "base-url")
		private String		url;
	}
}
