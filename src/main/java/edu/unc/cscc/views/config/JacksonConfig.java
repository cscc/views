package edu.unc.cscc.views.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Configuration
public class JacksonConfig
{

	@Bean
	public ObjectMapper
	mapper()
	{
		return (new ObjectMapper())
					.findAndRegisterModules()
					.configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false);
	}
}
