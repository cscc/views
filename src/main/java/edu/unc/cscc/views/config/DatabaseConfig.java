package edu.unc.cscc.views.config;

import java.lang.reflect.Method;

import javax.sql.DataSource;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultDSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.security.util.InMemoryResource;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.AnnotationTransactionAttributeSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.interceptor.DelegatingTransactionAttribute;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.interceptor.TransactionAttribute;
import org.springframework.util.StringUtils;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement(mode = AdviceMode.ASPECTJ)
public class DatabaseConfig
{
	
	@Bean
	public HikariDataSource
	datasource(@Value("${jdbc.username}") String username, 
					@Value("${jdbc.password}") String password, 
					@Value("${jdbc.url}") String url,
					@Value("${jdbc.initialize}") boolean initialize)
	{
		
		
		HikariConfig config = new HikariConfig();
		
		if (StringUtils.hasText(username))
		{
			config.setUsername(username);
		}
		
		if (StringUtils.hasText(password))
		{
			config.setPassword(password);
		}
		
		config.setJdbcUrl(url);
		config.setMaximumPoolSize(4);
		
		
		HikariDataSource ds = new HikariDataSource(config);
		
		
		if (initialize)
		{
			ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
			
			
			
			ClassPathResource script = new ClassPathResource("schema.sql");
			
			
			if (ds.getJdbcUrl().contains(":hsqldb:"))
			{
				populator.addScripts(
						new InMemoryResource("DROP SCHEMA PUBLIC CASCADE;"),
						new InMemoryResource("SET DATABASE SQL SYNTAX PGS TRUE; "),
						new InMemoryResource("SET DATABASE TRANSACTION CONTROL MVCC;"),
						new InMemoryResource("CREATE TYPE bytea AS VARBINARY(1000000);"));
				DatabasePopulatorUtils.execute(populator, ds);
				
				populator = new ResourceDatabasePopulator();

			}
			populator.setScripts(script);

			
			DatabasePopulatorUtils.execute(populator, ds);
			
		}
		
		
		return ds;
	}
	
	@Bean
	@Autowired
	public DSLContext
	ctx(HikariDataSource datasource)
	{
		if (datasource.getJdbcUrl().contains(":hsqldb:"))
		{
			return new DefaultDSLContext(datasource, SQLDialect.HSQLDB);
		}
		else if (datasource.getJdbcUrl().contains(":postgresql:"))
		{
			return new DefaultDSLContext(datasource, SQLDialect.POSTGRES);
		}
		throw new IllegalArgumentException("Unsupported DB type");
	}
	
	@Bean
	@Autowired
	public PlatformTransactionManager
	transactionManager(TransactionAwareDataSourceProxy ctxProxy)
	{
		return new DataSourceTransactionManager(ctxProxy);
	}
	
	@Bean
	@Autowired
	public TransactionAwareDataSourceProxy
	ctxProxy(DataSource datasource)
	{
		return new TransactionAwareDataSourceProxy(datasource);
	}
	
	@Bean
	@Autowired
	public DataSourceConnectionProvider
	dsConnectionProvider(TransactionAwareDataSourceProxy ctxProxy)
	{
		return new DataSourceConnectionProvider(ctxProxy);
	}
	
	@SuppressWarnings("serial")
	@Autowired
	public void
	configure(TransactionAspectSupport tas)
	{
		tas.setTransactionAttributeSource(new AnnotationTransactionAttributeSource(){
			@Override
			public TransactionAttribute getTransactionAttribute(Method method,
					Class<?> targetClass) {
				TransactionAttribute target = super.getTransactionAttribute(method, targetClass);
		        if (target == null)
		        {
		        	return null;
		        }
		        else
		        {
		        	return new DelegatingTransactionAttribute(target)
		        	{
						private static final long serialVersionUID = 1L;
			
						@Override
			            public boolean rollbackOn(Throwable ex) {
			                return true;
			            }
		        	};
		        }
			}
		});
	}
}
