package edu.unc.cscc.views.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;

/**
 * Set appropriate CORS headers.
 * 
 * @author Rob Tomsick (rtomsick@unc.edu)
 *
 */
/* When the "unofficial" "legacy" way to do something is both the same size as
 * the official way and actually works reliably, you know something is wrong...
 */
public final class CORSFilter
extends OncePerRequestFilter
{

	@Override
	protected void
	doFilterInternal(HttpServletRequest request, HttpServletResponse response, 
					FilterChain filterChain)
	throws ServletException, IOException 
	{
		response.addHeader("Access-Control-Allow-Origin", "*");

		if (request.getHeader("Access-Control-Request-Method") != null
			&& "OPTIONS".equals(request.getMethod()))
		{
			response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
			response.addHeader("Access-Control-Allow-Headers", "Authorization, Content-Type");
			response.addHeader("Access-Control-Max-Age", "1");
		}

		filterChain.doFilter(request, response);
	}

}
