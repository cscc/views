package edu.unc.cscc.views.config;

import java.time.Instant;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

import edu.unc.cscc.views.model.security.LocalCredentials;
import edu.unc.cscc.views.model.security.User;
import edu.unc.cscc.views.model.security.User.ViewsRole;
import edu.unc.cscc.views.service.LocalUsernamePasswordCredentialsSource;
import edu.unc.cscc.views.service.storage.LocalCredentialStorageService;
import edu.unc.cscc.views.service.storage.UserStorage;

@Configuration
public class PreloadedUserConfig
{
	@Value("${admin.username}")
	private String		adminUsername;
	@Value("${admin.password}")
	private String		adminPassword;
	
	@Value("${admin.reset-password}")
	private boolean		resetAdminPassword;

	@Value("${admin.resourceadmins}")
	private String defaultResourceAdmins;
	
	@Autowired
	@Transactional
	public void
	setupAdminUser(LocalCredentialStorageService lcs, UserStorage us)
	{
		User user = us.find(adminUsername, LocalUsernamePasswordCredentialsSource.IDENTIFIER);
		
		boolean shouldSavePassword = this.resetAdminPassword;
		
		if (user == null)
		{
			user = new User(null, adminUsername, 
							LocalUsernamePasswordCredentialsSource.IDENTIFIER, 
							adminUsername, Instant.now(), true,
							Arrays.asList(ViewsRole.values()));
			user = us.save(user);
			shouldSavePassword = true;
		}
		
		if (shouldSavePassword)
		{
			LocalCredentials creds = lcs.create(user, adminPassword);
			
			creds = lcs.save(creds);
		}
	}
	
	public Map<String, String> 
	getDefaultResourceAdmin()
	{
        Map<String, String> resourceUsers = Arrays.stream(defaultResourceAdmins.split(","))
                .map(pair -> pair.split("-"))          // Split each pair by "-"
                .filter(userPwd -> userPwd.length == 2) // Ensure it's a valid "user-pwd" pair
                .collect(Collectors.toMap(userPwd -> userPwd[0], userPwd -> userPwd[1]));

		return resourceUsers;
	}

}