package edu.unc.cscc.views.config;

import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.auth0.client.auth.AuthAPI;
import com.auth0.client.mgmt.ManagementAPI;
import com.auth0.exception.Auth0Exception;
import com.auth0.json.auth.TokenHolder;
import com.auth0.net.AuthRequest;

@Configuration
public class Auth0Api {
    
    @Value("${auth.issuer-uri}")
    private String auth0Domain;

    @Value("${auth.client-id}")
    private String auth0ClientId;

    @Value("${auth.client-secret}")
    private String auth0ClientSecret;

    private final Timer timer = new Timer("Auth0TokenTimer");

    private void renewManagementAPIToken(AuthAPI authAPI, ManagementAPI managementAPI) {
        try {
            AuthRequest authRequest = authAPI.requestToken(auth0Domain + "api/v2/");
            TokenHolder holder = authRequest.execute();
            managementAPI.setApiToken(holder.getAccessToken());
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    renewManagementAPIToken(authAPI, managementAPI);
                }
            }, holder.getExpiresIn());
        } catch (Auth0Exception e) {
            System.err.println("Problem getting management api token, and so failing");
            System.err.println("Failed because: " + e.getMessage());
        }
    }


    @Bean
    public AuthAPI authAPI() {
        return new AuthAPI(auth0Domain, auth0ClientId, auth0ClientSecret);
    }

    @Bean
    @Autowired
    public ManagementAPI managementAPI(AuthAPI authAPI) {
        try {
            AuthRequest authRequest = authAPI.requestToken(auth0Domain + "api/v2/");
            TokenHolder holder = authRequest.execute();
            ManagementAPI managementAPI = new ManagementAPI(auth0Domain, holder.getAccessToken());
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    renewManagementAPIToken(authAPI, managementAPI);
                }
            }, holder.getExpiresIn());
            return managementAPI;
        } catch (Auth0Exception e) {
            System.err.println("Problem getting management api token, and so failing");
            throw new RuntimeException(e);
        }
    }

}
