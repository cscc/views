package edu.unc.cscc.views.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import edu.unc.cscc.views.service.TokenAuthenticationService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, 
							proxyTargetClass = false,
							mode = AdviceMode.ASPECTJ)
public class SecurityConfig
{

	@Autowired
    private TokenAuthenticationService tokenAuthenticationService;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception
    {
        return http.authorizeRequests()
                .antMatchers("/auth/**").permitAll()
                .antMatchers("/doc/**", 
                				"/swagger-ui.html",
                				"/swagger-resources/**",
                				"/webjars/**")
                	.permitAll()
                .antMatchers(HttpMethod.OPTIONS).permitAll() /* allowed for CORS preflight */
                .anyRequest().authenticated().and()
                .csrf().disable() // We shouldn't need this since we are using JWT token based authentication
                .addFilterAfter(new CORSFilter(), ChannelProcessingFilter.class)
                
                .addFilterBefore(new StatelessAuthenticationFilter(this.tokenAuthenticationService),
                        UsernamePasswordAuthenticationFilter.class)
                .anonymous().and()
                .servletApi().and()
                .exceptionHandling().accessDeniedHandler(new AccessDeniedHandlerImpl())
                .and()
                .headers()
                    .cacheControl()
                        .and()
                    .and()
                .build();
    }    
}