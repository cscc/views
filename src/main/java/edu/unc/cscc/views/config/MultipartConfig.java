package edu.unc.cscc.views.config;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

@Configuration
public class MultipartConfig
{

	/* workaround for Spring only wanting to handle multipart POSTs */
	@Bean
	public MultipartResolver 
	multipartResolver() 
	{
	   return new StandardServletMultipartResolver()
	   {
	     @Override
	     public boolean
	     isMultipart(HttpServletRequest request) 
	     {
	        
	        if (! Arrays.asList("put", "post")
	        			.contains(request.getMethod().toLowerCase()))
	        {
	           return false;
	        }
	        
	        final String contentType = request.getContentType();
	        
	        return (contentType != null 
	        		&& contentType.toLowerCase().startsWith("multipart/"));
	     }
	   };
	}
}
