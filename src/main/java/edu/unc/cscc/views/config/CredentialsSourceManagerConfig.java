package edu.unc.cscc.views.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import edu.unc.cscc.views.model.security.CredentialSourceFactory;
import edu.unc.cscc.views.service.UsernamePasswordCredentialsSourceManager;

@Configuration
public class CredentialsSourceManagerConfig
{
	@Autowired
	private List<CredentialSourceFactory>			factories;
	
	@Autowired
	public void
	configureSources(UsernamePasswordCredentialsSourceManager manager)
	{
		
		this.factories.forEach(s -> s.createSources().forEach(manager :: addSource));
		
	}
	
	
	
	

}
