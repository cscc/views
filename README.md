views
===

This is the server-side component of the views application, CSCC's chart
and visualization management tool.

Environment
---

This tool requires Java 8. If Java 8 is not the default version on your system, you can use the `env-activate` script on Unix like systems to temporarily activate a Java 8 environment. 

First change the `JAVA_8_DIR` variable at the top of the `env-activate` file to point to the directory where the Java 8 JRE is located. Then on a bash like shell run `source env-activate` to switch to Java 8. You can deactivate this setup by running the command `deactivate` at any time.

Building
---

Compile with `mvn compile`, or package for deployment with `mvn package`. Note that this must be with Java 8, newer versions of Java won't work.

Configuration/Usage
---
For a look at the various configuration options, take a look at 
`src/main/resources/application.properties`.  These properties can be defined
at run-time like so:

```
java -jar <path to views JAR> --jdbc.password="secret password"
```

You will probably want to change at least the secret used for JWT token 
signing (`jwt.secret`). On the initial run you will want to set the `jbdc.initialize` value to true in order to set up the database. Be sure to change it back to false afterwards

### Setting up Auth0

Create an Auth0 tenant and create a Machine to Machine application within it. Once you have the account set up, set the `auth.client-id`, `auth.client-secret`, and `auth.issuer-url` configuration keys in application.yml. These should be your Auth0 client-id, client-secret, and domain respectively. The machine to machine app will need to access the Auth0 management API with the `update:users`, `create:users`, `read:users`, and `create:user_tickets` permissions. You will need to set the callback URL to be your server's URL

Deploying
---

Update the `application.yml` to fit your production server as described in the configuration section above.
Build the jar file with `mvn package`. Copy this jar file to whatever server you are running
and start it. Note that the jar file runs on port 8080 by default. You can override application.yml
settings later as commandline arguments when you start the server. Ex. 
`java -jar views.jar --jbdc.username=user --jwt.secret=yoursecret`

Be sure to also deploy any updates to the fronted.

### Configuration

Here are some notes on some other useful configuration:

- The discovery endpoint for the Drupal login integration can be set using the auth.discovery key in application.yml

API Documentation
---
During the compilation phase, an OpenAPI spec will be generated, providing 
documentation and metadata on all RESTful endpoints provided by the views
application.  The resulting spec will be located in `api-docs/`. 

If you do not wish to build the project from source in order to generate the 
documentation, a build artifact containing the API documentation in HTML form 
(without the OpenAPI spec JSON) is available from 
[the project's Gitlab page](https://gitlab.com/cscc/views) (the artifact is 
called `api-docs` as of this writing.)

Errata
---

This project uses `swagger-core` for its API specification/API modelling.

`swagger-core` is, unfortunately, buggy.  As a result, there are a number of
aspects of the views domain model that look somewhat peculiar, source-code-wise.
In most cases these oddities are commented with a reference to whatever
bug number they're working around.  

The one exception to this is the presence of "setters" annotated with 
`@JsonSetter`, yet marked `private`.  As the views domain model is 
immutable, these setters are not intended for use (and in fact, will throw
an exception when invoked.)  They are intended solely as a workaround to 
[swagger-core bug #2379](https://github.com/swagger-api/swagger-core/issues/2379)
and [swagger-core bug #2169](https://github.com/swagger-api/swagger-core/issues/2169).